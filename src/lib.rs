// File Name: lib.rs
// Description: Python module for time expensive calculations
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPL3
// Creation Date: 11 Jan 2023 08:24:49
// Last Modified: 27 Feb 2024 12:53:51

use numpy::array::{PyArray1, PyArray3};
use numpy::ndarray::{s, Array, Axis, Ix3};
use numpy::{IntoPyArray, PyReadonlyArray1, PyReadonlyArray3};
use pyo3::{pyfunction, pymodule, types::PyModule, wrap_pyfunction, PyResult, Python};
use std::f64::consts::PI;

mod errors;
mod util;
use errors::{BasicError, IndexError};
use util::{deg2rad, rad2deg, repeat_3d};

/// solar constant (W m-2)
const GSC: u64 = 1367;
///  kernel constants
const KC: [[f64; 4]; 3] = [
    [1.0, 0.0, 0.0, 1.0],
    [-0.007574, -0.070987, 0.307588, 0.189184],
    [-1.284909, -0.166314, 0.041840, -1.377622],
];

#[pymodule]
fn rustlib(py: Python<'_>, m: &PyModule) -> PyResult<()> {
    child_alb(py, m)?;
    child_snow(py, m)?;
    Ok(())
}

/// submodule alb
fn child_alb(py: Python<'_>, parent_module: &PyModule) -> PyResult<()> {
    let child_module = PyModule::new(py, "alb")?;
    child_module.add_function(wrap_pyfunction!(extraterr, child_module)?)?;
    child_module.add_function(wrap_pyfunction!(rs_cf, child_module)?)?;
    child_module.add_function(wrap_pyfunction!(fdif, child_module)?)?;
    child_module.add_function(wrap_pyfunction!(albedo, child_module)?)?;
    parent_module.add_submodule(child_module)?;
    Ok(())
}

/// submodule snow
fn child_snow(py: Python<'_>, parent_module: &PyModule) -> PyResult<()> {
    let child_module = PyModule::new(py, "snow")?;
    child_module.add_function(wrap_pyfunction!(dss, child_module)?)?;
    parent_module.add_submodule(child_module)?;
    Ok(())
}

/// Instantaneous (hourly) extraterrestrial radiation; Ham 2005
/// Converted from MATLAB code provided by Dr. Albin Hammerle.
///
/// INPUTS:
///     doy = (hourly) decimal day of year
///     lst = (hourly) decimal local standard time
///     lat = latitude
///     lon = longitude
/// lat and lon are obtained via np.meshgrid and should be y, x, t
#[pyfunction]
#[pyo3(
    name = "extraterrestrial_radiation",
    text_signature = "(doy, lst, lat, lon) -> rs, sza"
)]
fn extraterr<'py>(
    _py: Python<'py>,
    doy: PyReadonlyArray3<f64>,
    lst: PyReadonlyArray3<f64>,
    lat: PyReadonlyArray3<f64>,
    lon: PyReadonlyArray3<f64>,
) -> (&'py PyArray3<f64>, &'py PyArray3<f64>) {
    let doy_arr = doy.as_array();
    let lst_arr = lst.as_array();
    let lob_arr = lon.as_array();
    let lat_arr = lat.as_array();

    // standard meridian of local time zone (degrees);
    let mut lob_round = &lob_arr / 15.0;
    lob_round.par_mapv_inplace(|v| v.round());
    let sm = 15.0 * lob_round;

    // declination angle (radians); Ham 2005 [Eqn 37];
    let mut sin_doy = 2.0 * PI / 365.0 * &doy_arr - 1.39;
    sin_doy.par_mapv_inplace(|v| v.sin());
    let delta = 0.409 * sin_doy;

    // latitude phi (radians);
    let phi = deg2rad(&lat_arr);

    // radians;
    let t = 2.0 * PI * &doy_arr / 366.0 + 4.8718;
    let mut t_sin = t.clone();
    t_sin.par_mapv_inplace(|v| v.sin());

    let mut t_sin2 = &t * 2.0;
    t_sin2.par_mapv_inplace(|v| v.sin());

    let mut t_sin3 = &t * 3.0;
    t_sin3.par_mapv_inplace(|v| v.sin());

    let mut t_sin4 = &t * 4.0;
    t_sin4.par_mapv_inplace(|v| v.sin());

    let mut t_cos = t.clone();
    t_cos.par_mapv_inplace(|v| v.cos());

    let mut t_cos2 = &t * 2.0;
    t_cos2.par_mapv_inplace(|v| v.cos());

    let mut t_cos3 = &t * 3.0;
    t_cos3.par_mapv_inplace(|v| v.cos());

    // equation of time (minutes); Ham 2005 [Eqn 40];
    let e = (5.0323 - 430.847 * t_cos + 12.5024 * t_cos2 + 18.25 * t_cos3 - 100.976 * t_sin
        + 595.275 * t_sin2
        + 3.6858 * t_sin3
        - 12.47 * t_sin4)
        / 60.0;

    let tsolar = &lst_arr + e / 60.0 + (sm - &lob_arr) / 15.0;

    // hour angle (angular distance); Ham 2005 [Eqn 46]
    let h = (12.0 - &tsolar) * PI / 12.0;

    let mut sin_phi = phi.clone();
    sin_phi.par_mapv_inplace(|v| v.sin());

    let mut sin_delta = delta.clone();
    sin_delta.par_mapv_inplace(|v| v.sin());

    let mut cos_phi = phi.clone();
    cos_phi.par_mapv_inplace(|v| v.cos());

    let mut cos_delta = delta.clone();
    cos_delta.par_mapv_inplace(|v| v.cos());

    let mut cos_h = h.clone();
    cos_h.par_mapv_inplace(|v| v.cos());

    let cos_z = &sin_phi * &sin_delta + &cos_phi * &cos_delta * &cos_h;

    // solar zenith angle (radians); Ham 2005 [Eqn 48]
    let mut sza = cos_z.clone();
    sza.par_mapv_inplace(|v| v.acos());

    // solar elevation angle (degrees); Ham 2005 [Eqn 49]
    let mut se_deg = sin_phi * sin_delta + cos_phi * cos_delta * cos_h;
    se_deg.par_mapv_inplace(|v| v.asin());
    let se_deg = rad2deg(&se_deg);
    let se_deg2 = &se_deg * &se_deg;

    // global irradiance (Avaste 1967); Ham 2005 [Eq 64]
    let mut rs = 0.8646 + 7.2396 * &se_deg + 0.43386 * &se_deg2
        - 6.6192 * f64::powi(10.0, -3) * &se_deg2 * &se_deg
        + 2.6677 * f64::powi(10.0, -5) * &se_deg2 * &se_deg2;

    rs.zip_mut_with(&se_deg, |r, s| {
        if s < &0.0 {
            *r = 0.0
        }
    });

    return (rs.into_pyarray(_py), sza.into_pyarray(_py));
}

/// Calculates global irradiance correction factor with BESS data (y, x, t) (Ryu et al. 2018).
/// Converted from MATLAB code provided by Dr. Albin Hammerle.
/// Variables should be passed y, x, t
#[pyfunction]
#[pyo3(name = "correction_factor", text_signature = "(bess, rs) -> rs_cf")]
fn rs_cf<'py>(
    _py: Python<'py>,
    bess: PyReadonlyArray3<f64>,
    rs: PyReadonlyArray3<f64>,
) -> Result<&'py PyArray3<f64>, BasicError> {
    let shape = bess.shape();
    let rs_arr = rs.as_array();

    // daily mean irradiance (mean hourly values to daily)
    let mut rs_dm: Array<f64, Ix3> = Array::zeros((shape[0], shape[1], shape[2]));
    rs_dm.fill(f64::NAN);

    for i in 0..shape[2] {
        let dm = &rs_arr
            .slice(s![.., .., i * 24..i * 24 + 24])
            .mean_axis(Axis(2));

        match dm {
            Some(mean) => {
                rs_dm.slice_mut(s![.., .., i]).assign(mean);
            }
            None => {
                return Err(BasicError {
                    msg: "Unable to calculate mean for 'rs' along axis '2'".to_string(),
                })
            }
        }
    }

    // correction factor; actual (bess) radiation per rs
    let cf = &bess.as_array() / rs_dm;

    // create 24 values a day (duplicate daily values to hourly)
    let cf_24 = repeat_3d(&cf, 24);
    let rs_cf = &rs_arr * &cf_24;

    return Ok(rs_cf.into_pyarray(_py));
}

/// Estimates diffuse fraction of radiation (Chen et al. 1999; EcoMod; Eq. 19).
/// Returns fraction of diffuse (fdif) radiation of the global solar radiation (W/m²).
/// Converted from MATLAB code provided by Dr. Albin Hammerle.
/// Variables should be passed y, x, t
#[pyfunction]
#[pyo3(name = "partition_diffuse", text_signature = "(sza, rs_cf) -> fdif")]
fn fdif<'py>(
    _py: Python<'py>,
    sza: PyReadonlyArray3<f64>,
    rs_cf: PyReadonlyArray3<f64>,
) -> &'py PyArray3<f64> {
    // clearness index
    let rs_cf_arr = rs_cf.as_array();
    let sza_arr = sza.as_array();

    let mut cos_sza = sza_arr.to_owned();
    cos_sza.par_mapv_inplace(|v| v.cos());
    let ci = &rs_cf_arr / (GSC as f64 * cos_sza);

    let mut fdif = ci.clone();
    fdif.par_mapv_inplace(|v| {
        if v < 0.8 {
            0.943 + 0.734 * v - 4.9 * f64::powi(v, 2)
                + 1.795 * f64::powi(v, 3)
                + 2.058 * f64::powi(v, 4)
        } else {
            0.13
        }
    });

    return fdif.into_pyarray(_py);
}

/// Calculates 'actual' albedo.
/// Converted from MATLAB code provided by Dr. Albin Hammerle.
/// Variables should be passed y, x, t
/// g0-2 = MCD43A1 bands
#[pyfunction]
#[pyo3(
    name = "albedo",
    text_signature = "(sza, fdif, rs_cf, g0, g1, g2) -> albedo"
)]
fn albedo<'py>(
    _py: Python<'py>,
    sza: PyReadonlyArray3<f64>,
    fdif: PyReadonlyArray3<f64>,
    rs_cf: PyReadonlyArray3<f64>,
    g0: PyReadonlyArray3<f64>,
    g1: PyReadonlyArray3<f64>,
    g2: PyReadonlyArray3<f64>,
) -> &'py PyArray3<f64> {
    let sza_arr = sza.as_array();
    let fdif_arr = fdif.as_array();
    // create hourly values
    let g0_arr = repeat_3d(&g0.as_array(), 24);
    let g1_arr = repeat_3d(&g1.as_array(), 24);
    let g2_arr = repeat_3d(&g2.as_array(), 24);

    let sza2 = &sza_arr * &sza_arr;
    let sza3 = &sza2 * &sza_arr;

    // black-sky albedo
    let bs = &g0_arr * (KC[0][0] + KC[0][1] * &sza2 + KC[0][2] * &sza3)
        + &g1_arr * (KC[1][0] + KC[1][1] * &sza2 + KC[1][2] * &sza3)
        + &g2_arr * (KC[2][0] + KC[2][1] * &sza2 + KC[2][2] * &sza3);

    // white-sky albedo
    let ws = &g0_arr * KC[0][3] + &g1_arr * KC[1][3] + &g2_arr * KC[2][3];

    // actual albedo
    let mut bw = &bs * (1.0 - &fdif_arr) + &ws * &fdif_arr;

    // clear vor dark periods
    bw.zip_mut_with(&rs_cf.as_array(), |b, r| {
        if r <= &0.0 {
            *b = f64::NAN
        }
    });

    // set maximum of 1
    bw.iter_mut()
        .filter(|v| v > &&mut 1.0)
        .for_each(|v| *v = 1.0);

    return bw.into_pyarray(_py);
}
/// Calculates days since snowfall.
/// Variables should be passed y, x, t
///
/// Written in rust, because I could not find a smart way to write
/// an implementation that was fast enough in Python.
///
/// The first value (time) cannot be evaluated, so the returned array will be one index shorter.
/// NaN values will be set to NaN until snow depth starts increasing again.
/// This way, only valid '(no) snow events' will be included.
#[pyfunction]
#[pyo3(name = "days_since_snowfall", text_signature = "(sd, fill) -> dss")]
fn dss<'py>(
    _py: Python<'py>,
    sd: PyReadonlyArray1<f64>,
    fill: i64,
) -> Result<&'py PyArray1<i64>, IndexError> {
    let mut dss: Vec<i64> = vec![fill; sd.len() - 1];
    let mut days = 0;
    let mut nan = false;

    match sd.get([0]) {
        Some(s) => {
            if s.is_nan() {
                nan = true
            }
        }
        None => {
            return Err(IndexError {
                msg: "snow depth at index",
                index: 0,
            });
        }
    }

    for t in 1..sd.len() {
        let curr: &f64;
        let prev: &f64;

        match sd.get([t]) {
            Some(float) => curr = float,
            None => {
                return Err(IndexError {
                    msg: "snow depth at index",
                    index: t,
                });
            }
        }

        match sd.get([t - 1]) {
            Some(float) => prev = float,
            None => {
                return Err(IndexError {
                    msg: "snow depth at index",
                    index: t - 1,
                });
            }
        }

        if curr.is_nan() {
            nan = true;
        }

        if nan == false {
            if curr > prev {
                days = 0;
            } else {
                days += 1;
            }

            dss[t - 1] = days;
        // if NaN is true but the current SD (which is not NaN) is bigger than the previous SD (which is not NaN) we break NaN
        // comparisons between NaNs will always return false
        } else {
            if curr > prev {
                nan = false;
                days = 0;
                dss[t - 1] = days;
            }
        }
    }

    Ok(dss.into_pyarray(_py))
}
