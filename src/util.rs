// File Name: util.rs
// Description:
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPL3
// Creation Date: 20 Sep 2023 16:52:35
// Last Modified: 20 Sep 2023 17:17:59

use numpy::ndarray::{s, Array, ArrayBase, Data, Dimension, Ix3, OwnedRepr};
use std::f64::consts::PI;

/// Converts a f64 array of degrees to radians.
pub fn deg2rad<S, D>(deg: &ArrayBase<S, D>) -> ArrayBase<OwnedRepr<f64>, D>
where
    S: Data<Elem = f64>,
    D: Dimension,
{
    return deg * (PI / 180.0);
}

/// Converts a f64 array of radians to degrees.
pub fn rad2deg<S, D>(rad: &ArrayBase<S, D>) -> ArrayBase<OwnedRepr<f64>, D>
where
    S: Data<Elem = f64>,
    D: Dimension,
{
    return rad * (180.0 / PI);
}

// Repeats a 3d array n times along the last axis.
pub fn repeat_3d<S>(to_repeat: &ArrayBase<S, Ix3>, n: usize) -> ArrayBase<OwnedRepr<f64>, Ix3>
where
    S: Data<Elem = f64>,
{
    let shape = to_repeat.shape();
    let mut repeated: Array<f64, Ix3> = Array::zeros((shape[0], shape[1], shape[2] * n));
    repeated.fill(f64::NAN);

    for i in 0..shape[2] * n {
        repeated
            .slice_mut(s![.., .., i])
            .assign(&to_repeat.slice(s![.., .., (i as f64 / n as f64).floor() as usize]));
    }

    return repeated;
}

#[cfg(test)]
mod tests {
    use super::{deg2rad, rad2deg, repeat_3d};
    use numpy::ndarray::array;

    #[test]
    fn test_deg2rad() {
        let deg = array![11.9977, 33.2316, 360.0008, 44.1235];
        let result = deg2rad(&deg);

        // round to 4 digits
        let rounded = result.mapv(|v| (v * f64::powi(10.0, 4)).round() / f64::powi(10.0, 4));
        let rad = array![0.2094, 0.5800, 6.2832, 0.7701];
        assert_eq!(rounded, rad);
    }

    #[test]
    fn test_rad2deg() {
        let rad = array![0.2094, 0.5800, 6.2832, 0.7701];
        let result = rad2deg(&rad);

        // round to 4 digits
        let rounded = result.mapv(|v| (v * f64::powi(10.0, 4)).round() / f64::powi(10.0, 4));
        let deg = array![11.9977, 33.2316, 360.0008, 44.1235];
        assert_eq!(rounded, deg);
    }

    #[test]
    fn test_repeat() {
        let arr = array![[[0.0, 0.0], [1.0, 1.0]]];
        let result = repeat_3d(&arr, 2);

        let should_be = array![[[0.0, 0.0, 0.0, 0.0], [1.0, 1.0, 1.0, 1.0]]];

        assert_eq!(result, should_be);
    }
}
