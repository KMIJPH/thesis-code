// File Name: errors.rs
// Description: Errors
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPL3
// Creation Date: 11 Jan 2023 08:24:49
// Last Modified: 20 Sep 2023 16:53:49

use pyo3::exceptions::PyOSError;
use pyo3::PyErr;
use std::fmt;

/// Index error type
#[derive(Debug)]
pub struct IndexError {
    pub msg: &'static str,
    pub index: usize,
}
impl std::error::Error for IndexError {}
impl fmt::Display for IndexError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "IndexError: {} {}", self.msg, self.index)
    }
}
impl std::convert::From<IndexError> for PyErr {
    fn from(err: IndexError) -> PyErr {
        PyOSError::new_err(err.to_string())
    }
}

/// Basic error type
#[derive(Debug)]
pub struct BasicError {
    pub msg: String,
}
impl std::error::Error for BasicError {}
impl fmt::Display for BasicError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.msg)
    }
}
impl std::convert::From<BasicError> for PyErr {
    fn from(err: BasicError) -> PyErr {
        PyOSError::new_err(err.to_string())
    }
}
