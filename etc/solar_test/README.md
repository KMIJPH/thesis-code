Taken from https://github.com/AlexandreWadoux/MapQualityEvaluation.  

None of this is required. It was used to test the solar diagram in python.  
The generated .csv files are included here.

## Dependencies
- r
- tcl
- tk
- gdal

```R
install.packages(
    c(
        "ranger",
        "rpart",
        "sp",
        "raster",
        "tdr",
        "ggrepel",
        "magrittr",
        "ggforce"
    )
)
```
