#!/usr/bin/env python3
# File Name: radiative_forcing.py
# Description: Radiative forcing calculations
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 06 Dez 2023 09:17:22
# Last Modified: 25 Jan 2024 18:20:44

# stdlib imports
import os
from copy import deepcopy
from datetime import datetime
from typing import Any

# dependencies
import numpy as np
import pandas as pd
from netCDF4 import Dataset, num2date

# local imports
from settings import CACK, DIRS, LC_FILTER, Data, FuSE, get_mask
from util import Array, Indexer, index

# m2
AREA_AUSTRIA = 8.388e+10
AREA_EARTH = 5.101e+14


def indices(ds: Dataset, m: int, start: datetime, end: datetime) -> np.ndarray:
    """
    Returns time indices for monthly intervals.
    """
    time = ds["time"]
    dtime = num2date(time[:], time.units, calendar=time.calendar).data

    i = index(dtime, Indexer.MONTH, m)
    outofrange = np.array([False if x <= end and x >= start else True for x in dtime])
    arr = np.array(i)
    arr[outofrange] = False
    return arr


def separate(lc: np.ndarray, name: str, data: np.ndarray) -> np.ndarray:
    """
    Returns the albedo values for a given land cover.
    """
    days, _, _ = data.shape
    lv = lc == Data.LC.lc_value(name)
    lc_filter = np.repeat(lv, days, axis=0)
    if lc_filter.shape != data.shape:
        print(f"WARNING: empty slice for '{name}'")
        return np.array([])
    else:
        return data[lc_filter]


def monthly_cack(mask: np.ndarray, year: int, fuse: FuSE) -> np.ndarray:
    """
    Returns the monthly CACK for Austria.
    """
    monthly: list[np.ndarray] = []

    if year < 2020:
        cack_ds = Dataset(CACK().get_outpath(), "r")
        for m in range(0, 12):
            cack = cack_ds["cack"][m, :, :]
            cack[mask == 0] = np.NaN
            cack[cack.mask] = np.NaN
            monthly.append(cack)
    else:
        y = year - 2020
        cack_ds = Dataset(CACK(file=fuse.get_filename() + ".nc").get_outpath(), "r")
        for m in range(0, 12):
            cack = cack_ds["cack"][y, m, :, :]
            cack[mask == 0] = np.NaN
            cack[cack.mask] = np.NaN
            monthly.append(cack)

    return np.array(monthly)


def rfa(km: Array, delta: Array, ar: float) -> float:
    """
    Returns the radiative forcing from albedo changes in (W / m^2) according to Wohlfahrt et al. 2021 - eq. 4
    """
    return (np.sum(km * delta) / 12) * (ar / AREA_EARTH)


def delta(fuse: FuSE, reference: list[int], years: list[int], yearly: bool = False) -> None:
    """
    Partitions raw data in order to calculate delta albedo.
    if yearly == True: calculate yearly delta from reference
    if yearly == False: calculate delta from reference

    Not very time efficient..
    NOTE: Values outside of Austria are filtered.
    """
    mask = get_mask()

    alb_modis_era, _ = Data.ALB_PRED_MODIS_ERA_LAI_MEAN.ds(fuse=fuse)
    alb, _ = Data.ALB_PRED.ds(fuse=fuse)

    # to separate lc
    p, _ = Data.LC.ds()
    lc_ds = Dataset(p, "r")
    lc = lc_ds[Data.LC.varn()][:].data
    lc_types = Data.LC.lc_types()
    lc_names = list(filter(lambda x: x not in LC_FILTER, lc_types.keys()))

    template = {m: {"sum": 0, "n": 0} for m in range(1, 13)}
    container1 = deepcopy(template)
    lc_container1 = {k: deepcopy(template) for k in lc_names}
    container2: dict[int, Any]
    lc_container2: dict[str, Any]

    if yearly:
        container2 = {y: deepcopy(template) for y in range(2020, 2101)}
        lc_container2 = {k: deepcopy(container2) for k in lc_names}
    else:
        container2 = deepcopy(template)
        lc_container2 = {k: deepcopy(template) for k in lc_names}

    for m in range(1, 13):
        print(f"  {datetime.now()}: month {m}")

        # in order to not overload RAM, sum and count over years and calculate mean later
        for y in reference:
            if y > 2019:
                ds = Dataset(alb, "r")
            else:
                ds = Dataset(alb_modis_era, "r")

            start = datetime(y, 1, 1)
            end = datetime(y, 12, 31)
            i = indices(ds, m, start, end)
            data = ds["bw"][i, :, :]
            data[data.mask] = np.NaN

            if len(data) > 0:
                # austria mask
                days, _, _ = data.shape
                temp_mask = np.repeat(mask[np.newaxis, :, :], days, axis=0)
                data[~temp_mask] = np.NaN

                container1[m]["sum"] += np.nansum(data)
                container1[m]["n"] += np.sum(~np.isnan(data))

                for name in lc_names:
                    alb_lc = separate(lc, name, data)
                    lc_container1[name][m]["sum"] += np.nansum(alb_lc)
                    lc_container1[name][m]["n"] += np.sum(~np.isnan(alb_lc))

        ds = Dataset(alb, "r")
        for y in years:
            start = datetime(y, 1, 1)
            end = datetime(y, 12, 31)
            i = indices(ds, m, start, end)
            data = ds["bw"][i, :, :]
            data[data.mask] = np.NaN

            if len(data) > 0:
                # austria mask
                days, _, _ = data.shape
                temp_mask = np.repeat(mask[np.newaxis, :, :], days, axis=0)
                data[~temp_mask] = np.NaN

                if yearly:
                    container2[y][m]["sum"] += np.nansum(data)
                    container2[y][m]["n"] += np.sum(~np.isnan(data))
                else:
                    container2[m]["sum"] += np.nansum(data)
                    container2[m]["n"] += np.sum(~np.isnan(data))

                for name in lc_names:
                    alb_lc = separate(lc, name, data)
                    if yearly:
                        lc_container2[name][y][m]["sum"] += np.nansum(alb_lc)
                        lc_container2[name][y][m]["n"] += np.sum(~np.isnan(alb_lc))
                    else:
                        lc_container2[name][m]["sum"] += np.nansum(alb_lc)
                        lc_container2[name][m]["n"] += np.sum(~np.isnan(alb_lc))

    def mean(sum: int, n: int) -> float:
        try:
            return sum / n
        except ZeroDivisionError:
            return np.NaN

    # combined
    m1 = np.array([mean(container1[m]["sum"], container1[m]["n"]) for m in range(1, 13)])
    if yearly:
        m2 = {y: np.array([mean(container2[y][m]["sum"], container2[y][m]["n"]) for m in range(1, 13)]) for y in years}
        delta_combined = {y: m1 - m2[y] for y in years}
    else:
        m2 = np.array([mean(container2[m]["sum"], container2[m]["n"]) for m in range(1, 13)])  # type: ignore
        delta_combined = m1 - m2

    # separate (per lc) means
    m_lc1 = {k:
             np.array(
                 [mean(lc_container1[k][m]["sum"], lc_container1[k][m]["n"]) for m in range(1, 13)]
             ) for k in lc_names
             }
    if yearly:
        m_lc2 = {k:
                 {y:
                  np.array(
                      [mean(lc_container2[k][y][m]["sum"], lc_container2[k][y][m]["n"]) for m in range(1, 13)])
                     for y in years
                  }
                 for k in lc_names
                 }
        delta_lc = {k: {y: m_lc1[k] - m_lc2[k][y] for y in years} for k in lc_names}
    else:
        m_lc2 = {k: np.array(  # type: ignore
            [mean(lc_container2[k][m]["sum"], lc_container2[k][m]["n"]) for m in range(1, 13)]
        ) for k in lc_names
        }
        delta_lc = {k: m_lc1[k] - m_lc2[k] for k in lc_names}

    delta_lc["combined"] = delta_combined
    df = pd.DataFrame(delta_lc)
    df.to_csv(os.path.join(DIRS["results"], f"delta_{fuse.get_filename()}{'_yearly' if yearly else ''}.csv"))


def rad_forc(fuse: FuSE) -> None:
    """
    Calculates the radiative forcing from albedo changes (µW / m^2) according to Wohlfahrt et al. 2021 - eq. 4
    km = monthly mean CACK kernel (austria) (W/m^2)
    ̣delta = monthly difference (albedo) 2003-2032 and 2070-2099
    """
    print(f"  {datetime.now()}: radiative forcing")
    file = "delta_" + fuse.get_filename() + ".csv"
    mask = get_mask()
    y_res, x_res = mask.shape
    df = pd.read_csv(os.path.join(DIRS["results"], file))

    # lc stuff
    p, _ = Data.LC.ds()
    lc_ds = Dataset(p, "r")
    lc = lc_ds[Data.LC.varn()][:].data
    lc_types = Data.LC.lc_types()
    lc_names = list(filter(lambda x: x not in LC_FILTER, lc_types.keys()))

    # figure out the area of one pixel (approx)
    one_pixel = AREA_AUSTRIA / np.sum(mask)

    # rf from albedo changes
    years = list(range(2070, 2101))
    monthly = np.full((len(years), 12, y_res, x_res), np.NaN)
    for i, y in enumerate(years):
        monthly[i, :, :, :] = monthly_cack(mask, y, fuse)
    km = np.nanmean(monthly, axis=0)  # mean cack over year range
    kmi = np.nanmean(km, axis=(1, 2))  # monthly means for all of austria
    sep = {"combined": rfa(kmi, df["combined"].values, AREA_AUSTRIA) * 1e+6}  # type: ignore

    for name in lc_names:
        lv = lc == Data.LC.lc_value(name)
        lc_filter = np.repeat(lv, 12, axis=0)
        cack_lc = km.copy()
        cack_lc[~lc_filter] = np.NaN
        kmi = np.nanmean(cack_lc, axis=(1, 2))
        delta = df[name].values
        sep[name] = rfa(kmi, delta, one_pixel * np.sum(lv)) * 1e+6  # type: ignore

    results = pd.DataFrame([sep])
    results.to_csv(os.path.join(DIRS["results"], f"rf_{fuse.get_filename()}.csv"))
