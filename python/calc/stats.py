#!/usr/bin/env python3
# File Name: stats.py
# Description: Statistics calulation over time
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 01 May 2023 12:49:57
# Last Modified: 11 Dez 2023 11:20:31

# stdlib imports
import os
from datetime import datetime
from enum import Enum
from typing import Any

# dependencies
import numpy as np
import pandas as pd
from netCDF4 import Dataset, num2date

# local imports
from settings import DIRS, LC_FILTER, Data, get_mask
from util import Chunker, Indexer, daily, index, monthly, yearly


def stats(input: np.ndarray, d: Any) -> dict[str, Any]:
    return {
        "time": d,
        "n": np.sum(~np.isnan(input)),
        "sum": np.nansum(input),
        "min": np.nanmin(input),
        "max": np.nanmax(input),
        "mean": np.nanmean(input),
        "std": np.nanstd(input),
        "q0.05": np.nanquantile(input, 0.05),
        "q0.25": np.nanquantile(input, 0.05),
        "q0.50": np.nanquantile(input, 0.5),
        "q0.75": np.nanquantile(input, 0.75),
        "q0.95": np.nanquantile(input, 0.95),
    }


class Interval(Enum):
    MONTHLY = 1
    YEARLY = 2
    DAILY = 3
    SEASONAL = 4

    def __str__(self) -> str:
        match self:
            case Interval.YEARLY:
                return "yearly"
            case Interval.MONTHLY:
                return "monthly"
            case Interval.DAILY:
                return "daily"
            case Interval.SEASONAL:
                return "seasonal"


class Stats():
    """
    Calculates stats for a given variable in a NetCDF dataset over multiple intervals.
    NOTE: uses the Austria mask to filter values outside of Austria.

    - Interval.YEARLY calculates yearly stats
    - Interval.MONTHLY calculates monthly stats
    - Interval.DAILY calculates daily stats
    - Interval.SEASONAL calculates the stats for every month (over all years).

    kwargs:
        - chunks: split the data in n chunks (will only calculate some of the stats)
        - prefix: prefix to use for the outfile
        - suffix: suffix to use for the outfile
    """

    def __init__(self, path: str, var: str, interval: Interval, years: list[int], **kwargs: Any):
        self.chunks: int | None = kwargs["chunks"] if "chunks" in kwargs else None
        self.prefix: str = kwargs["prefix"] if "prefix" in kwargs else "data"
        self.suffix: str = f"_{kwargs['suffix']}" if "suffix" in kwargs else ""

        self.mask = get_mask()
        self.var = var
        self.years = years
        self.start = datetime(self.years[0], 1, 1)
        self.end = datetime(self.years[-1], 12, 31)
        self.interval = interval
        p, _ = Data.LC.ds()
        ds = Dataset(p, "r")
        self.lc = ds[Data.LC.varn()][:].data

        self.ds = Dataset(path, "r")

        time = self.ds["time"]
        dtime = num2date(time[:], time.units, calendar=time.calendar).data
        self.dtime = np.array([datetime.strptime(str(x), "%Y-%m-%d %H:%M:%S") for x in dtime])

    def calc(self) -> None:
        """
        Calculates the stats and writes them to a csv file.
        """
        interval: list[datetime] | list[int]

        match self.interval:
            case Interval.YEARLY:
                interval = yearly(self.start, self.end)
            case Interval.MONTHLY:
                interval = monthly(self.start, self.end)
            case Interval.DAILY:
                interval = daily(self.start, self.end)
            case Interval.SEASONAL:
                interval = list(range(1, 13))

        df = pd.DataFrame()
        lc_types = Data.LC.lc_types()
        lc_names = list(filter(lambda x: x not in LC_FILTER, lc_types.keys()))
        lc_container: dict[str, pd.DataFrame] = {x: df.copy(deep=True) for x in lc_names + ["combined"]}

        dir = os.path.join(DIRS["results"])
        if not os.path.exists(dir):
            os.makedirs(dir)

        for d in interval:
            print(f"{datetime.now()}: {self.prefix} {d}")

            match self.interval:
                case Interval.YEARLY:
                    if isinstance(d, datetime):
                        indices = np.array([x.year == d.year for x in self.dtime])
                    else:
                        raise Exception("Invalid type")
                case Interval.MONTHLY:
                    if isinstance(d, datetime):
                        indices = np.array([(x.month == d.month and x.year == d.year) for x in self.dtime])
                    else:
                        raise Exception("Invalid type")
                case Interval.DAILY:
                    if isinstance(d, datetime):
                        indices = np.array([x == d for x in self.dtime])
                    else:
                        raise Exception("Invalid type")
                case Interval.SEASONAL:
                    if isinstance(d, int):
                        indices = index(self.dtime, Indexer.MONTH, d)  # type: ignore
                        outofrange = np.array([False if x <= self.end and x >= self.start else True for x in self.dtime])
                        indices = np.array(indices)
                        indices[outofrange] = False
                    else:
                        raise Exception("Invalid type")

            # create chunks
            if self.chunks:
                chunker = Chunker(indices, self.chunks)
                chunks = []
                for c in range(0, self.chunks):
                    chunk = chunker.chunk(c)
                    if not (chunk == False).all():
                        chunks.append(chunk)
            else:
                chunks = [indices]

            for i, cc in enumerate(chunks):
                print(f"  {datetime.now()}: chunk {i+1} / {len(chunks)}")
                data = self.ds[self.var][cc, :, :].astype(np.float64)  # convert to float to be able to use NaN
                data[data.mask] = np.NaN  # https://github.com/numpy/numpy/issues/12283

                if len(data) == 0:
                    print("Warning: empty chunk!")
                    continue

                mask = np.repeat(self.mask[np.newaxis, :, :], sum(cc), axis=0)
                data[~mask] = np.NaN
                data = data.data

                # separate
                for name in lc_container.keys():
                    print(f"    {datetime.now()}: {name}")
                    if name == "combined":
                        s = stats(data, d)

                    else:
                        lv = self.lc == Data.LC.lc_value(name)
                        lc_filter = np.repeat(lv, sum(cc), axis=0)
                        alb_lc = data[lc_filter]
                        s = stats(alb_lc, d)

                    if i == 0:
                        lc_container[name] = pd.concat((lc_container[name], pd.DataFrame([s])), ignore_index=True)
                    else:
                        old = lc_container[name].copy(deep=True)
                        ot = old[old["time"] == d]
                        ot.loc[:, "n"] += s["n"]
                        ot.loc[:, "sum"] += s["sum"]
                        ot.loc[:, "min"] = np.minimum(
                            np.nan_to_num(ot["min"], nan=np.inf),
                            np.nan_to_num(s["min"], nan=np.inf)
                        )
                        ot.loc[:, "max"] = np.maximum(
                            np.nan_to_num(ot["max"], nan=0.0),
                            np.nan_to_num(s["max"], nan=0.0)
                        )
                        lc_container[name][lc_container[name]["time"] == d] = ot

                data = None

        # calculate mean and remove uncalculated stuff
        if self.chunks:
            for n, df in lc_container.items():
                temp = df.drop(columns=["mean", "std", "q0.05", "q0.25", "q0.50", "q0.75", "q0.95"])
                temp["mean"] = temp.apply(lambda row: row["sum"] / row["n"], axis=1)
                lc_container[n] = temp

        # write
        sy = self.start.year
        ey = self.end.year

        for name, _ in lc_container.items():
            if name == "combined":
                outfile = os.path.join(dir, f"{self.prefix}_{self.interval}_{sy}-{ey}{self.suffix}.csv")
            else:
                outfile = os.path.join(dir, f"{self.prefix}_{self.interval}_{name}_{sy}-{ey}{self.suffix}.csv")

            lc_container[name].to_csv(outfile)

