#!/usr/bin/env python3
# File Name: model_prep.py
# Description: Model preparation
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 15 Sep 2023 18:10:18
# Last Modified: 22 Sep 2023 11:28:04

# stdlib imports
import json
import os

# dependencies
import lightgbm as lgb
import numpy as np
import pandas as pd
from sklearn.feature_selection import RFECV
from sklearn.model_selection import GridSearchCV, KFold

# local imports
from calc.training_data import ParquetDataset
from settings import CORES, DIRS, RAND, FuSE

DEFAULT_PARAMS = dict(
    boosting_type="gbdt",
    num_leaves=31,
    max_depth=-1,
    learning_rate=0.1,
    n_estimators=100,
    subsample_for_bin=200000,
    objective="regression",
    class_weight=None,
    min_split_gain=0.0,
    min_child_weight=1e-3,
    min_child_samples=20,
    subsample=1.0,
    subsample_freq=0,
    colsample_bytree=1.0,
    reg_alpha=0.0,
    reg_lambda=0.0,
    random_state=RAND,
    n_jobs=CORES,
    importance_type="split",
)

# C(15,5) = 15!/(5!*(15-5)!) = 3003 combinations
PARAM_GRID = dict(
    boosting_type=["gbdt", "goss", "dart"],
    n_estimators=[100, 1000],
    max_depth=[-1, 8, 12],
    num_leaves=[31, 124, 2048],
    min_split_gain=[0, 1e-3, 1e-2],
)


def grid_search(fuse: FuSE, n: int = 2, splits: int = 2) -> None:
    """
    Performs a primitive grid search for the best parameters.
    # https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html
    # https://lightgbm.readthedocs.io/en/latest/Parameters-Tuning.html

    n = number of samples used
    NOTE: this uses the combined training data
    """
    train_ds = ParquetDataset(fuse, "train")
    test_ds = ParquetDataset(fuse, "test")
    r = np.random.default_rng(RAND).choice(len(train_ds), size=n, replace=False)

    x = pd.concat([train_ds[i][1] for i in r])
    x_test = pd.concat([test_ds[i][1] for i in r])
    y = pd.concat([train_ds[i][2] for i in r])
    y_test = pd.concat([test_ds[i][2] for i in r])

    x = pd.concat([x, x_test])
    y = pd.concat([y, y_test])

    booster = lgb.LGBMRegressor(**DEFAULT_PARAMS)  # type: ignore
    cv = KFold(n_splits=splits, shuffle=True, random_state=RAND).split(X=x, y=y)
    gsearch = GridSearchCV(
        estimator=booster,
        param_grid=PARAM_GRID,
        cv=cv,
        verbose=2,
    )

    gsearch = gsearch.fit(X=x, y=y)
    for k, v in gsearch.best_params_.items():
        DEFAULT_PARAMS[k] = v

    print(json.dumps(DEFAULT_PARAMS, indent=2))

    with open(os.path.join(DIRS["train"], "lgbm_params.json"), "w") as f:
        json.dump(DEFAULT_PARAMS, f, indent=2)


def feature_selection(fuse: FuSE, n: int = 2) -> None:
    """
    Feature selection using Recursive feature elimination with cross-validation.
    # https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.RFECV.html#sklearn.feature_selection.RFECV

    n = number of samples used
    NOTE: this uses the combined training data
    """

    with open(os.path.join(DIRS["train"], "lgbm_params.json"), "r") as f:
        params = json.load(f)

    train_ds = ParquetDataset(fuse, "train")
    test_ds = ParquetDataset(fuse, "test")
    r = np.random.default_rng(RAND).choice(len(train_ds), size=n, replace=False)

    x = pd.concat([train_ds[i][1] for i in r])
    x_test = pd.concat([test_ds[i][1] for i in r])
    y = pd.concat([train_ds[i][2] for i in r])
    y_test = pd.concat([test_ds[i][2] for i in r])

    x = pd.concat([x, x_test])
    y = pd.concat([y, y_test])

    booster = lgb.LGBMRegressor(params)
    selector = RFECV(booster, step=1, cv=5)
    selector = selector.fit(X=x, y=y)

    feature_names = train_ds.cont
    ranking = selector.ranking_
    keep = selector.support_

    result = {}
    for i in range(0, len(feature_names)):
        result[feature_names[i]] = {"ranking": int(ranking[i]), "keep": bool(keep[i])}

    print(json.dumps(result, indent=2))

    with open(os.path.join(DIRS["train"], f"lgbm_{fuse.get_filename()}_features.json"), "w") as f:
        json.dump(result, f, indent=2)

