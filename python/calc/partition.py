#!/usr/bin/env python3
# File Name: partition.py
# Description: Partitions data
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 13 Jan 2023 12:26:02
# Last Modified: 14 Feb 2024 09:39:50

# stdlib imports
import glob
import os
import re
from datetime import datetime
from typing import Any

# dependencies
import numpy as np
import pandas as pd
from netCDF4 import Dataset

# local imports
from settings import DIRS, LC_FILTER, Data, FuSE, get_mask
from util import Bounder, ExponentialBounder, FibonacciBounder, daily, read_at_time


class Partitioner:
    """
    Partitioner convenience class.
    """

    def __init__(self, partition: list[Data], by: Data, fuse: FuSE):
        self.partition = partition
        self.by = by
        self.bounds: list[tuple[float, float]] = []
        self.fuse = fuse

        for d in [Data.DOY, Data.LAT, Data.LON, Data.LC]:
            if d in self.partition:
                print(f"WARNING: removing {d} from partitioning data!")
                self.partition.remove(d)

    def create_bounds(self) -> None:
        """
        Create class boundaries based on the partitioning variable.
        """
        match self.by:
            case Data.DSS:
                self.bounds = FibonacciBounder(14).create()
            case Data.ELEV:
                # read elevation and build classes
                path, _ = Data.ELEV.ds()
                ds = Dataset(path, "r")
                elev = ds[Data.ELEV.varn()][0, :, :]
                max_elev = round(np.nanmax(elev), -3)  # round to nearest 1000
                self.bounds = Bounder(0, max_elev, 21).create()
            case Data.CANOPY:
                path, _ = Data.CANOPY.ds()
                ds = Dataset(path, "r")
                canopy = ds[Data.CANOPY.varn()][0, :, :]
                max_canopy = np.nanmax(canopy)
                self.bounds = ExponentialBounder(0, max_canopy, 21).int()
            case Data.LAI:
                self.bounds = ExponentialBounder(0, 10, 21).create(zero=True)
            case _:
                raise Exception(f"Partitioning by '{self.by}' not implemented.")


def partition_data(part: Partitioner, suffix: str = "", years: list[int] = list(range(2002, 2020))) -> None:
    """
    Partitions data into classes.
    The result will be a parquet file for each class.
    Can then be used to calculate statistics.
    NOTE: Values outside of Austria are filtered.
    NOTE: LC will always be included.
    NOTE: not very RAM efficient..
    """
    part.create_bounds()
    fuse_mask = get_mask()

    ds, _ = Data.LC.ds()
    lc_ds = Dataset(ds, "r")
    lc = lc_ds["type"]

    # read static data
    statics: dict[str, Dataset] = {}  # type: ignore
    for d in part.partition + [part.by]:
        ds, static = d.ds(fuse=part.fuse)
        if static:
            try:
                statics[f"{d}"] = Dataset(ds, "r")[f"{d.varn()}"][0, :, :]
            except ValueError:
                statics[f"{d}"] = Dataset(ds, "r")[f"{d.varn()}"][:]

    for i, year in enumerate(years):
        print(f"{datetime.now()}: Partition {part.by}: {i+1} / {len(years)}")

        time_range = daily(datetime(year, 1, 1), datetime(year, 12, 31))

        # read by
        if str(part.by) in statics:
            by = statics[f"{part.by}"].astype(np.float64)
            by[~fuse_mask] = np.NaN
            by[by.mask] = np.NaN
            by = np.repeat(by[np.newaxis, :, :], len(time_range), axis=0)
        else:
            ds_path, _ = part.by.ds(fuse=part.fuse)
            var_name = part.by.varn()
            by = read_at_time(ds_path, var_name, time_range).astype(np.float64)
            fuse_mask_multi = np.repeat(fuse_mask[np.newaxis, :, :], len(time_range), axis=0)
            by[~fuse_mask_multi] = np.NaN
            by[by.mask] = np.NaN
            fuse_mask_multi = None  # type: ignore

        mask = np.isnan(by)

        temp_df = pd.DataFrame()
        temp_df["by"] = by[~mask]
        temp_df["lc"] = np.repeat(
            lc,
            len(time_range), axis=0
        )[~mask]

        by = None

        # read data that should be partitioned
        for d in part.partition:
            ds, static = d.ds(fuse=part.fuse)

            if static:
                var = statics[f"{d}"]
                var[var.mask] = np.NaN
                temp_df[str(d)] = np.repeat(
                    np.array(var, dtype=d.dtype())[np.newaxis, :, :],
                    len(time_range), axis=0
                )[~mask]
            else:
                var = read_at_time(ds, d.varn(), time_range).astype(np.float64)
                var[var.mask] = np.NaN
                temp_df[str(d)] = np.array(var[~mask], dtype=d.dtype())

            var = None

        # use only rows where no value is nan
        temp_df = temp_df[~temp_df.isnull().any(axis=1)]

        # partition data
        for min, max in part.bounds:
            clss = f"[{min}-{max}]"

            outfile = f"partition_{part.by}_{part.fuse.get_filename()}_{clss}_{year}"
            outfile = f"{outfile}.parquet" if suffix == "" else f"{outfile}_{suffix}.parquet"

            outfile = os.path.join(DIRS["partition"], outfile)

            bounded = temp_df[np.logical_and(temp_df["by"] >= min, temp_df["by"] < max)]
            bounded.to_parquet(outfile, engine="pyarrow")


def mean_partition(fuse: FuSE, mean: Data | list[Data], by: Data | None, partition: Data, **kwargs: Any) -> None:
    """
    Calculates means for a partitioned value (`mean`) along a another partioned variable (`by`).
    `by` can be set to None to simply calculate means for a list of variables.

    NOTE: RAM efficient but not very time efficient.

    kwargs:
        years: list of years to include
        suffix: filename suffix
        rmin: minimum value for variable to mean along
        rmax: maximum value for variable to mean along
        n: number of classes for the meaning variable
        zero: add 0 class
    """
    years = kwargs["years"] if "years" in kwargs else list(range(2002, 2020))
    rmin: float = kwargs["rmin"] if "rmin" in kwargs else 0.0
    rmax: float = kwargs["rmax"] if "rmax" in kwargs else 10.0
    zero = kwargs["zero"] if "zero" in kwargs else False
    suffix = kwargs["suffix"] if "suffix" in kwargs else ""
    n: int = kwargs["n"] if "n" in kwargs else 50

    files = glob.glob(
        os.path.join(
            DIRS["partition"], f"partition_{partition}_{fuse.get_filename()}_*{suffix}.parquet"
        )
    )
    files.sort()
    classes: list[str] = [
        re.search(f"partition_{partition}_{fuse.get_filename()}_\\[(.*)\\].*", f).group(1)  # type: ignore
        for f in files
    ]

    def _parse(x: str) -> tuple[float, float]:
        try:
            return (float(x.split("-", 1)[0]), float(x.split("-", 1)[1]))
        except ValueError:
            return (float(x.rsplit("-", 1)[0]), float(x.rsplit("-", 1)[1]))

    classes = list(np.unique(classes))
    partition_classes = [_parse(x) for x in classes]
    partition_classes.sort()

    # create containers (linear)
    bounds = Bounder(rmin, rmax, n).create(zero=zero)
    print(bounds)

    if by:
        if not isinstance(mean, Data):
            raise Exception("Mean should be a of type Data!")
        df = pd.DataFrame(columns=[str(by)] + [f"{x[0]}-{x[1]}" for x in partition_classes])
        df[str(by)] = [x[1] for x in bounds]

        if zero:
            df.loc[0, str(by)] = 0
    else:
        if not isinstance(mean, list):
            raise Exception("Mean should be a list of Data!")
        df = pd.DataFrame(columns=["classes"] + [str(m) for m in mean])
        df["classes"] = partition_classes

    lc_types = Data.LC.lc_types()
    lc_names = list(filter(lambda x: x not in LC_FILTER, lc_types.keys()))
    lc_container = {x: df.copy(deep=True) for x in lc_names}

    for cc, (c0, c1) in enumerate(partition_classes):
        print(f"{datetime.now()}: mean {c0}-{c1}")
        outfile = f"partition_{partition}_{fuse.get_filename()}_[{c0}-{c1}]"
        files = [
            os.path.join(
                DIRS["partition"], f"{outfile}_{x}.parquet" if suffix == "" else f"{outfile}_{x}_{suffix}.parquet"
            )
            for x in years
        ]

        for name in lc_container.keys():
            print(f"  {name}")

            if by:
                for i, (min, max) in enumerate(bounds):
                    sel = [("lc", "==", Data.LC.lc_value(name)), (str(by), ">=", min), (str(by), "<", max)]
                    try:
                        col_mean = pd.read_parquet(files, engine="pyarrow", columns=[str(mean)], filters=sel)  # type: ignore
                    except FileNotFoundError:
                        col_mean = pd.DataFrame()
                    if i == 0 and zero:
                        lc_container[name].loc[df[str(by)] == 0.0, f"{c0}-{c1}"] = np.nanmean(col_mean)  # type: ignore
                    else:
                        lc_container[name].loc[df[str(by)] == max, f"{c0}-{c1}"] = np.nanmean(col_mean)  # type: ignore
            else:
                sel = [("lc", "==", Data.LC.lc_value(name))]

                if isinstance(mean, list):
                    for d in mean:
                        try:
                            col_mean = pd.read_parquet(
                                files, engine="pyarrow", columns=[str(d)], filters=sel  # type: ignore
                            )
                        except FileNotFoundError:
                            col_mean = pd.DataFrame()
                        lc_container[name].loc[cc, str(d)] = np.nanmean(col_mean)  # type: ignore
                else:
                    try:
                        col_mean = pd.read_parquet(files, engine="pyarrow", columns=[str(d)], filters=sel)  # type: ignore
                    except FileNotFoundError:
                        col_mean = pd.DataFrame()
                    lc_container[name].loc[cc, str(mean)] = np.nanmean(col_mean)  # type: ignore

    for name in lc_container.keys():
        if by:
            outfile = f"partition_{partition}_{name}_{fuse.get_filename()}_mean_{mean}_by_{by}"
        else:
            outfile = f"partition_{partition}_{name}_{fuse.get_filename()}_means"

        lc_container[name].to_csv(os.path.join(DIRS["results"], f"{outfile}.csv"))

