#!/usr/bin/env python3
# File Name: model.py
# Description: Model training and testing
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 16 Sep 2023 18:25:59
# Last Modified: 01 Feb 2024 17:55:39

# stdlib imports
import json
import math
import os
from datetime import datetime
from typing import Any

# dependencies
import lightgbm as lgb
import numpy as np
import pandas as pd

# local imports
from calc.training_data import ParquetDataset
from settings import DIRS, LC_FILTER, RAND, Data, FuSE
from util import metrics


class MultiModel():
    """
    A 'multi' model that combines the models of the different landcover types.
    """

    def __init__(self, fuse: FuSE, **kwargs: Any):
        only_snow = kwargs["only_snow"] if "only_snow" in kwargs else False
        self.models: dict[str, lgb.Booster] = {}

        for name, _ in Data.LC.lc_types().items():
            if name in LC_FILTER:
                continue
            self.models[name] = _load_model(fuse, lc=name, only_snow=only_snow)

    def predict(self, conts: pd.DataFrame, lc: str) -> np.ndarray:
        model = self.models[lc]
        return np.array(model.predict(conts))


def _load_model(fuse: FuSE, lc: str, **kwargs: Any) -> lgb.Booster:
    """Loads model"""
    only_snow = kwargs["only_snow"] if "only_snow" in kwargs else False
    filename = os.path.join(DIRS["train"], f"{lc}_lgbm_{fuse.get_filename()}")
    if only_snow:
        filename = filename + "_snow"
    return lgb.Booster(model_file=f"{filename}.txt")


def _dataset(x: pd.DataFrame, y: pd.DataFrame, ds: ParquetDataset, reference: lgb.Dataset | None = None) -> lgb.Dataset:
    """
    Creates LGBM dataset.
    """
    feature_names = list(ds.cont) + list(ds.cat)

    if len(ds.cat) == 0:
        return lgb.Dataset(
            data=x,
            label=y,
            feature_name=feature_names,
            reference=reference,
        )
    else:
        return lgb.Dataset(
            data=x,
            label=y,
            feature_name=feature_names,
            categorical_feature=[ds.n_cont()],
            reference=reference,
        )


def train_single(fuse: FuSE, lc: str, **kwargs: Any) -> None:
    """
    Trains any of the models.

    NOTE: this will always train separate models for every landcover type.

    kwargs:
        max_sample_size: maximum amount of sample data (MB) loaded into memory for testing
        only_snow: whether to train only on snow events
    """
    max_sample_size = kwargs["max_sample_size"] if "max_sample_size" in kwargs else 16000
    only_snow = kwargs["only_snow"] if "only_snow" in kwargs else False

    print(f"{datetime.now()}: Training {lc}")
    filename = os.path.join(DIRS["train"], f"{lc}_lgbm_{fuse.get_filename()}")
    train_ds = ParquetDataset(fuse, "train", lc=lc)
    valid_ds = ParquetDataset(fuse, "valid", lc=lc)

    # train on a subsample (or all of the data, depending on memory)
    max_samples = math.floor(max_sample_size / (train_ds.get_size() + valid_ds.get_size()))
    max_samples = min(max_samples, len(train_ds))
    r = np.random.default_rng(RAND).choice(len(train_ds), size=max_samples, replace=False)
    print(f"{datetime.now()}: Using {max_samples} samples")

    # read x and y
    x = pd.concat([train_ds[i][1] for i in r])
    x_valid = pd.concat([valid_ds[i][1] for i in r])
    y = pd.concat([train_ds[i][2] for i in r])
    y_valid = pd.concat([valid_ds[i][2] for i in r])

    if only_snow:
        # filter events where snow_depth <= 0.1
        no_snow_indices = x["sd"] <= 0.1
        no_snow_indices_valid = x_valid["sd"] <= 0.1

        x = x[~no_snow_indices]
        y = y[~no_snow_indices]
        x_valid = x_valid[~no_snow_indices_valid]
        y_valid = y_valid[~no_snow_indices_valid]

    ds_train = _dataset(x, y, train_ds)
    ds_valid = _dataset(x_valid, y_valid, valid_ds, reference=ds_train)
    feature_names = train_ds.cont + train_ds.cat

    # fit model
    with open(os.path.join(DIRS["train"], "lgbm_params.json"), "r") as f:
        params = json.load(f)

    bst = lgb.train(
        params=params,
        train_set=ds_train,
        valid_sets=[ds_train, ds_valid],
        valid_names=["train", "valid"],
        feature_name=feature_names,
        callbacks=[lgb.log_evaluation(1), lgb.early_stopping(10)],
    )

    print(f"{datetime.now()}: Saving model")
    bst.save_model(f"{filename}{'_snow' if only_snow else ''}.txt")


def test_single(fuse: FuSE, lc: str, **kwargs: Any) -> None:
    """
    Tests a single model.

    NOTE: this will always test the separate models for every landcover type.

    kwargs:
        max_sample_size: maximum amount of sample data (MB) loaded into memory for testing
    """
    max_sample_size = kwargs["max_sample_size"] if "max_sample_size" in kwargs else 16000

    filename = os.path.join(DIRS["train"], f"{lc}_lgbm_{fuse.get_filename()}")
    test_ds = ParquetDataset(fuse, "test", lc=lc)
    num_features = test_ds.n_cat() + test_ds.n_cont()

    # test on a subsample (or all of the data, depending on memory)
    max_samples = math.floor(max_sample_size / (test_ds.get_size()))
    max_samples = min(max_samples, len(test_ds))
    r = np.random.default_rng(RAND).choice(len(test_ds), size=max_samples, replace=False)

    # read x and y
    x = pd.concat([test_ds[i][1] for i in r])
    y = pd.concat([test_ds[i][2] for i in r])

    booster = _load_model(fuse, lc=lc)
    yhat = booster.predict(x)

    df = pd.DataFrame({"real": y.values.ravel(), "pred": yhat})
    m = metrics(df["real"], df["pred"], num_features)
    print(json.dumps(m, indent=2))

    with open(f"{filename}_skill.json", "w") as file:
        json.dump(m, file, indent=2)

    df.to_parquet(f"{filename}_predictions.parquet", engine="pyarrow")


def test_multi(fuse: FuSE, **kwargs: Any) -> None:
    """
    Tests the multi/combined model.

    NOTE: this uses the combined training data

    kwargs:
        max_sample_size: maximum amount of sample data (MB) loaded into memory for testing
    """
    max_sample_size = kwargs["max_sample_size"] if "max_sample_size" in kwargs else 16000

    filename = os.path.join(DIRS["train"], f"lgbm_{fuse.get_filename()}")
    test_ds = ParquetDataset(fuse, "test")
    num_features = test_ds.n_cat() + test_ds.n_cont()

    # test on a subsample (or all of the data, depending on memory)
    max_samples = math.floor(max_sample_size / (test_ds.get_size()))
    max_samples = min(max_samples, len(test_ds))
    r = np.random.default_rng(RAND).choice(len(test_ds), size=max_samples, replace=False)

    cats = pd.concat([test_ds[i][0] for i in r])
    x = pd.concat([test_ds[i][1] for i in r])
    y = pd.concat([test_ds[i][2] for i in r])

    booster = MultiModel(fuse)

    yhat = np.full(len(y), np.NaN)
    for name, value in Data.LC.lc_types().items():
        if name in LC_FILTER:
            continue
        lc_index = cats["lc"] == value
        lc_x = x[lc_index]
        yhat[lc_index] = booster.predict(lc_x, name)

    df = pd.DataFrame({"real": y.values.ravel(), "pred": yhat, "lc": cats.values.ravel()})
    m = metrics(df["real"], df["pred"], num_features)

    print(json.dumps(m, indent=2))

    sep = {"combined": m}
    # separate metrics per lc
    for name, i in Data.LC.lc_types().items():
        if name in LC_FILTER:
            continue
        index = df["lc"] == i
        real = df["real"][index]
        pred = df["pred"][index]

        m = metrics(real, pred, num_features)
        sep[name] = m

    with open(f"{filename}_skill.json", "w") as file:
        json.dump(sep, file, indent=2)

    df.to_parquet(f"{filename}_predictions.parquet", engine="pyarrow")
