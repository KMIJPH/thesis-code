#!/usr/bin/env python3
# File Name: training_data.py
# Description: Training data generation
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 02 Jan 2023 00:32:47
# Last Modified: 01 Feb 2024 17:35:03

# stdlib imports
import glob
import os
from copy import deepcopy
from datetime import datetime
from typing import Any

# dependencies
import numpy as np
import pandas as pd
from netCDF4 import Dataset

# local imports
from settings import DIRS, RAND, Data, FuSE
from util import daily, date2doy, read_at_time


class ParquetDataset():
    """
    Custom Dataset which loads the corresponding file out of a list of files.
    NOTE: Values outside of austria are filtered via SD/DSS

    kwargs:
        device: device to be used on
        lc: landcover type (string name)
    """

    def __init__(self, fuse: FuSE, dataset: str, **kwargs: Any):
        if "lc" in kwargs and kwargs["lc"]:
            lc = kwargs["lc"]
            self.files = glob.glob(
                os.path.join(
                    DIRS["train"], "training_data",
                    f"{lc}_{fuse.get_filename()}_{dataset}_*.parquet"
                )
            )
        else:
            self.files = glob.glob(os.path.join(
                DIRS["train"], "training_data", f"{fuse.get_filename()}_{dataset}_*.parquet")
            )

        self.files.sort()

        test = pd.read_parquet(self.files[0], engine="pyarrow")
        cols: list[str] = list(test.columns)
        cols.sort()
        cols.remove("alb")

        if "lc" in cols:
            cols.remove("lc")
            self.cat = ["lc"]
        else:
            self.cat = []

        self.cont = cols
        self.y = ["alb"]

        self.device = kwargs["device"] if "device" in kwargs else ""

    def n_cont(self) -> int:
        """
        Returns number of continuous variables in the Dataset.
        """
        return len(self.cont)

    def n_cat(self) -> int:
        """
        Returns number of categorical variables in the Dataset.
        """
        return len(self.cat)

    def n_y(self) -> int:
        """
        Returns number of outcome / label / y variables in the Dataset.
        """
        return len(self.y)

    def get_size(self) -> float:
        """
        Returns estimated memory size in MB.
        """
        cats, conts, y = self[0]
        x = pd.concat([cats, conts], axis=1, join="inner")
        return (x.memory_usage(index=True).sum() + y.memory_usage(index=True).sum()) * 1e-6

    def __getitem__(self, idx: int) -> tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
        df = pd.read_parquet(self.files[idx], engine="pyarrow")
        cats = df[self.cat]
        conts = df[self.cont]
        y = df[self.y]
        return cats, conts, y

    def __len__(self) -> int:
        return len(self.files)


def outfile(fuse: FuSE, container_name: str, ds_name: str, n: int) -> str:
    """
    Returns filename for the dataset.
    """
    if container_name == "combined":
        return os.path.join(
            DIRS["train"], "training_data",
            f"{fuse.get_filename()}_{ds_name}_{n}.parquet"
        )
    else:
        return os.path.join(
            DIRS["train"], "training_data",
            f"{container_name}_{fuse.get_filename()}_{ds_name}_{n}.parquet"
        )


def training_data(fuse: FuSE, data: list[Data], **kwargs: Any) -> None:
    """
    Creates training and testing data for a given time range and writes them to parquet.
    Frac can be passed to change the fraction of data used for training.
       e.g: 0.8 -> 80% training, 10% validation, 10% test
    Will only include data inside the boundaries of Austria.
    Will filter out NaNs.
    Shuffles data.

    Data will always include:
        - Albedo
        - SD

    kwargs:
        n: number of days to include
        chunksize: size of a chunk in MB (uncompressed)
        frac: fraction used for training, validation, testing
        include_sd0: includes values where sd == 0
        separate: whether to generate separated training sets per landcover
        all: use all the data available (ignores n)
        lc_filter: landcover types to ignore
    """
    n = kwargs["n"] if "n" in kwargs else 1000
    chunksize = kwargs["chunksize"] if "chunksize" in kwargs else 500
    frac = kwargs["frac"] if "frac" in kwargs else 0.8
    include_sd0 = kwargs["include_sd0"] if "include_sd0" in kwargs else True
    separate = kwargs["separate"] if "separate" in kwargs else False
    all = kwargs["all"] if "all" in kwargs else False
    lc_filter = kwargs["lc_filter"] if "lc_filter" in kwargs else []

    # take random sample from days
    # lai starts at that time
    time_range = np.random.default_rng(RAND).choice(
        np.array(daily(datetime(2002, 7, 4), datetime(2019, 12, 31))),
        size=n,
        replace=False
    )

    if all:
        time_range = np.array(daily(datetime(2002, 7, 4), datetime(2019, 12, 31)))
        np.random.default_rng(RAND).shuffle(time_range)

    # create directory
    if not os.path.exists(os.path.join(DIRS["train"], "training_data")):
        os.makedirs(os.path.join(DIRS["train"], "training_data"))

    # read static variables only once
    statics = {}
    for d in data:
        ds, static = d.ds(fuse=fuse)
        if static:
            try:
                statics[str(d)] = Dataset(ds, "r")[f"{d.varn()}"][0, :, :]
            except ValueError:
                statics[str(d)] = Dataset(ds, "r")[f"{d.varn()}"][:]

    # always include LC
    ds, _ = Data.LC.ds()
    lc_ds = Dataset(ds, "r")
    statics[str(Data.LC)] = lc_ds["type"][0, :, :]

    # meshgrid (included)
    if Data.LON in data or Data.LAT in data:
        x, y = np.meshgrid(statics[str(Data.LON)], statics[str(Data.LAT)])
        statics[str(Data.LON)] = x
        statics[str(Data.LAT)] = y

    cont = {"n": 0, "df": pd.DataFrame()}
    lc_types = Data.LC.lc_types()
    lc_names = list(filter(lambda x: x not in lc_filter, lc_types.keys()))
    lc_container: dict[str, dict[str, Any]] = {x: deepcopy(cont) for x in lc_names + ["combined"]}

    for i, t in enumerate(time_range):
        print(f"{datetime.now()}: Training data {i+1} / {len(time_range)}")

        alb = read_at_time(Data.ALB.ds()[0], Data.ALB.varn(), [t])[0, :, :]
        sd = read_at_time(Data.SD.ds(fuse=fuse)[0], Data.SD.varn(), [t])[0, :, :]
        doy = np.sin(date2doy(t))

        # values outside of Austria are filtered via sd
        # unclassified landcover is filtered
        # boolean arrays
        idx = np.logical_or(
            alb.mask, np.logical_or(
                sd.mask, statics[str(Data.LC)] == 255
            )
        )

        # filter out no_snow data
        if not include_sd0:
            idx = np.logical_or(idx, sd == 0)

        # filter out nans
        for d in data:
            match d:
                case Data.ALB | Data.SD | Data.LC | Data.DOY:
                    continue  # already there
                case _:
                    ds, static = d.ds(fuse=fuse)
                    if static:
                        var = statics[str(d)]
                        idx = np.logical_or(idx, var.mask)
                    else:
                        var = read_at_time(ds, d.varn(), [t])[0, :, :]
                        idx = np.logical_or(idx, var.mask)

        # create dataframe
        temp_df = pd.DataFrame()
        temp_df[str(Data.ALB)] = np.array(alb[~idx])
        temp_df[str(Data.SD)] = np.array(sd[~idx])
        temp_df[str(Data.LC)] = np.array(statics[str(Data.LC)][~idx], dtype=np.uint64)

        for d in data:
            match d:
                case Data.ALB | Data.SD | Data.LC:
                    continue  # already there
                case Data.DOY:
                    temp_df[str(d)] = np.full(temp_df[str(Data.ALB)].shape, doy, dtype=d.dtype())
                case _:
                    ds, static = d.ds(fuse=fuse)
                    if static:
                        var = statics[str(d)]
                        temp_df[str(d)] = np.array(var[~idx], dtype=d.dtype())
                    else:
                        var = read_at_time(ds, d.varn(), [t])[0, :, :]
                        temp_df[str(d)] = np.array(var[~idx], dtype=d.dtype())

        if not separate:
            lc_container["combined"]["df"] = pd.concat((lc_container["combined"]["df"], temp_df))

        # separate per lc
        if separate:
            for name in lc_names:
                lc_df = temp_df[temp_df["lc"] == Data.LC.lc_value(name)]
                lc_df = lc_df.drop(columns=["lc"])
                lc_container[name]["df"] = pd.concat((lc_container[name]["df"], lc_df))

        # write chunk
        for name in lc_container.keys():
            if not separate:
                if name != "combined":
                    continue
            else:
                if name == "combined":
                    continue

            df = lc_container[name]["df"]
            if (df.memory_usage(index=True).sum() * 1e-6) > chunksize or i == len(time_range) - 1:

                # split training and testing
                train: pd.DataFrame
                valid: pd.DataFrame
                test: pd.DataFrame
                train, valid, test = np.split(  # type: ignore
                    df.sample(frac=1, random_state=RAND),
                    [int(frac * len(df)), int((frac + (1 - frac) / 2) * len(df))]
                )

                # write
                train.to_parquet(outfile(fuse, name, "train", lc_container[name]["n"]), engine="pyarrow")
                test.to_parquet(outfile(fuse, name, "test", lc_container[name]["n"]), engine="pyarrow")
                valid.to_parquet(outfile(fuse, name, "valid", lc_container[name]["n"]), engine="pyarrow")
                lc_container[name]["n"] += 1
                lc_container[name]["df"] = pd.DataFrame()

