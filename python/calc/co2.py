#!/usr/bin/env python3
# File Name: co2.py
# Description: Calculate time-dependent emissions equivalent (TDEE)
#   (Converted from MATLAB code provided by Dr. Georg Wohlfahrt)
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 14 Dez 2023 11:09:05
# Last Modified: 28 Dez 2023 17:09:28

# stdlib imports
import os
from datetime import datetime

# dependencies
import numpy as np
import pandas as pd
from netCDF4 import Dataset

# local imports
from calc.radiative_forcing import AREA_AUSTRIA, monthly_cack, rfa
from settings import DIRS, LC_FILTER, Data, FuSE, get_mask

A_IRF = [0.2173, 0.2240, 0.2824, 0.2763]  # Table 5 in Joos et al. (2013)
T_IRF = [394.4, 36.54, 4.304]  # Table 5 in Joos et al. (2013)
KCO2 = 1.76 * 1e-15  # radiative efficiency of CO2 (W m-2 kg-1) (Bright et al., 2016)


def ir(t: np.ndarray) -> np.ndarray:
    """
    Returns impulse-response function; Eq. 5 from Joos et al. (2013)
    t: time vector
    """
    return A_IRF[0] + A_IRF[1] * np.exp(-t / T_IRF[0]) + A_IRF[2] * np.exp(-t / T_IRF[1]) + A_IRF[3] * (np.exp(-t / T_IRF[2]))


def ltm(irf: np.ndarray) -> np.ndarray:
    """
    Returns lower triangular matrix of IRF; Eq. 12 from Bright et al. (2016)
    """
    n = len(irf)
    yco2 = np.zeros((n, n))
    for i in range(0, n):
        if i == 0:
            yco2[i:, i] = irf
        else:
            yco2[i:, i] = irf[:-i]
    return yco2


def tdee(yco2: np.ndarray, rf: np.ndarray) -> np.ndarray:
    """
    Returns time dependent emissions equivalent (kg eCO2 m-2 y-1); Eq. 13 from Bright et al. (2016)
    MATLAB: tdee = kco2^-1 * yco2^-1 * rf;
    """
    return np.linalg.inv(KCO2 * np.eye(len(yco2))) @ np.linalg.inv(yco2) @ rf


def co2(fuse: FuSE) -> None:
    """
    Calculates time-dependent emissions equivalent (TDEE) (Mt eCO2 m-2 y-1)
    """
    def converter(strarr):
        return np.fromstring(strarr[1:-1], sep=' ')

    print(f"  {datetime.now()}: tdee")
    file = "delta_" + fuse.get_filename() + "_yearly.csv"
    mask = get_mask()
    y_res, x_res = mask.shape
    df = pd.read_csv(os.path.join(DIRS["results"], file), index_col=0)

    # figure out the area of one pixel (approx)
    one_pixel = AREA_AUSTRIA / np.sum(mask)

    # lc stuff
    p, _ = Data.LC.ds()
    lc_ds = Dataset(p, "r")
    lc = lc_ds[Data.LC.varn()][:].data
    lc_types = Data.LC.lc_types()
    lc_names = list(filter(lambda x: x not in LC_FILTER, lc_types.keys()))

    # overall
    years = list(range(2020, 2101))
    monthly = np.full((len(years), 12, y_res, x_res), np.NaN)
    for i, y in enumerate(years):
        monthly[i, :, :, :] = monthly_cack(mask, y, fuse)

    km = np.nanmean(monthly, axis=(2, 3))  # monthly cack for every year
    rf = np.array([rfa(km[i], converter(df["combined"][y]), AREA_AUSTRIA) for i, y in enumerate(years)])

    # check for empty years
    # NOTE: empty years meaning the possible missing year at the end (2100) for some GCM/RCM
    t = np.arange(0, len(rf))
    nans = np.isnan(rf)

    tdees = {}
    rfs = {"combined": rf * 1e6}

    irf = ir(t[~nans])
    yco2 = ltm(irf)
    tdee_arr = np.full(len(t), np.NaN)
    tdee_arr[~nans] = tdee(yco2, rf[~nans]) * 1e-9
    tdees["combined"] = tdee_arr

    for name in lc_names:
        rf_lc = np.full(len(years), np.NaN)
        lv = lc == Data.LC.lc_value(name)

        for i, y in enumerate(years):
            lc_filter = np.repeat(lv, 12, axis=0)
            cack_lc = monthly[i, :, :, :].copy()
            cack_lc[~lc_filter] = np.NaN
            km = np.nanmean(cack_lc, axis=(1, 2))
            rf_lc[i] = rfa(km, converter(df[name][y]), one_pixel * np.sum(lv))

        rfs[name] = rf_lc * 1e6
        tdee_arr = np.full(len(t), np.NaN)
        tdee_arr[~nans] = tdee(yco2, rf_lc[~nans]) * 1e-9
        tdees[name] = tdee_arr

    tdeedf = pd.DataFrame(tdees)
    rfdf = pd.DataFrame(rfs)

    tdeedf.to_csv(os.path.join(DIRS["results"], f"tdee_{fuse.get_filename()}.csv"))
    rfdf.to_csv(os.path.join(DIRS["results"], f"rf_{fuse.get_filename()}_yearly.csv"))
