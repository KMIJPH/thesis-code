#!/usr/bin/env python3
# File Name: predict.py
# Description: Albedo prediction
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 23 Feb 2023 15:44:53
# Last Modified: 07 Dez 2023 17:12:28

# stdlib imports
import os
from datetime import datetime
from typing import Any

# dependencies
import numpy as np
import pandas as pd
from cftime import to_tuple
from netCDF4 import Dataset, num2date

# local imports
from calc.model import MultiModel
from settings import DIRS, LC_FILTER, Data, FuSE
from util import date2doy, get_info, leap_year, read_at_time

FILL = np.iinfo(np.uint16).max


def predict(fuse: FuSE, data: list[Data], **kwargs: Any) -> None:
    """
    Predicts albedo.
    NOTE: Values outside of Austria wont be predicted due sd/lc (mask).

    kwargs:
        n: number of days to predict per iteration.
        suffix: suffix to append to the generated filename
        rcp26: force models trained with rcp26 data
        years: year range to predict
    """
    n = kwargs["n"] if "n" in kwargs else 50
    suffix = f"_{kwargs['suffix']}" if "suffix" in kwargs else ""
    rcp26 = kwargs["rcp26"] if "rcp26" in kwargs else False
    years = kwargs["years"] if "years" in kwargs else list(range(2020, 2101))

    if rcp26:
        f26 = FuSE(fuse.fuse_26())
        model = MultiModel(f26)
    else:
        model = MultiModel(fuse)

    outfile = os.path.join(DIRS["output_dir"], f"Albedo_{fuse.get_filename()}{suffix}.nc")

    # meshgrid
    lat_ds, _ = Data.LAT.ds()
    lon_ds, _ = Data.LON.ds()
    lat = Dataset(lat_ds, "r")[f"{Data.LAT.varn()}"][:]
    lon = Dataset(lon_ds, "r")[f"{Data.LON.varn()}"][:]
    x, y = np.meshgrid(lon, lat)

    # read time
    sd_path, _ = Data.SD.ds(fuse=fuse)
    sd_ds = Dataset(sd_path, "r")
    time = sd_ds["time"]
    lat = sd_ds["lat"]
    lon = sd_ds["lon"]
    info = get_info(sd_ds)
    dtime = num2date(time[:], time.units, calendar=time.calendar).data

    # create time range
    time_idx = [to_tuple(t)[0] in years for t in dtime]
    time_range = dtime[time_idx]

    date_string = time_range[0].strftime("%Y-%m-%d %H:%M:%S")
    time_units = f"days since {date_string}"
    time_calendar = time.calendar
    multiplier = 100

    # make dataset
    out = Dataset(outfile, "w", format="NETCDF4")
    out.Conventions = "CF-1.6"
    out.title = "Albedo Austria"
    out.creation_date = str(datetime.now())
    out.comment = "Predicted albedo values Austria, @Joseph Kiem"
    out.createDimension("time", size=len(time_range))
    out.createDimension("lat", size=len(lat))
    out.createDimension("lon", size=len(lon))
    # variables----------------------------------------- #
    crs = out.createVariable(
        "crs", "u2",
        dimensions=(),
        fill_value=None,
        zlib=False
    )
    crs.spatial_ref = info["variables"]["crs"]["spatial_ref"]
    # -------------------------------------------------- #
    time_var  = out.createVariable(
        "time", "u2",
        dimensions=("time"),
        fill_value=None,
        zlib=False
    )
    time_var.set_auto_maskandscale(False)
    time_var.long_name = "time"
    time_var.units = time_units
    time_var.calendar = time_calendar
    time_var.axis = "T"
    # -------------------------------------------------- #
    lat_var  = out.createVariable(
        "lat", "f8",
        dimensions=("lat"),
        fill_value=None,
        zlib=False
    )
    lat_var.set_auto_maskandscale(False)
    lat_var.long_name = "latitude"
    lat_var.units = "degrees_north"
    lat_var.axis = "Y"
    # -------------------------------------------------- #
    lon_var  = out.createVariable(
        "lon", "f8",
        dimensions=("lon"),
        fill_value=None,
        zlib=False
    )
    lon_var.set_auto_maskandscale(False)
    lon_var.long_name = "longitude"
    lon_var.units = "degrees_east"
    lon_var.axis = "X"
    # -------------------------------------------------- #
    alb_var = out.createVariable(
        "bw", "u2",
        dimensions=("time", "lat", "lon"),
        fill_value=FILL,
        zlib=True,
        complevel=4,
        shuffle=True,
        chunksizes=(1, len(lat), len(lon)),
    )
    alb_var.set_auto_maskandscale(False)
    alb_var.long_name = "predicted mean daily albedo"
    alb_var.units = "unitless"
    alb_var.grid_mapping = "crs"
    alb_var.scale_factor = 1 / multiplier

    lat_var[:] = lat[:]
    lon_var[:] = lon[:]

    # iterate over time chunks of len(n) to minimize iterations
    tc = list(range(0, len(time_range), n))
    time_chunks = [
        (t, len(time_range)) if t == tc[-1]
        else (t, t + n)
        for t in tc
    ]

    # read static variables only once
    statics = {}
    for d in data:
        ds, static = d.ds(fuse=fuse)
        if static:
            try:
                statics[str(d)] = Dataset(ds, "r")[f"{d.varn()}"][0, :, :]
            except ValueError:
                statics[str(d)] = Dataset(ds, "r")[f"{d.varn()}"][:]

    for i, t in enumerate(time_chunks):
        print(f"{datetime.now()}: Processing chunk {i+1} of {len(time_chunks)}")

        # prealloc
        predicted = np.full((t[1] - t[0], len(lat), len(lon)), np.NaN)

        print("  Prediction..")
        # create dataframe
        df = pd.DataFrame()

        for d in data:
            match d:
                case Data.LAT:
                    df[str(d)] = np.repeat(y[np.newaxis, :, :], t[1] - t[0], axis=0).ravel()
                case Data.LON:
                    df[str(d)] = np.repeat(x[np.newaxis, :, :], t[1] - t[0], axis=0).ravel()
                case Data.DOY:
                    fun = np.vectorize(date2doy)
                    doy = np.sin(fun(np.array(time_range[t[0]:t[1]])))
                    repeated = doy[:, np.newaxis, np.newaxis] * np.ones(predicted.shape)
                    df[str(d)] = repeated.ravel()
                case Data.LAI_MEAN:
                    ds, _ = d.ds()
                    dataset = Dataset(ds, "r")
                    ref_time = time_range[t[0]: t[1]]
                    # duplicate last day
                    days = [
                        int(date2doy(x) - 2) if leap_year(x.year) and date2doy(x) > 365
                        else int(date2doy(x) - 1) for x in ref_time
                    ]
                    var = dataset[d.varn()][days, :, :]
                    df[str(d)] = var.ravel()
                case _:
                    ds, static = d.ds(fuse=fuse)

                    if static:
                        var = statics[f"{d}"]
                        df[str(d)] = np.repeat(var[np.newaxis, :, :], t[1] - t[0], axis=0).ravel()
                    else:
                        var = read_at_time(ds, d.varn(), time_range[t[0]: t[1]])
                        df[str(d)] = var.ravel()

        # filter NaNs
        nans = df.isna().any(axis=1)
        pred_temp = np.full(len(df), np.NaN)

        # predict
        cont_names = list(df.columns)
        cont_names.sort()
        if "lc" in cont_names:
            cont_names.remove("lc")
            lc_unknown = df["lc"] == 255  # to numpy
            nans = np.logical_or(nans.to_numpy(), lc_unknown.to_numpy())  # type: ignore

        for name, value in Data.LC.lc_types().items():
            if name in LC_FILTER:
                continue
            lc_index = np.logical_and(~nans, df["lc"] == value)
            lc_df = df[lc_index]
            conts = lc_df[cont_names]
            pred_temp[lc_index] = model.predict(conts, name)

        predicted = np.reshape(pred_temp, predicted.shape)

        # just to make sure
        predicted[predicted < 0] = 0.0
        predicted[predicted > 1] = 1.0

        # scale
        okay = ~np.isnan(predicted)
        scaled = np.full((t[1] - t[0], len(lat), len(lon)), FILL)
        scaled[okay] = (predicted[okay] * multiplier).astype(np.uint16)

        # write
        print("  Writing..")
        alb_var[t[0]:t[1], :, :] = scaled
        time_var[t[0]:t[1]] = list(range(t[0], t[1]))

    out.close()
    print(f"Generated netCDF file '{outfile}'")
    return

