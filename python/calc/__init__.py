#!/usr/bin/env python3
# File Name: __init__.py
# Description: Calculations
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 13 Jan 2023 12:25:51
# Last Modified: 14 Dez 2023 16:38:53

from .co2 import co2, ir, ltm, tdee
from .model import MultiModel, test_multi, test_single, train_single
from .partition import Partitioner, mean_partition, partition_data
from .predict import predict
from .radiative_forcing import AREA_AUSTRIA, AREA_EARTH, delta, monthly_cack, rad_forc, rfa
from .stats import Interval, Stats
from .training_data import ParquetDataset, training_data
