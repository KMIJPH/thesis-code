#!/usr/bin/env python3
# File Name: croplands.py
# Description: Croplands (temporary)
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 31 Jan 2024 15:24:00
# Last Modified: 01 Feb 2024 22:54:10

import glob
import json
import os
from datetime import datetime
from tempfile import mkdtemp
from zipfile import ZipFile

# dependencies
import lightgbm as lgb
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from netCDF4 import Dataset
from osgeo import gdal, ogr
from scipy.stats import linregress

# local imports
from calc.stats import stats
from prep.lc import raster_metadata
from settings import DIRS, RAND, Data, FigureSettings, FuSE
from util import daily, metrics, read_at_time

fs = FigureSettings()

# {
#   "adj_r2": 0.09328400004320658,
#   "bias": -0.0007433308996691868,
#   "corr": 0.3480538214818162,
#   "crmsd": 0.09700786494600228,
#   "kge": 0.18730923354800477,
#   "mae": 0.05526618054302067,
#   "me": -0.0007433308996680977,
#   "mse": 0.009411078402208246,
#   "nse": 0.09328416181606136,
#   "pearsonr": 0.3480538214818162,
#   "r2": 0.09328416181606136,
#   "rmse": 0.0970107128218747,
#   "sde": 0.09700786494600226,
#   "std": 0.05244717147074005,
#   "std_ref": 0.1018788642507779
# }


CLC = {
    "non-irrigated arable land": 211,
    "permanently irrigated land": 212,
    "rice fields": 213,
    "vineyards": 221,
    "fruit trees and berry plantations": 222,
    "olive groves": 223,
    "pastures": 231,
    "annual crops associated with permanent crops": 241,
    "complex cultivation patterns": 242,
    "land principally occupied by agriculture": 243,
    "agro-forestry": 244,
}


def train_croplands(fuse: FuSE) -> None:
    files = glob.glob(os.path.join(DIRS["train"], "training_data", f"*_train_dataset_{fuse.get_filename()}.parquet"))
    feature_names = ["sd", "dss", "lai", "elev", "aspect", "slope", "canopy", "lat", "lon"]

    for file in files:
        lc = os.path.basename(file)[0:3]
        print(lc)
        filename = os.path.join(DIRS["train"], f"{lc}_{fuse.get_filename()}")
        train = pd.read_parquet(file, engine="pyarrow")
        x_train = train[feature_names]
        y_train = train["alb"]

        valid = pd.read_parquet(
            os.path.join(
                DIRS["train"], "training_data",
                f"{lc}_valid_dataset_{fuse.get_filename()}.parquet"), engine="pyarrow"
        )
        x_valid = valid[feature_names]
        y_valid = valid["alb"]

        ds_valid = lgb.Dataset(
            data=x_valid,
            label=y_valid,
            feature_name=feature_names,
        )

        ds_train = lgb.Dataset(
            data=x_train,
            label=y_train,
            feature_name=feature_names,
            reference=ds_valid
        )

        with open(os.path.join(DIRS["train"], "lgbm_params.json"), "r") as f:
            params = json.load(f)

        bst = lgb.train(
            params=params,
            train_set=ds_train,
            valid_sets=[ds_train, ds_valid],
            valid_names=["train", "valid"],
            feature_name=feature_names,
            callbacks=[lgb.log_evaluation(1), lgb.early_stopping(10)],
        )
        bst.save_model(f"{filename}.txt")

        test = pd.read_parquet(
            os.path.join(
                DIRS["train"], "training_data",
                f"{lc}_test_dataset_{fuse.get_filename()}.parquet"), engine="pyarrow"
        )
        x = test[feature_names]
        y = test["alb"]
        yhat = bst.predict(x)
        df = pd.DataFrame({"real": y.values.ravel(), "pred": yhat})
        m = metrics(df["real"], df["pred"], len(feature_names))
        print(json.dumps(m, indent=2))

        with open(f"{filename}_skill.json", "w") as f:
            json.dump(m, f, indent=2)


def plot_lc2() -> None:
    output = os.path.join(DIRS["output_dir"], "CLC.tif")
    ds = gdal.Open(output, gdal.GA_ReadOnly)
    lc_ds, _ = Data.LC.ds()
    lat = Dataset(Data.LC.ds()[0], "r")["lat"][:]
    lon = Dataset(Data.LC.ds()[0], "r")["lon"][:]
    lc1 = Dataset(lc_ds, "r")["type"][:, :, :]
    lc2 = ds.ReadAsArray().astype(np.uint16)
    lc_croplands = np.logical_or(lc1 == 12, lc1 == 14)
    nope = lc1.copy()
    nope[~lc_croplands] = 0

    fig = go.Figure()
    fig.add_trace(
        go.Heatmap(
            x=lon.data,
            y=lat.data,
            z=nope[0, :, :].data,
            colorscale=fs.create_colorscale(list(fs.lc_colors.values())),
            showscale=False
        )
    )

    data = np.zeros(nope.shape)

    counter = 1
    for lc in CLC.values():
        lc_spec = lc2 == lc
        lc_spec = lc_spec[np.newaxis, :, :]
        lc_filter1 = np.logical_and(lc_spec, lc_croplands)

        if not np.any(lc_filter1):
            continue

        print(lc)

        data[lc_filter1] = counter
        counter += 1

    x1 = lc1[lc_croplands].size
    x2 = lc1[data != 0].size

    x2 / x1 * 100

    fig.add_trace(
        go.Heatmap(
            x=lon.data,
            y=lat.data,
            z=data[0, :, :],
            colorscale=fs.create_colorscale(list(fs.lc_colors.values())),
            showscale=True
        )
    )


def training_data_croplands(fuse: FuSE) -> None:
    output = os.path.join(DIRS["output_dir"], "CLC.tif")
    ds = gdal.Open(output, gdal.GA_ReadOnly)
    lc_ds, _ = Data.LC.ds()
    lc1 = Dataset(lc_ds, "r")["type"][:, :, :]
    lc2 = ds.ReadAsArray().astype(np.uint16)
    lc_croplands = np.logical_or(lc1 == 12, lc1 == 14)

    year = 2006
    time_range = daily(datetime(year, 1, 1), datetime(year, 12, 31))

    lat = Dataset(Data.LC.ds()[0], "r")["lat"][:]
    lon = Dataset(Data.LC.ds()[0], "r")["lon"][:]

    x, y = np.meshgrid(lon, lat)
    x = x[np.newaxis, :, :]
    y = y[np.newaxis, :, :]

    for lc in CLC.values():
        print(lc)
        lc_spec = lc2 == lc
        lc_spec = lc_spec[np.newaxis, :, :]
        lc_filter1 = np.logical_and(lc_spec, lc_croplands)

        if not np.any(lc_filter1):
            continue

        lc_filter2 = np.repeat(lc_filter1, len(time_range), axis=0)
        alb = read_at_time(Data.ALB.ds()[0], Data.ALB.varn(), time_range)[lc_filter2]
        sd = read_at_time(Data.SD.ds(fuse=fuse)[0], Data.SD.varn(), time_range)[lc_filter2]
        dss = read_at_time(Data.DSS.ds(fuse=fuse)[0], Data.DSS.varn(), time_range)[lc_filter2]
        lai = read_at_time(Data.LAI.ds()[0], Data.LAI.varn(), time_range)[lc_filter2]
        elev = np.repeat(Dataset(Data.ELEV.ds()[0], "r")[Data.ELEV.varn()][:][lc_filter1], len(time_range), axis=0)
        aspect = np.repeat(Dataset(Data.ELEV.ds()[0], "r")[Data.ASPECT.varn()][:][lc_filter1], len(time_range), axis=0)
        slope = np.repeat(Dataset(Data.ELEV.ds()[0], "r")[Data.SLOPE.varn()][:][lc_filter1], len(time_range), axis=0)
        canopy = np.repeat(Dataset(Data.CANOPY.ds()[0], "r")[Data.CANOPY.varn()][:][lc_filter1], len(time_range), axis=0)
        lat = np.repeat(y[lc_filter1], len(time_range), axis=0)
        lon = np.repeat(x[lc_filter1], len(time_range), axis=0)

        store = {
            "alb": alb,
            "sd": sd,
            "dss": dss,
            "lai": lai,
            "elev": elev,
            "aspect": aspect,
            "slope": slope,
            "canopy": canopy,
            "lat": lat,
            "lon": lon,
        }
        df = pd.DataFrame(store)
        df = df.dropna()
        alb = None
        sd = None
        dss = None
        lai = None
        elev = None  # type: ignore
        aspect = None  # type: ignore
        slope = None  # type: ignore
        canopy = None  # type: ignore
        lat = None
        lon = None
        train, valid, test = np.split(  # type: ignore
            df.sample(frac=1, random_state=RAND),
            [int(0.8 * len(df)), int((0.8 + (1 - 0.8) / 2) * len(df))]
        )
        train.to_parquet(  # type: ignore
            os.path.join(DIRS["train"], "training_data",
                         f"{lc}_train_dataset_{fuse.get_filename()}.parquet"), engine="pyarrow"
        )
        test.to_parquet(  # type: ignore
            os.path.join(DIRS["train"], "training_data",
                         f"{lc}_test_dataset_{fuse.get_filename()}.parquet"), engine="pyarrow"
        )
        valid.to_parquet(  # type: ignore
            os.path.join(DIRS["train"], "training_data",
                         f"{lc}_valid_dataset_{fuse.get_filename()}.parquet"), engine="pyarrow"
        )
        df = None  # type: ignore


def rasterize() -> None:
    """
    Rasterizes CLC 2006
    https://www.data.gv.at/katalog/de/dataset/clc2006revised
    """
    # read shapefile
    driverName = "ESRI Shapefile"
    driver = ogr.GetDriverByName(driverName)
    ds = os.path.join(DIRS["shapefile_dir"], "CLC_2006_revised_AT_clip.zip")
    zip = ZipFile(ds)
    temp = mkdtemp()
    zip.extractall(temp)

    shp = driver.Open(os.path.join(temp, "CLC06_rev_AT_clip.shp"))
    layer = shp.GetLayer()

    # get info
    info = raster_metadata()
    lc_ds, _ = Data.LC.ds()
    lc = Dataset(lc_ds, "r")
    _, y, x = lc["type"][:].shape
    lc_gdal = gdal.Open(f"NETCDF:{lc_ds}:type", gdal.GA_ReadOnly)
    transform = lc_gdal.GetGeoTransform()

    # write to tif
    output = os.path.join(DIRS["output_dir"], "CLC.tif")
    target = gdal.GetDriverByName('GTiff').Create(output, x, y, 1, gdal.GDT_Float32)
    target.SetSpatialRef(info["srs"])
    target.SetGeoTransform(transform)
    gdal.RasterizeLayer(target, [1], layer, options=["ATTRIBUTE=Code06_L3"])
    target.FlushCache()
    target = None

    # ds = gdal.Open(output, gdal.GA_ReadOnly)
    # data = ds.ReadAsArray().astype(np.uint16)


def sd_croplands(fuse: FuSE) -> None:
    """
    TEMP
    Calculates SD statistics for croplands.
    """
    sd_ds, _ = Data.SD.ds(fuse=fuse)
    lc_ds, _ = Data.LC.ds()
    lc = Dataset(lc_ds, "r")["type"][:]

    time_range = daily(datetime(2002, 1, 1), datetime(2019, 12, 31))
    store = []

    for d in time_range:
        print(d)
        sd = read_at_time(sd_ds, "HS", [d])
        data = sd[lc == 12]
        s = stats(data, d)
        store.append(s)

    df = pd.DataFrame(store)
    df.to_csv(os.path.join(DIRS["results"], f"croplands_{fuse.get_filename()}.csv"))


def corr(fuse: FuSE) -> None:
    """
    TEMP
    """
    sd_ds, _ = Data.DSS.ds(fuse=fuse)
    lc_ds, _ = Data.LC.ds()
    lc = Dataset(lc_ds, "r")["type"][:]
    lc_mask = lc == 12

    lai_ds, _ = Data.LAI_MEAN.ds()

    store_sd = []
    store_lai = []

    year = 2005
    time_range = daily(datetime(year, 1, 1), datetime(year, 12, 31))
    lc_mask_m = np.repeat(lc_mask, len(time_range), axis=0)

    sd = read_at_time(sd_ds, "dss", time_range)
    temp = sd[lc_mask_m]
    temp[temp.mask] = np.NaN
    store_sd.append(temp.data)

    lai = Dataset(lai_ds, "r")["lai"][:]
    temp = lai[lc_mask_m]
    temp[temp.mask] = np.NaN
    store_lai.append(temp.data)

    x = np.concatenate(store_sd)
    store_sd = []
    y = np.concatenate(store_lai)
    nans = np.isnan(y)
    y = y[~nans]
    x = x[~nans]
    store_lai = []

    slope, intercept, r_value, p_value, std_err = linregress(x, y)

    line = x[::1000]  * slope + intercept
    line[line < 0] = np.NaN

    fig = go.Figure(
        data=[
            go.Scatter(x=x[::1000], y=y[::1000], mode="markers"),
            go.Scatter(x=x[::1000], y=line, mode="lines")
        ],
        layout=dict(
            plot_bgcolor="#ffffff",
            xaxis=dict(title=dict(text="DSS")),
            yaxis=dict(title=dict(text="LAI")),
            annotations=[dict(showarrow=False, x=0.9, y=0.9, xref="paper",
                              yref="paper", text=f"r:{r_value:.3}, p: {p_value}")]
        )
    )
    fs.write(fig, os.path.join(DIRS["figures"], "croplands_dss"))


def plot_sd_croplands(fuse: FuSE) -> None:
    """
    TEMP
    """
    data = os.path.join(DIRS["results"], f"croplands_{fuse.get_filename()}.csv")
    df = pd.read_csv(data, index_col=0)

    x = pd.DatetimeIndex(df["time"]).to_numpy()
    y = df["q0.50"].to_numpy()
    y_upper = df["q0.05"].to_numpy()
    y_lower = df["q0.95"].to_numpy()

    months = np.datetime_as_string(x, unit='M')
    month_integers = np.array([int(month[-2:]) for month in months])
    i = (month_integers >= 11) | (month_integers <= 4)

    y[~i] = np.NaN

    fig = go.Figure()
    fig.add_trace(
        go.Scatter(
            x=x,
            y=y,
            name="median",
            mode="lines"
        )
    )

    fig.add_trace(
        go.Scatter(
            x=np.concatenate((x, x[::-1])),
            y=np.concatenate((y_upper, (y_lower)[::-1])),
            fill="toself",
            line=dict(color="rgba(255,0,0,0)"),
            name="std",
        )
    )

    fig.update_layout(
        plot_bgcolor="#ffffff",
        xaxis=dict(
            range=[x[~i][0], x[~i][-1]]
        ),
        yaxis=dict(
            range=[0, 1]
        )
    )
    fig.update_xaxes(fs.axes_props)
    fig.update_yaxes(fs.axes_props)
    fs.write(fig, os.path.join(DIRS["figures"], "croplands"))
