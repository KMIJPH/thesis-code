#!/usr/bin/env python3
# File Name: test_time.py
# Description: Tests for time functions
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 22 Jul 2023 11:23:07
# Last Modified: 26 Sep 2023 21:52:33

# stdlib imports
import unittest
from datetime import datetime

# dependencies
import numpy as np

# local imports
from util import Indexer, daily, date2doy, date2lst, index, leap_year, monthly, yearly


class Tests(unittest.TestCase):
    def test_daily(self):
        self.assertEqual(daily(datetime(2010, 1, 1), datetime(2010, 1, 2)), [datetime(2010, 1, d) for d in range(1, 3)])
        t1 = daily(datetime(2010, 1, 1), datetime(2020, 1, 2))
        self.assertEqual(t1[0], datetime(2010, 1, 1))
        self.assertEqual(t1[-1], datetime(2020, 1, 2))

    def test_monthly(self):
        self.assertEqual(monthly(datetime(2010, 1, 1), datetime(2010, 1, 3)), [datetime(2010, 1, 1)])
        self.assertEqual(monthly(datetime(2010, 1, 1), datetime(2010, 4, 4)), [datetime(2010, m, 1) for m in range(1, 5)])
        self.assertEqual(
            monthly(datetime(2010, 1, 1), datetime(2020, 12, 31)),
            [datetime(y, m, 1) for y in range(2010, 2021) for m in range(1, 13)]
        )

    def test_yearly(self):
        self.assertEqual(yearly(datetime(2010, 1, 1), datetime(2010, 12, 31)), [datetime(2010, 1, 1)])
        self.assertEqual(yearly(datetime(2010, 1, 1), datetime(2020, 1, 3)), [datetime(y, 1, 1) for y in range(2010, 2021)])
        self.assertEqual(yearly(datetime(1995, 1, 1), datetime(2020, 1, 3)), [datetime(y, 1, 1) for y in range(1995, 2021)])

    def test_index(self):
        t = daily(datetime(2010, 1, 1), datetime(2020, 12, 31))
        n = np.array(t)
        self.assertEqual(
            n[index(t, Indexer.DAY, 1)].tolist(),
            [datetime(y, m, 1) for y in range(2010, 2021) for m in range(1, 13)]
        )
        self.assertEqual(
            n[index(t, Indexer.MONTH, 1)].tolist(),
            [datetime(y, 1, d) for y in range(2010, 2021) for d in range(1, 32)]
        )
        self.assertEqual(
            n[index(t, Indexer.YEAR, 2010)].tolist(),
            daily(datetime(2010, 1, 1), datetime(2010, 12, 31))
        )

    def test_date2doy(self):
        self.assertEqual(
            round(date2doy(datetime(2008, 12, 31, 1, 33, 12)), 4),
            366.0647
        )

        self.assertEqual(
            round(date2doy(datetime(2008, 2, 29, 23, 12, 8)), 4),
            60.9668
        )

        self.assertEqual(
            round(date2doy(datetime(1995, 7, 28, 10, 15, 59)), 4),
            209.4278
        )

    def test_date2lst(self):
        self.assertEqual(
            round(date2lst(datetime(2008, 12, 31, 1, 33, 12)), 4),
            1.5533
        )

        self.assertEqual(
            round(date2lst(datetime(2008, 2, 29, 23, 12, 8)), 4),
            23.2022
        )

        self.assertEqual(
            round(date2lst(datetime(1995, 7, 28, 10, 15, 59)), 4),
            10.2664
        )

    def test_leap_year(self):
        self.assertTrue(leap_year(1924))
        self.assertTrue(leap_year(1964))
        self.assertTrue(leap_year(2004))
        self.assertTrue(leap_year(2020))
        self.assertTrue(leap_year(2068))
        self.assertTrue(leap_year(2096))

        self.assertFalse(leap_year(1913))
        self.assertFalse(leap_year(1963))
        self.assertFalse(leap_year(2003))
        self.assertFalse(leap_year(2009))
        self.assertFalse(leap_year(2077))
        self.assertFalse(leap_year(2100))


if __name__ == "__main__":
    unittest.main()

