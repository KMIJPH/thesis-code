#!/usr/bin/env python3
# File Name: test_stats.py
# Description: Skill tests
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 21 Jul 2023 14:03:11
# Last Modified: 20 Sep 2023 20:46:18

# stdlib imports
import unittest

# dependencies
import numpy as np

# local imports
from util import adj_r2, bias, corr, crmsd, kge, mae, me, mse, nse, pearsonr, r2, rmse, sde

df1 = {
    "real": np.array([1, 0.9, 0.9, 0.8]),
    "pred": np.array([1, 0.9, 0.9, 0.8]),
}

df2 = {
    "real": np.array([1, 0.9, 0.9, 0.8, 0.6, 0.6, 0.8]),
    "pred": np.array([1, 1, 1, 0.8, 0.7, 0.6, 0.8]),
}

df3 = {
    "real": np.array([34, 37, 44, 47, 48, 48, 46, 43, 32, 27, 26, 24]),
    "pred": np.array([37, 40, 46, 44, 46, 50, 45, 44, 34, 30, 22, 23]),
}


class Tests(unittest.TestCase):
    def test_mse(self):
        self.assertEqual(mse(df1["real"], df1["pred"]), 0)
        self.assertAlmostEqual(mse(df2["real"], df2["pred"]), 0.00428571)
        self.assertAlmostEqual(mse(df3["real"], df3["pred"]), 5.91666666)

    def test_rmse(self):
        self.assertEqual(rmse(df1["real"], df1["pred"]), 0)
        self.assertAlmostEqual(rmse(df2["real"], df2["pred"]), 0.06546536)
        self.assertAlmostEqual(rmse(df3["real"], df3["pred"]), 2.43241991)

    def test_r2(self):
        self.assertEqual(r2(df1["real"], df1["pred"]), 1.0)
        self.assertAlmostEqual(r2(df2["real"], df2["pred"]), 0.78571428)
        self.assertAlmostEqual(r2(df3["real"], df3["pred"]), 0.92282608)

    def test_adj_r2(self):
        self.assertEqual(adj_r2(df1["real"], df1["pred"], 1), 1.0)
        self.assertAlmostEqual(adj_r2(df2["real"], df2["pred"], 2), 0.67857142)
        self.assertAlmostEqual(adj_r2(df3["real"], df3["pred"], 3), 0.89388586)

    def test_corr(self):
        self.assertAlmostEqual(corr(df1["real"], df1["pred"]), 1.0)
        self.assertAlmostEqual(corr(df1["real"], np.flip(df1["pred"])), -1.0)
        self.assertAlmostEqual(corr(df2["real"], df2["pred"]), 0.94387980)
        self.assertAlmostEqual(corr(df3["real"], df3["pred"]), 0.96346836)

    def test_bias_centered_rms(self):
        b = bias(df1["real"], df1["pred"])
        c = crmsd(df1["real"], df1["pred"])
        m = mse(df1["real"], df1["pred"])
        self.assertAlmostEqual(b ** 2 + c ** 2, m)

        b = bias(df2["real"], df2["pred"])
        c = crmsd(df2["real"], df2["pred"])
        m = mse(df2["real"], df2["pred"])
        self.assertAlmostEqual(b ** 2 + c ** 2, m)

        b = bias(df3["real"], df3["pred"])
        c = crmsd(df3["real"], df3["pred"])
        m = mse(df3["real"], df3["pred"])
        self.assertAlmostEqual(b ** 2 + c ** 2, m)

    def test_nse(self):
        self.assertAlmostEqual(nse(df1["real"], df1["pred"]), 1.0)
        self.assertAlmostEqual(nse(df2["real"], df2["pred"]), 0.78571428)
        self.assertAlmostEqual(nse(df3["real"], df3["pred"]), 0.92282608)

    def test_kge(self):
        self.assertAlmostEqual(kge(df1["real"], df1["pred"]), 1.0)
        self.assertAlmostEqual(kge(df2["real"], df2["pred"]), 0.90225287)
        self.assertAlmostEqual(kge(df3["real"], df3["pred"]), 0.95699475)

    def test_mae(self):
        self.assertAlmostEqual(mae(df1["real"], df1["pred"]), 0.0)
        self.assertAlmostEqual(mae(df2["real"], df2["pred"]), 0.04285714)
        self.assertAlmostEqual(mae(df3["real"], df3["pred"]), 2.25)

    def test_me(self):
        self.assertAlmostEqual(me(df1["real"], df1["pred"]), 0.0)
        self.assertAlmostEqual(me(df2["real"], df2["pred"]), -0.04285714)
        self.assertAlmostEqual(me(df3["real"], df3["pred"]), -0.41666666)

    def test_pearsonr(self):
        self.assertAlmostEqual(pearsonr(df1["real"], df1["pred"]), 1.0)
        self.assertAlmostEqual(pearsonr(df1["real"], np.flip(df1["pred"])), -1.0)
        self.assertAlmostEqual(pearsonr(df2["real"], df2["pred"]), 0.94387980)
        self.assertAlmostEqual(pearsonr(df3["real"], df3["pred"]), 0.96346836)

    def test_sde(self):
        self.assertAlmostEqual(sde(df1["real"], df1["pred"]), 0.0)
        self.assertAlmostEqual(sde(df2["real"], df2["pred"]), 0.04948716)
        self.assertAlmostEqual(sde(df3["real"], df3["pred"]), 2.39646730)


if __name__ == "__main__":
    unittest.main()

