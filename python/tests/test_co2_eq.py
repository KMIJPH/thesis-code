#!/usr/bin/env python3
# File Name: test_co2_eq.py
# Description:
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 14 Dez 2023 11:51:33
# Last Modified: 14 Dez 2023 15:23:35

# dependencies
# stdlib imports
import unittest

import numpy as np

# local imports
from calc import AREA_AUSTRIA, AREA_EARTH, ir, ltm, tdee


class Tests(unittest.TestCase):
    def test_ir(self):
        self.assertTrue(
            np.array_equal(
                ir(np.array([0])), np.array([1.0])
            )
        )
        self.assertTrue(
            np.array_equal(
                np.round(ir(np.array([1])), decimals=4), np.array([0.9345])
            )
        )
        self.assertTrue(
            np.array_equal(
                np.round(ir(np.array([2])), decimals=4), np.array([0.8811])
            )
        )

    def test_tdee(self):
        t = np.arange(0, 5)  # time vector (y)
        km = 130  # avg CACK of earth
        delta_albedo = np.linspace(0, -0.0075, len(t))  # assume linear decrease over years
        rf_albedo = km * delta_albedo * (AREA_AUSTRIA / AREA_EARTH)

        irf = ir(t)
        yco2 = ltm(irf)
        res = tdee(yco2, rf_albedo)

        self.assertTrue(
            np.array_equal(
                res.astype(int), np.array([0, -22773776532, -24264886286, -25578436245, -26740984536])
            )
        )


if __name__ == "__main__":
    unittest.main()


