#!/usr/bin/env python3
# File Name: test_chunker.py
# Description: Chunker tests
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 10 Sep 2023 11:44:02
# Last Modified: 20 Sep 2023 09:29:29

# stdlib imports
import unittest

# dependencies
import numpy as np

# local imports
from util import Chunker


class Tests(unittest.TestCase):
    def test_chunker(self):
        indices = np.array([True, False, False, True, True, True, False, False, True, True])
        c = Chunker(indices, 2)

        self.assertTrue(
            np.array_equal(
                c.chunk(0), np.array([True, False, False, True, True, False, False, False, False, False])
            )
        )
        self.assertTrue(
            np.array_equal(
                c.chunk(1), np.array([False, False, False, False, False, True, False, False, True, True])
            )
        )


if __name__ == "__main__":
    unittest.main()

