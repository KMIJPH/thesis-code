#!/usr/bin/env python3
# File Name: test_bounder.py
# Description: Bounder tests
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 14 Sep 2023 14:29:30
# Last Modified: 05 Dez 2023 13:39:26

# stdlib imports
import unittest

# local imports
from util import Bounder, ExponentialBounder, FibonacciBounder


class Tests(unittest.TestCase):
    def test_chunker(self):
        b = Bounder(0, 10, 11)
        self.assertEqual(
            b.create(zero=True),
            [
                (0.0, 1e-6),
                (1e-6, 1.0),
                (1.0, 2.0),
                (2.0, 3.0),
                (3.0, 4.0),
                (4.0, 5.0),
                (5.0, 6.0),
                (6.0, 7.0),
                (7.0, 8.0),
                (8.0, 9.0),
                (9.0, 10.0)
            ]
        )

        self.assertEqual(
            b.create(),
            [
                (0.0, 1.0),
                (1.0, 2.0),
                (2.0, 3.0),
                (3.0, 4.0),
                (4.0, 5.0),
                (5.0, 6.0),
                (6.0, 7.0),
                (7.0, 8.0),
                (8.0, 9.0),
                (9.0, 10.0)
            ]
        )

        b = FibonacciBounder(11)
        self.assertEqual(
            b.create(),
            [
                (0.0, 1.0),
                (1.0, 2.0),
                (2.0, 3.0),
                (3.0, 5.0),
                (5.0, 8.0),
                (8.0, 13.0),
                (13.0, 21.0),
                (21.0, 34.0),
                (34.0, 55.0),
                (55.0, 89.0),
                (89.0, 144.0)
            ]
        )

        b = ExponentialBounder(0, 10, 11)
        self.assertEqual(
            b.create(),
            [
                (0.0, 0.27),
                (0.27, 0.62),
                (0.62, 1.05),
                (1.05, 1.61),
                (1.61, 2.32),
                (2.32, 3.22),
                (3.22, 4.36),
                (4.36, 5.81),
                (5.81, 7.65),
                (7.65, 10.0)
            ]
        )

        b = ExponentialBounder(0, 10, 11)
        self.assertEqual(
            b.int(),
            [
                (0.0, 1.0),
                (1.0, 2.0),
                (2.0, 3.0),
                (3.0, 4.0),
                (4.0, 6.0),
                (6.0, 8.0),
                (8.0, 10.0)
            ]
        )


if __name__ == "__main__":
    unittest.main()

