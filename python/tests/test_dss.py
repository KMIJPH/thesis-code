#!/usr/bin/env python3
# File Name: test_dss.py
# Description: Days since snowfall tests
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 06 Jan 2023 10:13:23
# Last Modified: 20 Sep 2023 14:48:39

# stdlib imports
import unittest

# dependencies
import numpy as np
from rustlib import snow

# local imports
from prep.dss import FILL


class Tests(unittest.TestCase):
    def test_dss(self):
        """
        Test cases for 'days_since_snowfall'
        """

        self.assertTrue(
            np.array_equal(
                snow.days_since_snowfall(np.array([np.NaN, 0, 0, 0, 1, 2, 3, 4, 3, 3, 3]), FILL),
                np.array([FILL, FILL, FILL, 0, 0, 0, 0, 1, 2, 3])
            )
        )
        self.assertTrue(
            np.array_equal(
                snow.days_since_snowfall(np.array([0, 0, 0, 0, 1, np.NaN, 1, 1, 1, 2, 2]), FILL),
                np.array([1, 2, 3, 0, FILL, FILL, FILL, FILL, 0, 1]),
            )
        )
        self.assertTrue(
            np.array_equal(
                snow.days_since_snowfall(np.array([0, 0, 0, 0, 1, np.NaN, 1, 1, 1, 2, 2]), FILL),
                np.array([1, 2, 3, 0, FILL, FILL, FILL, FILL, 0, 1]),
            )
        )
        self.assertTrue(
            np.array_equal(
                snow.days_since_snowfall(np.array([0, 0, 0, 0, 0, 0, 1, 1, 1, 2, np.NaN]), FILL),
                np.array([1, 2, 3, 4, 5, 0, 1, 2, 0, FILL]),
            )
        )
        self.assertTrue(
            np.array_equal(
                snow.days_since_snowfall(np.array([0, 0, 0, 0, 0, 0, 1, 1, 1, 2, np.NaN]), FILL),
                np.array([1, 2, 3, 4, 5, 0, 1, 2, 0, FILL]),
            )
        )
        self.assertTrue(
            np.array_equal(
                snow.days_since_snowfall(np.array([0, 0, 0, 0, 0, 0, 1, 1, 1, 2, np.NaN]), FILL),
                np.array([1, 2, 3, 4, 5, 0, 1, 2, 0, FILL]),
            )
        )
        self.assertTrue(
            np.array_equal(
                snow.days_since_snowfall(np.array([0, 0, 0, np.NaN, 0, 0, 1, np.NaN, 0, 0, 1]), FILL),
                np.array([1, 2, FILL, FILL, FILL, 0, FILL, FILL, FILL, 0]),
            )
        )
        self.assertTrue(
            np.array_equal(
                snow.days_since_snowfall(np.array([0.0, 0.0, 0.1, 0.3, 0.4, 0.4, 0.0]), FILL),
                np.array([1, 0, 0, 0, 1, 2]),
            )
        )


if __name__ == "__main__":
    unittest.main()

