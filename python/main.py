#!/usr/bin/env python3
# File Name: main.py
# Description: Main script
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: Thu 14 Jul 2022 09:54:35 AM CEST
# Last Modified: 14 Feb 2024 09:35:05

# stdlib imports
import argparse

# local imports
import calc
import plot
import prep
import settings
from calc import Interval, Partitioner
from settings import LC_FILTER, Data


def main() -> None:
    parser = argparse.ArgumentParser("thesis")

    parser.add_argument(
        "--alb",
        help="calculates albedo data and writes the output to the output directory",
        default=False, action="store_true"
    )

    parser.add_argument(
        "--bess",
        help="processes BESS data and writes the output to the output directory",
        default=False, action="store_true"
    )

    parser.add_argument(
        "--dem",
        help="processes DEM data and writes the output to the output directory",
        default=False, action="store_true"
    )

    parser.add_argument(
        "--dss",
        help="""calculates days since snowfall for a given FuSE file and writes the output to the output directory.
        A FuSE filename has to be provided.""",
        default=False, action="store_true"
    )

    parser.add_argument(
        "--fuse",
        help="""processes a FuSE file and writes the output to the output directory.
        A FuSE filename has to be provided.""",
        default=False, action="store_true"
    )

    parser.add_argument(
        "--lai",
        help="interpolates LAI data and writes the output to the output directory",
        default=False, action="store_true"
    )

    parser.add_argument(
        "--canopy",
        help="processes canopy data and writes the output to the output directory",
        default=False, action="store_true"
    )

    parser.add_argument(
        "--lc",
        help="processes Landcover data and writes the output to the output directory",
        default=False, action="store_true"
    )

    parser.add_argument(
        "--cack",
        help="processes CACK kernel and writes the output to the output directory",
        default=False, action="store_true"
    )

    parser.add_argument(
        "--shp",
        help="""creates the study area shapefile in the output directory.
        A FuSE filename has to be provided.""",
        default=False, action="store_true"
    )

    parser.add_argument(
        "--partition",
        help="""partitions data into classes and writes the partitions to parquet files.
        A FuSE filename has to be provided.""",
        default=False, action="store_true"
    )

    parser.add_argument(
        "--train",
        help="""writes training/test/validation data to the output directory and trains model(s).
        A FuSE filename has to be provided.""",
        default=False, action="store_true"
    )

    parser.add_argument(
        "--predict",
        help="""predicts albedo for a given fuse file.
        A FuSE filename has to be provided.""",
        default=False, action="store_true"
    )

    parser.add_argument(
        "--stats",
        help="""calculates stats and writes them csv files.
        A FuSE filename has to be provided.""",
        default=False, action="store_true"
    )

    parser.add_argument(
        "--rf",
        help="""calculates radiative forcing.
        A FuSE filename has to be provided.""",
        default=False, action="store_true"
    )

    parser.add_argument(
        "--plot",
        help="""saves plots to the figure directory.
        A FuSE filename has to be provided.""",
        default=False, action="store_true"
    )

    parser.add_argument(
        'files',
        help="input file(s) needed for some operations (FuSE)",
        nargs='*',
        default=[]
    )

    args = parser.parse_args()

    # creates output folders
    settings.create_output_folders()
    settings.write_dirs()

    if args.alb:
        prep.calculate_albedo()

    if args.bess:
        prep.prepare_bess()

    if args.dem:
        prep.prepare_dem()

    if args.lai:
        prep.prepare_lai()
        prep.mean_lai()

    if args.cack:
        prep.prepare_cack()

        if len(args.files) > 0:
            for file in args.files:
                print(f"File = {file}")
                fuse = settings.FuSE(file)
                prep.fuse_cack(fuse)
        else:
            raise Exception("No input file provided.")

    if args.canopy:
        prep.prepare_canopy()

    if args.lc:
        prep.lc_mode()

    if args.fuse:
        if len(args.files) > 0:
            for file in args.files:
                print(f"File = {file}")
                fuse = settings.FuSE(file)
                prep.prepare_fuse(fuse)
                prep.write_mask(fuse)

                data = Data.SD
                ds, _ = data.ds(fuse=fuse)
                var = data.varn()
                calc.Stats(
                    ds, var, Interval.YEARLY, [2000, 2100], prefix=str(data), suffix=fuse.get_filename()
                ).calc()
        else:
            raise Exception("No input file provided.")

    if args.dss:
        if len(args.files) > 0:
            for file in args.files:
                print(f"File = {file}")
                fuse = settings.FuSE(file)
                prep.calc_dss(fuse)

                data = Data.DSS
                ds, _ = data.ds(fuse=fuse)
                var = data.varn()
                calc.Stats(
                    ds, var, Interval.YEARLY, [2000, 2100], prefix=str(data), suffix=fuse.get_filename()
                ).calc()
        else:
            raise Exception("No input file provided.")

    if args.shp:
        if len(args.files) > 0:
            file = settings.FuSE(args.files[0]).get_path()
            shp = settings.StudyArea()
            shp2 = settings.BiggerStudyArea()
            prep.write_study_area(file, shp)
            prep.write_study_area(file, shp2)
        else:
            raise Exception("No input file provided.")

    if args.partition:
        if len(args.files) > 0:
            for file in args.files:
                print(f"File = {file}")
                fuse = settings.FuSE(file)
                # DSS
                calc.partition_data(
                    Partitioner(partition=[Data.ALB, Data.SD], by=Data.DSS, fuse=fuse)
                )
                calc.mean_partition(
                    fuse, mean=Data.ALB, by=Data.SD, partition=Data.DSS,
                    n=20, zero=True, rmax=2.5  # q0.95
                )

                # LAI
                calc.partition_data(
                    Partitioner(partition=[Data.ALB, Data.SD], by=Data.LAI, fuse=fuse),
                    years=list(range(2003, 2020))
                )
                calc.mean_partition(
                    fuse, mean=Data.ALB, by=Data.SD, partition=Data.LAI,
                    years=list(range(2003, 2020)),
                    n=20, zero=True, rmax=2.5
                )

                # CANOPY
                calc.partition_data(
                    Partitioner(partition=[Data.ALB, Data.SD], by=Data.CANOPY, fuse=fuse),
                )
                calc.mean_partition(
                    fuse, mean=Data.ALB, by=Data.SD, partition=Data.CANOPY,
                    n=20, zero=True, ramx=2.6
                )
        else:
            raise Exception("No input file provided.")

    if args.train:
        if len(args.files) > 0:
            for file in args.files:
                print(f"File = {file}")
                fuse = settings.FuSE(file)
                # combined training data (optimization & feature selection)
                calc.training_data(
                    fuse,
                    data=[
                        Data.ALB,
                        Data.ASPECT,
                        Data.CANOPY,
                        Data.DSS,
                        Data.ELEV,
                        Data.LAI,
                        Data.LAT,
                        Data.LC,
                        Data.LON,
                        Data.SD,
                        Data.SLOPE,
                    ],
                    n=1000,
                )

                # separate training data
                calc.training_data(
                    fuse,
                    data=[
                        Data.ALB,
                        Data.ASPECT,
                        Data.CANOPY,
                        Data.DSS,
                        Data.ELEV,
                        Data.LAI,
                        Data.LAT,
                        Data.LC,
                        Data.LON,
                        Data.SD,
                        Data.SLOPE,
                    ],
                    all=True,
                    separate=True,
                )

                for name, _ in Data.LC.lc_types().items():
                    if name in LC_FILTER:
                        continue
                    calc.train_single(fuse, lc=name, max_sample_size=12000)
                    calc.test_single(fuse, lc=name, max_sample_size=8000)
                    # calc.train_single(fuse, lc=name, max_sample_size=12000, only_snow=True)

                calc.test_multi(fuse, max_sample_size=8000)

        else:
            raise Exception("No input file provided.")

    if args.predict:
        if len(args.files) > 0:
            for file in args.files:
                print(f"File = {file}")
                fuse = settings.FuSE(file)
                # predict modis era
                calc.predict(
                    fuse,
                    data=[
                        Data.ASPECT,
                        Data.CANOPY,
                        Data.DSS,
                        Data.ELEV,
                        Data.LAI,
                        Data.LAT,
                        Data.LON,
                        Data.LC,
                        Data.SD,
                        Data.SLOPE,
                    ],
                    rcp26=True,
                    suffix="pred_modis",
                    years=list(range(2003, 2020)),
                    n=80,
                )

                # predict modis era with mean LAI
                calc.predict(
                    fuse,
                    data=[
                        Data.ASPECT,
                        Data.CANOPY,
                        Data.DSS,
                        Data.ELEV,
                        Data.LAI_MEAN,
                        Data.LAT,
                        Data.LON,
                        Data.LC,
                        Data.SD,
                        Data.SLOPE,
                    ],
                    rcp26=True,
                    suffix="pred_modis_lai_mean",
                    years=list(range(2003, 2020)),
                    n=80,
                )

                # predict future
                calc.predict(
                    fuse,
                    data=[
                        Data.ASPECT,
                        Data.CANOPY,
                        Data.DSS,
                        Data.ELEV,
                        Data.LAI_MEAN,
                        Data.LAT,
                        Data.LON,
                        Data.LC,
                        Data.SD,
                        Data.SLOPE,
                    ],
                    rcp26=True,
                    suffix="pred",
                    n=80,
                )
        else:
            raise Exception("No input file provided.")

    if args.stats:
        # modis era data
        for data in [Data.ALB, Data.LAI]:
            ds, _ = data.ds()
            var = data.varn()
            calc.Stats(ds, var, Interval.YEARLY, [2002, 2019], prefix=str(data)).calc()
            if data == Data.ALB:
                calc.Stats(ds, var, Interval.SEASONAL, [2003, 2019], prefix=str(data)).calc()

        if len(args.files) > 0:
            for file in args.files:
                print(f"File = {file}")
                fuse = settings.FuSE(file)

                # modis era data (FuSE dependent)
                for data in [Data.ALB_PRED_MODIS_ERA, Data.ALB_PRED_MODIS_ERA_LAI_MEAN]:
                    ds, _ = data.ds(fuse=fuse)
                    var = data.varn()
                    try:
                        calc.Stats(
                            ds, var, Interval.YEARLY, [2002, 2019],
                            prefix=str(data), suffix=fuse.get_filename()
                        ).calc()
                        calc.Stats(
                            ds, var, Interval.SEASONAL, [2003, 2019],
                            prefix=str(data), suffix=fuse.get_filename(),
                        ).calc()
                    except FileNotFoundError:
                        continue

                # predicted albedo
                data = Data.ALB_PRED
                ds, _ = data.ds(fuse=fuse)
                var = data.varn()
                calc.Stats(
                    ds, var, Interval.YEARLY, [2020, 2100],
                    prefix=str(data), suffix=fuse.get_filename(),
                ).calc()
        else:
            raise Exception("No input file provided.")

    if args.rf:
        if len(args.files) > 0:
            for file in args.files:
                print(f"File = {file}")
                fuse = settings.FuSE(file)
                calc.delta(fuse, list(range(2003, 2033)), list(range(2070, 2100)), yearly=False)
                calc.delta(fuse, list(range(2003, 2020)), list(range(2020, 2101)), yearly=True)
                calc.rad_forc(fuse)
                calc.co2(fuse)

        else:
            raise Exception("No input file provided.")

    if args.plot:
        plot.lc_stats()
        plot.fuse_comparison()
        plot.rad_forc()
        plot.co2()
        plot.yearly("alb_pred", -5)
        plot.yearly("sd", -3, filt="2000-2100")
        plot.yearly("dss", 0, filt="2000-2100")

        if len(args.files) > 0:
            for file in args.files:
                print(f"File = {file}")
                fuse = settings.FuSE(file)

                plot.mean_partition_by(fuse, mean=Data.ALB, by=Data.SD, partition=Data.DSS)
                plot.mean_partition_by(fuse, mean=Data.ALB, by=Data.SD, partition=Data.LAI)
                plot.mean_partition_by(fuse, mean=Data.ALB, by=Data.SD, partition=Data.CANOPY)
                plot.gra_mxf(fuse)
                plot.model_stats(fuse)
                plot.seasonal(fuse)
                plot.yearly_lc(fuse)
                # plot.feature_importance(fuse, False)
                # plot.feature_importance(fuse, True)
        else:
            raise Exception("No input file provided.")


if __name__ == "__main__":
    main()
