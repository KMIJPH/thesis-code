#!/usr/bin/env python3
# File Name: info.py
# Description: Convenience functions to retrieve and write information about a netCDF file.
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: Thu 14 Jul 2022 09:54:35 AM CEST
# Last Modified: 20 Sep 2023 14:53:01

# stdlib imports
from typing import Any

# dependencies
from netCDF4 import Dataset


def _get_ncvars(data: Dataset) -> dict:
    """
    Returns the variables for a given dataset.
    """
    variables: dict = data.variables
    vardict = {}
    for variable in variables:
        vardict[variable] = _get_attrs(data, variable)

    return vardict


def _get_ncdims(data: Dataset) -> dict:
    """
    Returns the dimensions for a given dataset.
    """
    dimensions: dict = data.dimensions
    dimdict = {}
    for key in dimensions:
        dimension = dimensions[key]

        name: str = dimension.name
        size: tuple = dimension.size
        unlimited: bool = dimension.isunlimited()

        dimdict[name] = {
            "size": size,
            "unlimited": unlimited,
        }

    return dimdict


def _get_attrs(data: Dataset, variable: str) -> dict[str, Any]:
    """
    Returns the attributes for a variable in a dataset.
    """
    # get the associated dynamic attributes
    attrs = data.variables[variable].ncattrs()
    attrdict: dict[str, Any] = {}
    for key in attrs:
        value = data.variables[variable].getncattr(key)
        # trx to convert value to float
        try:
            number = float(value)
            # convert to int if it is an integer
            if number.is_integer():
                number = int(number)
            attrdict[key] = number
        # is ndarray
        except TypeError:
            attrdict[key] = value.tolist()
        # else is string
        except ValueError:
            attrdict[key] = str(value)

    # get the static attributes
    datatype: str = str(data.variables[variable].datatype)
    shape: tuple = data.variables[variable].shape
    dimensions: dict = data.variables[variable].dimensions

    attrdict["datatype"] = datatype
    attrdict["shape"] = shape
    attrdict["dimensions"] = dimensions

    return attrdict


def _get_ncattrs(data: Dataset) -> dict[str, Any]:
    """
    Returns the attributes for a given dataset.
    """
    # these should all be dynamic
    attrs = data.ncattrs()
    attrdict: dict[str, Any] = {}
    for key in attrs:
        value = data.getncattr(key)
        # trx to convert value to float
        try:
            number = float(value)
            # convert to int if it is an integer
            if number.is_integer():
                number = int(number)
            attrdict[key] = number
        # is ndarray
        except TypeError:
            attrdict[key] = value.tolist()
        # else is string
        except ValueError:
            attrdict[key] = str(value)

    return attrdict


def get_info(data: Dataset) -> dict:
    """
    Returns the metadata for a given dataset.
    """
    name: str = data.name
    data_model: str = data.data_model
    format: str = data.disk_format
    # don't care about groups, so leave them as str
    groups: str = str(data.groups)

    dict = {
        "name": name,
        "dimensions": _get_ncdims(data),
        "variables": _get_ncvars(data),
        "attributes": _get_ncattrs(data),
        "groups": groups,
        "data_model": data_model,
        "format": format,
    }
    return dict
