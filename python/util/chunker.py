#!/usr/bin/env python3
# File Name: chunker.py
# Description: Chunker
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 10 Sep 2023 11:30:23
# Last Modified: 20 Sep 2023 09:30:09

# dependencies
import numpy as np


class Chunker():
    """
    Creates chunks for arrays of boolean indices.
    """

    def __init__(self, indices: np.ndarray, n: int):
        self.indices = indices
        self.n = n

    def chunk(self, i: int) -> np.ndarray:
        chunks = np.array_split(self.indices, self.n)

        for ii, c in enumerate(chunks):
            if i != ii:
                chunks[ii] = np.full(c.shape, False)

        return np.concatenate(chunks)

