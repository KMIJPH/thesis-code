#!/usr/bin/env python3
# File Name: stats.py
# Description: Various stats
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 07 Feb 2023 10:17:04
# Last Modified: 13 Feb 2024 17:42:07

# dependencies
import numpy as np
import pandas as pd
from scipy.special import erfcinv

Array = np.ndarray | pd.Series

__all__ = [
    "Array",
    "adj_r2",
    "bias",
    "corr",
    "crmsd",
    "kge",
    "linreg",
    "mae",
    "me",
    "metrics",
    "mode",
    "mse",
    "nse",
    "pearsonr",
    "r2",
    "scaled_mad",
    "is_outlier",
    "rmse",
    "sde",
]


def scaled_mad(y: Array, median: float) -> Array:
    """
    Scaled meadian absolute deviation copied from MATLAB.
    """
    abs_diff = np.abs(y - median)
    median_abs_diff = np.nanmedian(abs_diff)
    c = -1 / (np.sqrt(2) * erfcinv(3 / 2))
    scaled_mad = c * median_abs_diff
    return scaled_mad


def is_outlier(y: Array, method: str) -> Array:
    """
    Median:
    By default, an outlier is a value that is more than three scaled median absolute deviations (MAD) from the median.

    Quartiles:
    Outliers are defined as elements more than 1.5 interquartile ranges above the upper quartile (75 percent) or below the lower quartile (25 percent). This method is useful when the data in A is not normally distributed.
    """
    if method == "median":
        median = np.nanmedian(y)
        sm = scaled_mad(y, median)
        return np.abs(y - median) > (3 * sm)
    elif method == "quartiles":
        q1 = np.nanpercentile(y, 25)
        q3 = np.nanpercentile(y, 75)
        iqr = q3 - q1
        lower_bound = q1 - (1.5 * iqr)
        upper_bound = q3 + (1.5 * iqr)
        return (y < lower_bound) | (y > upper_bound)
    else:
        q1 = np.nanpercentile(y, 10)
        q3 = np.nanpercentile(y, 90)
        iqr = q3 - q1
        lower_bound = q1 - (iqr)
        upper_bound = q3 + (iqr)
        return (y < lower_bound) | (y > upper_bound)


def adj_r2(y: Array, yhat: Array, p: int) -> float:
    """
    Returns adjusted R2 according to wikipedia.
    https://en.wikipedia.org/wiki/Coefficient_of_determination#Adjusted_R2
    """
    n = len(y)
    return 1 - (1 - r2(y, yhat)) * ((n - 1) / (n - p - 1))


def bias(y: Array, yhat: Array) -> float:
    """
    Returns the overall bias according to Taylor (2001)
    """
    ybar, yhatbar = np.mean(y), np.mean(yhat)
    return float(ybar - yhatbar)


def corr(y: Array, yhat: Array) -> float:
    """
    Returns the correlation coefficient according to Taylor (2001) - eq. 1
    """
    ybar, yhatbar = np.mean(y), np.mean(yhat)
    sigma_y, sigma_yhat = np.std(y), np.std(yhat)
    return float(((1 / len(y)) * np.sum((y - ybar) * (yhat - yhatbar))) / (sigma_y * sigma_yhat))


def crmsd(y: Array, yhat: Array) -> float:
    """
    Returns the centered pattern RMS difference according to Taylor (2001) - eq. 2
    """
    ybar, yhatbar = np.mean(y), np.mean(yhat)
    return float(np.sqrt((1 / len(y)) * np.sum(((y - ybar) - (yhat - yhatbar))**2)))


def kge(y: Array, yhat: Array) -> float:
    """
    Returns the Kling-Gupta efficiency according to Gupta et al. (2009) - eq. 9
    """
    ybar, yhatbar = np.mean(y), np.mean(yhat)
    sigma_y, sigma_yhat = np.std(y), np.std(yhat)
    r = pearsonr(y, yhat)
    alpha = sigma_yhat / sigma_y
    beta = yhatbar / ybar
    return 1 - np.sqrt(((r - 1)**2) + ((alpha - 1)**2) + ((beta - 1)**2))


def linreg(x: Array, y: Array) -> tuple[np.ndarray, float, float]:
    """
    Calculate a linear least-squares regression for two sets of measurements.
    Returns the regression line, slope and intercept.
    https://en.wikipedia.org/wiki/Ordinary_least_squares#Simple_linear_regression_model
    """
    xbar, ybar = np.mean(x), np.mean(y)

    numerator = np.sum((x - xbar) * (y - ybar))
    denominator = np.sum((x - xbar)**2)

    slope = numerator / denominator
    intercept = ybar - slope * xbar
    line = x * slope + intercept

    return line, slope, intercept


def mae(y: Array, yhat: Array) -> float:
    """
    Returns mean absolute prediction error according to Wadoux et al. (2021) - eq. 2
    """
    return 1 / len(y) * np.sum(np.abs(y - yhat))


def me(y: Array, yhat: Array) -> float:
    """
    Returns mean prediction error according to Wadoux et al. (2021) - eq. 1
    """
    return 1 / len(y) * np.sum(y - yhat)


def mode(x: Array) -> tuple[np.ndarray, np.ndarray]:
    """
    Returns mode and count of a vector.
    """
    m, f = np.unique(x, return_counts=True)
    index = np.argmax(f)
    return m[index], f[index]


def mse(y: Array, yhat: Array) -> float:
    """
    Returns mean squared error according to wikipedia.
    https://en.wikipedia.org/wiki/Mean_squared_error#Definition_and_basic_properties
    """
    return float((1 / len(y)) * np.sum((y - yhat)**2))


def nse(y: Array, yhat: Array) -> float:
    """
    Returns the NSE (Nash-Sutcliffe model efficiency) according to Nash and Sutcliffe (1970).
    Is the same as the modelling efficiency coefficient MEC (Janssen & Heuberger, 1995),
    see Wadoux et al. (2021) - eq. 5
    """
    numerator = np.sum((y - yhat)**2)
    denominator = np.sum((y - np.mean(y))**2)
    return float(1 - (numerator / denominator))


def pearsonr(y: Array, yhat: Array) -> float:
    """
    Returns the pearson correlation coefficient according to wikipedia.
    https://en.wikipedia.org/wiki/Pearson_correlation_coefficient#For_a_sample
    """
    ybar, yhatbar = np.mean(y), np.mean(yhat)
    numerator = np.sum((y - ybar) * (yhat - yhatbar))
    denominator = np.sqrt(np.sum((y - ybar)**2) * np.sum((yhat - yhatbar)**2))
    return float(numerator / denominator)


def rmse(y: Array, yhat: Array) -> float:
    """
    Returns root mean squared error according to wikipedia.
    https://en.wikipedia.org/wiki/Root-mean-square_deviation
    """
    return np.sqrt(mse(y, yhat))


def r2(y: Array, yhat: Array) -> float:
    """
    Returns coefficient of determination according to wikipedia -> 1 - (SSR/SST)
    https://en.wikipedia.org/wiki/Coefficient_of_determination
    """
    ybar = np.mean(y)
    ssr = np.sum((y - yhat)**2)
    sst = np.sum((y - ybar)**2)
    return float(1 - (ssr / sst))


def sde(y: Array, yhat: Array) -> float:
    """
    Returns the standard deviation of the error according to Wadoux et al. (2021) - eq. 8
    """
    prediction_error = y - yhat
    return np.sqrt(1 / len(y) * np.sum((prediction_error - np.mean(prediction_error))**2))


def metrics(y: Array, yhat: Array, p: int) -> dict[str, float]:
    """
    Returns all metrics.

    y: real values
    yhat: predictions
    p: total number of explanatory variables in the model
    """
    return {
        "adj_r2": adj_r2(y, yhat, p),
        "bias": bias(y, yhat),
        "corr": corr(y, yhat),
        "crmsd": crmsd(y, yhat),
        "kge": kge(y, yhat),
        "mae": mae(y, yhat),
        "me": me(y, yhat),
        "mse": mse(y, yhat),
        "nse": nse(y, yhat),
        "pearsonr": pearsonr(y, yhat),
        "r2": r2(y, yhat),
        "rmse": rmse(y, yhat),
        "sde": sde(y, yhat),
        "std": float(np.std(yhat)),
        "std_ref": float(np.std(y)),
    }

