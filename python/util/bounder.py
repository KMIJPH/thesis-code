#!/usr/bin/env python3
# File Name: bounder.py
# Description: Bounder
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 14 Sep 2023 14:08:40
# Last Modified: 20 Sep 2023 09:30:06

# dependencies
import numpy as np


class Bounder():
    def __init__(self, min: float, max: float, n: int):
        self.min = min
        self.max = max
        self.n = n

    def create(self, **kwargs) -> list[tuple[float, float]]:
        """
        Creates n linear classes.

        kwargs:
            classes: custom classes
            zero: add zero class at the beginning
        """
        classes = kwargs["classes"] if "classes" in kwargs else np.round(np.linspace(self.min, self.max, self.n), 2)
        zero = kwargs["zero"] if "zero" in kwargs else False

        bounds = []
        for i in range(1, len(classes)):
            bounds.append((classes[i - 1], classes[i]))

        if zero:
            z = (0.0, 1e-6)
            bounds = [z] + bounds
            bounds[1] = (1e-6, bounds[1][1])
        return bounds

    def int(self) -> list[tuple[float, float]]:
        """
        Rounds classes to the next integer.
        """
        classes = self.create()

        int_classes = []
        for min, max in classes:
            rmin = np.round(min)
            rmax = np.round(max)
            if rmin != rmax:
                int_classes.append((rmin, rmax))

        return int_classes


class FibonacciBounder(Bounder):
    def __init__(self, n: int):
        super().__init__(0, 0, n)

    def create(self, **kwargs) -> list[tuple[float, float]]:
        """
        Creates classes based on the fibonacci sequence.
        """
        fib = [0, 1]
        for i in range(2, self.n + 2):
            fib.append(fib[i - 1] + fib[i - 2])
        return super().create(classes=np.unique(fib).astype(np.float64), **kwargs)


class ExponentialBounder(Bounder):
    def __init__(self, min: float, max: float, n: int):
        super().__init__(min, max, n)

    def create(self, **kwargs) -> list[tuple[float, float]]:
        """
        Creates exponential classes.
        """
        classes = np.round(np.logspace(np.log10(self.min + 1), np.log10(self.max + 1), self.n) - 1, 2)
        return super().create(classes=classes, **kwargs)

