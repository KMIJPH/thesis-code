#!/usr/bin/env python3
# File Name: read.py
# Description: Read netcdf data
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 13 Jan 2023 13:02:47
# Last Modified: 06 Dez 2023 11:49:25

# stdlib imports
from datetime import datetime

# dependencies
import numpy as np
from netCDF4 import Dataset, num2date


def read_at_time(file: str, var: str, time: list[datetime]) -> np.ma.masked_array:
    """
    Reads a variable from a netCDF4 dataset at a given time range.
    Will raise an exception if days are missing.
    """
    ds = Dataset(file, "r")
    time_var = ds["time"]
    out_var = ds[var]
    match len(out_var.shape):
        case 1:
            # lat and lon
            return ds[var][:]  # v
        case 2:
            # lc
            return ds[var][:, :]  # y, x
        case 3:
            cftime = num2date(time_var[:], time_var.units, calendar=time_var.calendar).data
            dtime = np.array(list(map(lambda x: datetime.fromisoformat(x.isoformat()), cftime)))
            time_start = np.argmax(dtime >= time[0])
            time_end = np.argmax(dtime >= time[-1]) + 1
            if time_end < time_start:
                raise IndexError(f"Incorrect end date '{time_end}'")
            idx = range(time_start, time_end)
            data = ds[var][idx, :, :]  # t, y, x
            if data.shape[0] != len(time):
                raise IndexError(
                    f"File: {file}\nLength of t dimension '{data.shape[0]}' does not match time range {len(time)}."
                )
            return data
        case _:
            raise IndexError("Maximum valid dimension size is 3.")

