#!/usr/bin/env python3
# File Name: time.py
# Description: Time functions
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 25 Jul 2023 09:51:34
# Last Modified: 26 Sep 2023 18:59:24

# stdlib imports
from datetime import datetime, timedelta
from enum import Enum

# dependencies
import numpy as np
from numpy.typing import NDArray


class Indexer(Enum):
    DAY = 1
    MONTH = 2
    YEAR = 3


def daily(start: datetime, end: datetime) -> list[datetime]:
    """Creates daily time range"""
    return [start + timedelta(days=x) for x in range(0, ((end + timedelta(days=1)) - start).days)]


def monthly(start: datetime, end: datetime) -> list[datetime]:
    """Creates monthly time range"""
    time_range = []
    for d in daily(start, end):
        m = datetime(d.year, d.month, 1)
        if m not in time_range:
            time_range.append(m)

    return time_range


def yearly(start: datetime, end: datetime) -> list[datetime]:
    """Creates yearly time range"""
    time_range = []
    for y in range(start.year, end.year + 1):
        time_range.append(datetime(year=y, month=1, day=1))

    return time_range


def leap_year(y: int) -> bool:
    """
    Checks whether the year is a leap year.
    """
    if (y % 400 == 0) and (y % 100 == 0):
        return True
    elif (y % 4 == 0) and (y % 100 != 0):
        return True

    return False


def index(t: list[datetime] | NDArray[np.datetime64], i: Indexer, v: int) -> list[bool]:
    """
    Logical indexing of a time range (only usable on numpy arrays).
    Can extract days, months and years.
    """
    match i:
        case Indexer.DAY:
            idx = [x.day == v for x in t]
        case Indexer.MONTH:
            idx = [x.month == v for x in t]
        case Indexer.YEAR:
            idx = [x.year == v for x in t]

    return idx


def date2doy(date: datetime) -> float:
    """
    Returns decimal day of year.
    """
    return date.timetuple().tm_yday + (date.hour / 24) + (date.minute / (24 * 60) + (date.second / (24 * 60 * 60)))


def date2lst(date: datetime) -> float:
    """
    Converts date to local standard time (decimal hour).
    """
    return date.hour + (date.minute / 60) + (date.second / (60 * 60))
