#!/usr/bin/env python3
# File Name: canopy.py
# Description: Forest canopy height
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 17 Feb 2023 11:04:15
# Last Modified: 06 Dez 2023 09:08:43

# stdlib imports
from datetime import datetime
from os.path import join
from tempfile import gettempdir

# dependencies
from netCDF4 import Dataset
from osgeo import gdal

# local imports
from prep.lc import raster_metadata
from settings import CANOPY, LC

FILL = 103


def prepare_canopy() -> None:
    """
    Prepares forest canopy height data (https://glad.umd.edu/dataset/gedi).
    - 'Zooms in' to the study area and crops a slightly bigger rectangle.
    - Resizes the raster to the MODIS resolution
    - Crops it to the real study area

    Pixel values: 0-60 Forest canopy height, meters
                  101 Water
                  102 Snow/ice
                  103 No data
    """
    print("Processing...")
    # get needed metadata from LC
    lc_dataset = LC().get_dataset()
    lat = lc_dataset.variables["lat"][:]
    lon = lc_dataset.variables["lon"][:]

    # srs
    metadata = raster_metadata()
    wkt = metadata["srs"].ExportToWkt()

    # time var
    date_time = datetime(2019, 1, 1)
    date_string = date_time.strftime("%Y-%m-%d %H:%M:%S")
    time_units = f"days since {date_string}"

    # read canopy
    ds = gdal.Open(CANOPY().get_path(), gdal.GA_ReadOnly)

    # crop #1
    vrt_cropped = gdal.Translate(
        "",
        ds,
        format="VRT",                    # virtual format
        noData=FILL,                     # set nodata
        projWin=metadata["box_big"],     # 'zoom in'
    )

    # write cropped to tempfile (set everything above 100 to NaN to avoid resampling)
    # 101 Water, 102 Snow/Ice, 103 No data
    cropped = vrt_cropped.ReadAsArray()
    cropped[cropped >= 100] = FILL
    [rows, cols] = cropped.shape
    driver = gdal.GetDriverByName("GTiff")
    outdata = driver.Create(join(gettempdir(), "canopy.tif"), cols, rows, 1, gdal.GDT_UInt16)
    outdata.SetGeoTransform(vrt_cropped.GetGeoTransform())
    outdata.SetProjection(vrt_cropped.GetProjection())
    outdata.GetRasterBand(1).WriteArray(cropped)
    outdata.GetRasterBand(1).SetNoDataValue(FILL)
    outdata.FlushCache()
    outdata = None

    # interpolate and crop it to the real extent
    ds = gdal.Open(join(gettempdir(), "canopy.tif"))
    vrt_resized = gdal.Warp(
        "",
        ds,
        outputType=gdal.GDT_UInt16,      # Uint16
        format="VRT",                    # virtual format
        srcNodata=FILL,                  # nodata / fillvalues
        dstNodata=FILL,
        xRes=metadata["xres"],           # pixel sizes
        yRes=metadata["yres"],
        outputBounds=metadata["box"],    # crop to bounding box from MCD12Q1
        resampleAlg=gdal.GRA_Average,
    )

    vrt = vrt_resized.ReadAsArray()
    outfile = CANOPY().get_outpath()

    # create the netCDF file
    out = Dataset(outfile, "w", format="NETCDF4")
    out.Conventions = "CF-1.6"
    out.title = "Forest canopy height 2019"
    out.project = "Global Forest Canopy Height 2019"
    out.institution = "Global Land Analysis and Discovery"
    out.references = "https://glad.umd.edu/dataset/gedi"
    out.creation_date = str(datetime.now())
    out.comment = "Forest canopy height Austria 500m, @Joseph Kiem"

    # create limited dimensions
    out.createDimension("time", size=1)
    out.createDimension("lat", size=len(lat))
    out.createDimension("lon", size=len(lon))

    # variables----------------------------------------- #
    crs = out.createVariable(
        "crs", "u2",
        dimensions=(),
        fill_value=None,
        zlib=False
    )
    crs.spatial_ref = wkt
    # -------------------------------------------------- #
    # uint16 time
    time_var  = out.createVariable(
        "time", "u2",
        dimensions=("time"),
        fill_value=None,
        zlib=False
    )
    time_var.set_auto_maskandscale(False)
    time_var.long_name = "time"
    time_var.units = time_units
    time_var.calendar = "julian"
    time_var.axis = "T"
    # -------------------------------------------------- #
    lat_var  = out.createVariable(
        "lat", "f8",
        dimensions=("lat"),
        fill_value=None,
        zlib=False
    )
    lat_var.set_auto_maskandscale(False)
    lat_var.long_name = "latitude"
    lat_var.units = "degrees_north"
    lat_var.axis = "Y"
    # -------------------------------------------------- #
    lon_var  = out.createVariable(
        "lon", "f8",
        dimensions=("lon"),
        fill_value=None,
        zlib=False
    )
    lon_var.set_auto_maskandscale(False)
    lon_var.long_name = "longitude"
    lon_var.units = "degrees_east"
    lon_var.axis = "X"
    # -------------------------------------------------- #
    canopy_var = out.createVariable(
        "canopy", "u2",
        dimensions=("time", "lat", "lon"),
        fill_value=FILL,
        zlib=False,
        shuffle=False,
    )
    canopy_var.set_auto_maskandscale(False)
    canopy_var.long_name = "Forest canopy height"
    canopy_var.units = "meters"
    canopy_var.grid_mapping = "crs"

    lat_var[:] = lat
    lon_var[:] = lon
    time_var[:] = 0
    canopy_var[:] = vrt
    out.close()
    print(f"Generated netCDF file '{outfile}'")
    return

