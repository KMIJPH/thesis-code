#!/usr/bin/env python3
# File Name: dss.py
# Description: Days since snowfall calculation.
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 02 Jan 2023 00:32:47
# Last Modified: 20 Sep 2023 21:30:16

# stdlib imports
from datetime import datetime

# dependencies
import numpy as np
from cftime import to_tuple
from netCDF4 import Dataset, num2date
from rustlib import snow

# local imports
import settings
from util import get_info

FILL = -9999


def calc_dss(
    fuse: settings.FuSE,
    years: list[int] | None = None,
    n: int = 15,
    lead: int = 365,
) -> None:
    """
    Calculates days since snowfall for a given FuSE netCDF file.
    NOTE: the calculation time span of dss should start some time prior to the year range of interest.
          The number of days can be specified with 'lead'.

    NOTE: n corresponds to the number of rows (latitude) it will include in one iteration
          when calculating and writing to the output file.
          Because this process is time-expensive,
          iterations should be kept as small as possible without overloading RAM.
          e.g: 694 (<- length of latitude dimension) / 50 (<- n) = 14 iterations
    """
    ds = Dataset(fuse.get_outpath(), "r")
    info = get_info(ds)
    outfile = fuse.get_dss_path()

    lat = ds.variables["lat"]
    lon = ds.variables["lon"]
    time = ds.variables["time"]
    hs = ds.variables["HS"]

    # calculate extracted time dimension
    dtime = num2date(time[:], time.units, calendar=time.calendar).data
    if not years:
        years = list(range(2002, dtime[-1].year + 1))

    time_idx = [to_tuple(t)[0] in years for t in dtime]

    # find first index
    # argmax stops at the first maximum value and returns its index
    first = np.argmax(time_idx)
    year_prior = np.full(lead, True)
    time_idx[first - lead:first] = year_prior

    # create dataset
    out = Dataset(outfile, "w", format="NETCDF4")
    out.Conventions = info["attributes"]["Conventions"]
    out.title = info["attributes"]["title"]
    out.institution = info["attributes"]["institution"]
    out.project = info["attributes"]["project"]
    out.creation_date = str(datetime.now())
    out.original_file = fuse.get_filename()
    out.comment = "Days since snowfall, @Joseph Kiem"

    out.createDimension("time", size=sum(time_idx) - lead)
    out.createDimension("lat", size=len(lat))
    out.createDimension("lon", size=len(lon))

    # variables----------------------------------------- #
    crs = out.createVariable(
        "crs", "u2",
        dimensions=(),
        fill_value=None,
        zlib=False
    )
    crs.spatial_ref = info["variables"]["crs"]["spatial_ref"]
    # -------------------------------------------------- #
    # coverting to uint16 should be okay here (decimal not needed)
    time_var = out.createVariable(
        "time", "u2",
        dimensions=("time"),
        fill_value=None,
        zlib=False
    )
    time_var.set_auto_maskandscale(False)
    time_var.long_name = "time"
    time_var.units = info["variables"]["time"]["units"]
    time_var.calendar = info["variables"]["time"]["calendar"]
    time_var.axis = "T"
    # -------------------------------------------------- #
    lat_var = out.createVariable(
        "lat", "f8",
        dimensions=("lat"),
        fill_value=None,
        zlib=False
    )
    lat_var.set_auto_maskandscale(False)
    lat_var.long_name = "latitude"
    lat_var.units = "degrees_north"
    lat_var.axis = "Y"
    # -------------------------------------------------- #
    lon_var = out.createVariable(
        "lon", "f8",
        dimensions=("lon"),
        fill_value=None,
        zlib=False
    )
    lon_var.set_auto_maskandscale(False)
    lon_var.long_name = "longitude"
    lon_var.units = "degrees_east"
    lon_var.axis = "X"
    # -------------------------------------------------- #
    dss_var = out.createVariable(
        "dss", "i8",
        dimensions=("time", "lat", "lon"),
        fill_value=FILL,
        zlib=True,
        complevel=4,
        shuffle=True,
        chunksizes=(1, len(lat), len(lon)),  # might take longer to write, but should be fast to read
    )
    dss_var.set_auto_maskandscale(False)
    dss_var.long_name = "Days since snowfall"
    dss_var.units = "days"
    dss_var.grid_mapping = "crs"
    # -------------------------------------------------- #

    # loop over latitude (rows)
    lrow: list[int] = list(range(0, len(lat), n))
    lat_row: list[tuple[int, int]] = [
        (t, len(lat)) if t == lrow[-1]
        else (t, t + n)
        for t in lrow
    ]

    for i, y in enumerate(lat_row):
        print(f"{datetime.now()}: Processing chunk {i+1} of {len(lat_row)}")

        print("  Reading snow depth..")
        row = hs[time_idx, y[0]:y[1], :]  # time, y, x
        row[row.mask] = np.NaN
        dss = np.full([sum(time_idx) - 1, row.shape[1], row.shape[2]], FILL)

        print("  Calculating dss..")
        dss = np.apply_along_axis(snow.days_since_snowfall, 0, row, FILL)

        print("  Writing to output..")
        # do not include the included days prior
        row = None
        dss_var[:, y[0]:y[1], :] = dss[lead - 1:, :, :]
        dss = np.ndarray([])  # clear

    lat_var[:] = lat[:]
    lon_var[:] = lon[:]

    # reset the included days prior to false
    year_prior = np.full(lead, False)
    time_idx[first - lead: first] = year_prior
    time_var[:] = time[time_idx]

    out.close()
    print(f"Generated netCDF file '{outfile}'")
    return

