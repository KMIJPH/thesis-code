#!/usr/bin/env python3
# File Name: bess.py
# Description: BESS data preparation.
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: Thu 14 Jul 2022 09:54:35 AM CEST
# Last Modified: 06 Dez 2023 08:21:32

# stdlib imports
from datetime import datetime

# dependencies
import numpy as np
from netCDF4 import Dataset
from osgeo import gdal, gdal_array

# local imports
import settings
from prep.lc import raster_metadata


def prepare_bess() -> None:
    """
    - 'Zooms in' to the study area and crops a slightly bigger rectangle.
    - Resizes the raster to the MODIS resolution
    - Crops it to the real study area
    - Generates one netCDF file containing all the data
    NOTE: assumes that there are no time gaps (missing files)
    NOTE: uses all existing BESS files (i.e. temporal extent)
          in the directory specified in the settings.
    NOTE: reads metadata from MCD12Q1, so that one is needed.
    NOTE: GDAL cropping and resizing is very fast;
          the writing / compression takes a while.
    NOTE: following CF-1.6 conventions
    https://cfconventions.org/Data/cf-conventions/cf-conventions-1.6/build/cf-conventions.html
    """
    variable_name = "surface_downwelling_shortwave_flux_in_air"
    file_name = "BESS_RSDN_Daily.A"

    bess_files = settings.BESS(0).get_files()

    # read metadata from first file
    bess_first = settings.BESS(0)
    bess_info = bess_first.get_info()
    fill = bess_info["variables"][variable_name]["_FillValue"]

    # metadata
    # NOTE: the authors stated these exact coordinates
    pixel_size = 0.05
    bess_maxlat = 90
    bess_minlon = -180
    #               ulx         xres   xskew    uly    yskew   yres
    transform = [bess_minlon, pixel_size, 0, bess_maxlat, 0, -pixel_size]
    metadata = raster_metadata()

    # read date from first file
    doy_year_string = bess_first.get_filename().replace(file_name, "")
    year = doy_year_string[0:4]
    doy = doy_year_string[4:]

    # create CF-1.6 conform time units
    date_time = datetime.strptime(f"{year} {doy}", "%Y %j")
    date_string = date_time.strftime("%Y-%m-%d %H:%M:%S")
    time_units = f"days since {date_string}"
    time_calendar = "julian"  # not sure whether gregorian / julian matters for daily resolution..

    # spatial metadata
    lat_len = len(metadata["lat"])
    lon_len = len(metadata["lon"])
    wkt = metadata["srs"].ExportToWkt()

    # other
    multiplier = 100
    uint16_max = np.iinfo(np.uint16).max
    outfile = bess_first.get_outpath()
    time_len = len(bess_files)

    # create the netCDF file
    out = Dataset(outfile, "w", format="NETCDF4")
    out.Conventions = "CF-1.6"
    out.title = "BESS global shortwave radiation"
    out.project = "BESS-RAD"
    out.institution = "Seoul National University, Seoul, South Korea"
    out.references = "https://www.environment.snu.ac.kr/bess-rad"
    out.creation_date = str(datetime.now())
    out.comment = "Merged to one file, cropped to Austria and resized, @Joseph Kiem"

    # create limited dimensions
    out.createDimension("time", size=time_len)
    out.createDimension("lat", size=lat_len)
    out.createDimension("lon", size=lon_len)

    # variables----------------------------------------- #
    crs = out.createVariable(
        "crs", "u2",
        dimensions=(),
        fill_value=None,
        zlib=False
    )
    crs.spatial_ref = wkt
    # -------------------------------------------------- #
    # uint16 time
    time_var  = out.createVariable(
        "time", "u2",
        dimensions=("time"),
        fill_value=None,
        zlib=False
    )
    time_var.set_auto_maskandscale(False)
    time_var.long_name = "time"
    time_var.units = time_units
    time_var.calendar = time_calendar
    time_var.axis = "T"
    # -------------------------------------------------- #
    lat_var  = out.createVariable(
        "lat", "f8",
        dimensions=("lat"),
        fill_value=None,
        zlib=False
    )
    lat_var.set_auto_maskandscale(False)
    lat_var.long_name = "latitude"
    lat_var.units = "degrees_north"
    lat_var.axis = "Y"
    # -------------------------------------------------- #
    lon_var  = out.createVariable(
        "lon", "f8",
        dimensions=("lon"),
        fill_value=None,
        zlib=False
    )
    lon_var.set_auto_maskandscale(False)
    lon_var.long_name = "longitude"
    lon_var.units = "degrees_east"
    lon_var.axis = "X"
    # -------------------------------------------------- #
    rg_var = out.createVariable(
        "Rg", "u2",
        dimensions=("time", "lat", "lon"),
        fill_value=uint16_max,
        zlib=True,
        complevel=4,
        shuffle=True,
        chunksizes=(1, lat_len, lon_len),
    )
    rg_var.set_auto_maskandscale(False)
    rg_var.long_name = variable_name
    rg_var.units = bess_info["variables"][variable_name]["Unit"]

    rg_var.grid_mapping = "crs"
    rg_var.scale_factor = 1 / multiplier
    # -------------------------------------------------- #
    # write static data
    lat_var[:] = metadata["lat"]
    lon_var[:] = metadata["lon"]

    # iterate over all the BESS files and write
    for i in range(0, time_len, 1):
        print(f"{datetime.now()}: BESS {i + 1}/{time_len}")
        bess = settings.BESS(i)
        bess_dataset = bess.get_dataset()

        # x, y
        rg = bess_dataset.variables[variable_name][:, :]
        rg = rg.transpose([1, 0])  # y, x

        # read array as GDAL dataset and set metadata
        ds = gdal_array.OpenArray(rg)
        ds.SetSpatialRef(metadata["srs"])
        ds.SetProjection(metadata["proj"])
        ds.SetGeoTransform(transform)

        # crop the raster to an extent a little bit bigger than Austria ('zoom in')
        # NOTE: no outputType set, but that should still be Uint16
        vrt_cropped = gdal.Translate(
            "",
            ds,
            format="VRT",                    # virtual format
            noData=fill,                     # set nodata
            projWin=metadata["box_big"],     # 'zoom in'
        )

        # interpolate and crop it to the real extent
        vrt_resized = gdal.Warp(
            "",
            vrt_cropped,
            outputType=gdal.GDT_Float32,     # float 32 should be okay
            format="VRT",                    # virtual format
            srcNodata=fill,                  # nodata / fillvalues
            dstNodata=fill,
            xRes=metadata["xres"],           # pixel sizes
            yRes=metadata["yres"],
            outputBounds=metadata["box"],    # crop to bounding box from MCD12Q1
            resampleAlg=gdal.GRA_Bilinear,
        )

        # read dataset back as array
        # the new array should have the same shape (y, x) and the same resolution as the MODIS data
        rg_final = vrt_resized.ReadAsArray()
        ds = vrt_resized = None

        multiply_index = rg_final != fill
        fill_index = rg_final == fill
        # fillvalues, scale, conversion
        rg_final[fill_index] = uint16_max
        rg_final[multiply_index] *= multiplier
        rg_ready = rg_final.astype(np.uint16)

        # get days since (cause there could be missing days)
        doy_year_string = bess.get_filename().replace(file_name, "")
        year = doy_year_string[0:4]
        doy = doy_year_string[4:]
        current_date = datetime.strptime(f"{year} {doy}", "%Y %j")
        days_since = current_date - date_time

        # write data
        time_var[i] = days_since.days
        rg_var[i, :, :] = rg_ready

    out.close()
    print(f"Generated netCDF file '{outfile}'")
    return

