#!/usr/bin/env python3
# File Name: snow_stats.py
# Description:
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 24 Feb 2024 10:06:41
# Last Modified: 24 Feb 2024 15:30:53

# stdlib imports
import os
from datetime import datetime

# dependencies
import numpy as np
import pandas as pd
from netCDF4 import Dataset

# local imports
from prep.lc import raster_metadata
from settings import DIRS, FuSE, get_mask
from util import daily, read_at_time

FILL = -1


def covered_by_snow(fuse: FuSE) -> None:
    """
    Calculate days covered by snow.
    """
    file = fuse.get_path()
    ds = fuse.get_dataset()

    fuse_mask = ds["mask"][:]
    lat, lon = ds["lat"][:].shape

    outfile = os.path.join(DIRS["output_dir"], f"snow_cover_{fuse.get_filename()}.nc")
    years = list(range(1991, 2021))

    # srs not correct, but doesnt matter
    metadata = raster_metadata()
    wkt = metadata["srs"].ExportToWkt()

    out = Dataset(outfile, "w", format="NETCDF4")
    out.Conventions = "CF-1.6"
    out.title = "Snow cover Austria"
    out.creation_date = str(datetime.now())
    out.comment = "Snow covered days for Austria, @Joseph Kiem"

    # create limited dimensions
    out.createDimension("year", size=len(years))
    out.createDimension("lat", size=lat)
    out.createDimension("lon", size=lon)

    crs = out.createVariable(
        "crs", "u2",
        dimensions=(),
        fill_value=None,
        zlib=False
    )
    crs.spatial_ref = wkt

    year_var  = out.createVariable(
        "year", "u2",
        dimensions=("year"),
        fill_value=None,
        zlib=False
    )
    year_var.set_auto_maskandscale(False)
    year_var.long_name = "year"
    year_var.units = "year"
    year_var.axis = "T"

    lat_var  = out.createVariable(
        "lat", "f8",
        dimensions=("lat"),
        fill_value=None,
        zlib=False
    )
    lat_var.set_auto_maskandscale(False)
    lat_var.long_name = "latitude"
    lat_var.units = "degrees_north"
    lat_var.axis = "Y"

    lon_var  = out.createVariable(
        "lon", "f8",
        dimensions=("lon"),
        fill_value=None,
        zlib=False
    )
    lon_var.set_auto_maskandscale(False)
    lon_var.long_name = "longitude"
    lon_var.units = "degrees_east"
    lon_var.axis = "X"

    days_var = out.createVariable(
        "days", "i2",
        dimensions=("year", "lat", "lon"),
        fill_value=FILL,
        zlib=False,
        shuffle=False,
    )
    days_var.set_auto_maskandscale(False)
    days_var.long_name = "Snow covered days"
    days_var.units = "days"
    days_var.grid_mapping = "crs"

    year_var[:] = years
    # NOTE: not writing lat and lon (not needed)

    for i, y in enumerate(years):
        time_range = daily(datetime(y, 1, 1), datetime(y, 12, 31))
        hs = read_at_time(file, "HS", time_range)
        hs[hs.mask] = np.NaN
        mask = hs.data > 0

        # calculate number of days in year which are covered by snow
        days = np.nansum(mask, axis=0).astype(np.float64)
        days[~fuse_mask] = FILL
        days_var[i, :, :] = days

    out.close()
    print(f"Generated netCDF file '{outfile}'")
    return


def calc_x(fuse: FuSE, treshold: int):
    outfile = os.path.join(DIRS["output_dir"], f"snow_cover_{fuse.get_filename()}.nc")
    ds = Dataset(outfile, "r")
    ds2 = fuse.get_dataset()
    fuse_mask = ds2["mask"][:]
    all_pixels = len(fuse_mask[fuse_mask == 1])

    var = ds["days"]
    years = ds["year"][:]

    data = {}

    for i, y in enumerate(years):
        x = var[i, :, :]
        covered = x[x > treshold]
        data[y] = len(covered) / all_pixels * 100

    df = pd.DataFrame(data.items())
    df.columns = ["year", "days"]  # type: ignore
    print(df["days"].mean())

    all_data = var[:]
    np.nanmean(all_data)
