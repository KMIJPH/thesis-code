#!/usr/bin/env python3
# File Name: lai.py
# Description: Lai interpolation
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 08 Feb 2023 10:49:17
# Last Modified: 24 Feb 2024 11:04:15


# stdlib imports
from datetime import datetime, timedelta
from typing import Callable

# dependencies
import numpy as np
from netCDF4 import Dataset, num2date

# local imports
import settings
from util import Indexer, daily, get_info, index

# fill value
FILL = np.iinfo(np.uint16).max


def _nan_helper(y: np.ndarray) -> tuple[np.ndarray, Callable[[np.ndarray], np.ndarray]]:
    """Helper to handle indices and logical indices of NaNs.
    https://stackoverflow.com/questions/6518811/interpolate-nan-values-in-a-numpy-array

    Input:
        - y, 1d numpy array with possible NaNs
    Output:
        - nans, logical indices of NaNs
        - index, a function, with signature indices= index(logical_indices),
          to convert logical indices of NaNs to 'equivalent' indices
    Example:
        >>> # linear interpolation of NaNs
        >>> nans, x = nan_helper(y)
        >>> y[nans] = np.interp(x(nans), x(~nans), y[~nans])
    """
    return np.isnan(y), lambda z: z.nonzero()[0]


def _interpolate_pixel(pixel: np.ndarray, overlap: np.ndarray) -> np.ndarray:
    """
    Interpolates missing values (along time axis) for a single pixel.
    overlap = boolean array
    """
    y = np.full((len(overlap)), np.NaN)
    y[overlap] = pixel
    nans, x = _nan_helper(y)
    y[nans] = np.interp(x(nans), x(~nans), y[~nans])
    return y


def _interpolate_qc(qc: np.ndarray, overlap: np.ndarray) -> np.ndarray:
    """
    Interpolates qc.
    Values are ceiled, so everything around bad quality will be seen as bad quality.
    overlap = boolean array

    >= 128 Pixel not produced at all, value couldn t be retrieved (possible reasons: bad L1B data, unusable MOD09G
    >= 96  Main (RT) method failed due to problems other than geometry, empirical algorithm used
    >= 64  Main (RT) method failed due to bad geometry, empirical algorithm used
    >= 32  Main (RT) method used with saturation. Good, very usable
    >= 0   Main (RT) method used, best result possible (no saturation)
    """
    y = np.full((len(overlap)), np.NaN)
    y[overlap] = qc >= 128
    nans, x = _nan_helper(y)
    y[nans] = np.ceil(np.interp(x(nans), x(~nans), y[~nans]))
    return y.astype(bool)


def prepare_lai(n: int = 70) -> None:
    """
    Since lai is 4-daily, interpolates lai data to daily resolution.
    Will only include main method lc (qc check).

    NOTE: n corresponds to the number of rows (latitude) it will include in one iteration
          when calculating and writing to the output file.
          Because this process is time-expensive,
          iterations should be kept as small as possible without overloading RAM.
          e.g: 694 (<- length of latitude dimension) / 50 (<- n) = 14 iterations
    """
    lai_o = settings.LAI()
    lai_dataset = lai_o.get_dataset()
    info = lai_o.get_info()
    outfile = lai_o.get_outpath()
    multiplier = 100

    lai = lai_dataset["Lai_500m"]
    time = lai_dataset["time"]
    lat = lai_dataset["lat"]
    lon = lai_dataset["lon"]
    qc = lai_dataset["FparLai_QC"]
    dtime = num2date(time[:], time.units, calendar=time.calendar).data

    # create CF-1.6 conform time units
    date_time = datetime.strptime(f"{dtime[0].year} {dtime[0].month} {dtime[0].day}", "%Y %m %d")
    date_string = date_time.strftime("%Y-%m-%d %H:%M:%S")
    time_units = f"days since {date_string}"
    time_calendar = time.calendar

    # create desired temporal resolution
    int_time = np.array(
        [
            dtime[0] + timedelta(days=x)
            for x in range(0, (dtime[-1] + timedelta(days=1) - dtime[0]).days)
        ]
    )
    days_since_int = np.array([(x - int_time[0]).days for x in int_time])
    days_since_real = np.array([(x - dtime[0]).days for x in dtime])
    # overlapping days
    overlapping = np.isin(days_since_int, days_since_real)

    # create the netCDF file
    out = Dataset(outfile, "w", format="NETCDF4")
    out.Conventions = info["attributes"]["Conventions"]
    out.title = info["attributes"]["title"]
    out.institution = info["attributes"]["institution"]
    out.source = info["attributes"]["institution"]
    out.creation_date = str(datetime.now())
    out.comment = "Daily LAI Austria, @Joseph Kiem"

    # create limited dimensions
    out.createDimension("time", size=len(days_since_int))
    out.createDimension("lat", size=len(lat))
    out.createDimension("lon", size=len(lon))

    # variables----------------------------------------- #
    crs = out.createVariable(
        "crs", "u2",
        dimensions=(),
        fill_value=None,
        zlib=False
    )
    crs.spatial_ref = info["variables"]["crs"]["spatial_ref"]
    # -------------------------------------------------- #
    # uint16 time
    time_var  = out.createVariable(
        "time", "u2",
        dimensions=("time"),
        fill_value=None,
        zlib=False
    )
    time_var.set_auto_maskandscale(False)
    time_var.long_name = "time"
    time_var.units = time_units
    time_var.calendar = time_calendar
    time_var.axis = "T"
    # -------------------------------------------------- #
    lat_var  = out.createVariable(
        "lat", "f8",
        dimensions=("lat"),
        fill_value=None,
        zlib=False
    )
    lat_var.set_auto_maskandscale(False)
    lat_var.long_name = "latitude"
    lat_var.units = "degrees_north"
    lat_var.axis = "Y"
    # -------------------------------------------------- #
    lon_var  = out.createVariable(
        "lon", "f8",
        dimensions=("lon"),
        fill_value=None,
        zlib=False
    )
    lon_var.set_auto_maskandscale(False)
    lon_var.long_name = "longitude"
    lon_var.units = "degrees_east"
    lon_var.axis = "X"
    # -------------------------------------------------- #
    lai_var = out.createVariable(
        "lai", "u2",
        dimensions=("time", "lat", "lon"),
        fill_value=FILL,
        zlib=True,
        complevel=4,
        shuffle=True,
        chunksizes=(1, len(lat), len(lon)),
    )
    lai_var.set_auto_maskandscale(False)
    lai_var.long_name = info["variables"]["Lai_500m"]["long_name"]
    lai_var.units = info["variables"]["Lai_500m"]["units"]
    lai_var.grid_mapping = "crs"
    lai_var.scale_factor = 1 / multiplier
    # -------------------------------------------------- #
    mask_var = out.createVariable(
        "mask", "u1",
        dimensions=("time"),
        fill_value=None,
        zlib=False,
    )
    mask_var.set_auto_maskandscale(False)
    mask_var.long_name = "Interpolation mask"
    mask_var.units = "boolean"
    mask_var.grid_mapping = "crs"

    # loop over latitude (rows)
    lrow: list[int] = list(range(0, len(lat), n))
    lat_row: list[tuple[int, int]] = [
        (t, len(lat)) if t == lrow[-1]
        else (t, t + n)
        for t in lrow
    ]

    for i, y in enumerate(lat_row):
        print(f"{datetime.now()}: Processing chunk {i+1} of {len(lat_row)}")

        row = lai[:, y[0]:y[1], :]  # time, y, x
        qc_row = qc[:, y[0]:y[1], :]  # time, y, x
        lai_int = np.apply_along_axis(_interpolate_pixel, 0, row, overlap=overlapping)
        qc_int = np.apply_along_axis(_interpolate_qc, 0, qc_row, overlap=overlapping)

        # scale and fill
        lai_int = (lai_int * multiplier).astype(np.uint16)
        lai_int[qc_int] = FILL
        lai_var[:, y[0]:y[1], :] = lai_int[:]

    lat_var[:] = lat[:]
    lon_var[:] = lon[:]
    mask_var[:] = overlapping[:]
    time_var[:] = days_since_int[:]

    out.close()
    print(f"Generated netCDF file '{outfile}'")
    return


def mean_lai() -> None:
    """
    Calculates daily mean LAI over all the years.
    """
    lai_path = settings.LAI().get_outpath()
    outfile = lai_path.replace(".nc", "_mean.nc")
    ds = Dataset(lai_path, "r")

    info = get_info(ds)
    lai = ds["lai"]
    time = ds["time"]
    lat = ds["lat"]
    lon = ds["lon"]
    dtime = num2date(time[:], time.units, calendar=time.calendar).data

    time_out = daily(datetime(2019, 1, 1), datetime(2019, 12, 31))
    date_time = datetime.strptime(f"{time_out[0].year} {time_out[0].month} {time_out[0].day}", "%Y %m %d")
    date_string = date_time.strftime("%Y-%m-%d %H:%M:%S")
    time_units = f"days since {date_string}"
    time_calendar = time.calendar
    multiplier = 100

    out = Dataset(outfile, "w", format="NETCDF4")
    out.Conventions = info["attributes"]["Conventions"]
    out.title = info["attributes"]["title"]
    out.institution = info["attributes"]["institution"]
    out.source = info["attributes"]["institution"]
    out.creation_date = str(datetime.now())
    out.comment = "Mean daily LAI Austria, @Joseph Kiem"

    # create limited dimensions
    out.createDimension("time", size=len(time_out))
    out.createDimension("lat", size=len(lat))
    out.createDimension("lon", size=len(lon))

    # variables----------------------------------------- #
    crs = out.createVariable(
        "crs", "u2",
        dimensions=(),
        fill_value=None,
        zlib=False
    )
    crs.spatial_ref = info["variables"]["crs"]["spatial_ref"]
    # -------------------------------------------------- #
    # uint16 time
    time_var  = out.createVariable(
        "time", "u2",
        dimensions=("time"),
        fill_value=None,
        zlib=False
    )
    time_var.set_auto_maskandscale(False)
    time_var.long_name = "time"
    time_var.units = time_units
    time_var.calendar = time_calendar
    time_var.axis = "T"
    # -------------------------------------------------- #
    lat_var  = out.createVariable(
        "lat", "f8",
        dimensions=("lat"),
        fill_value=None,
        zlib=False
    )
    lat_var.set_auto_maskandscale(False)
    lat_var.long_name = "latitude"
    lat_var.units = "degrees_north"
    lat_var.axis = "Y"
    # -------------------------------------------------- #
    lon_var  = out.createVariable(
        "lon", "f8",
        dimensions=("lon"),
        fill_value=None,
        zlib=False
    )
    lon_var.set_auto_maskandscale(False)
    lon_var.long_name = "longitude"
    lon_var.units = "degrees_east"
    lon_var.axis = "X"
    # -------------------------------------------------- #
    lai_var = out.createVariable(
        "lai", "u2",
        dimensions=("time", "lat", "lon"),
        fill_value=FILL,
        zlib=False,
        shuffle=False,
    )
    lai_var.set_auto_maskandscale(False)
    lai_var.long_name = info["variables"]["lai"]["long_name"]
    lai_var.units = info["variables"]["lai"]["units"]
    lai_var.grid_mapping = "crs"
    lai_var.scale_factor = 1 / multiplier

    lat_var[:] = lat[:]
    lon_var[:] = lon[:]
    time_var[:] = list(range(0, len(time_out)))

    # iterate over days
    for i, d in enumerate(time_out):
        day = index(dtime, Indexer.DAY, d.day)
        month = index(dtime, Indexer.MONTH, d.month)
        ind = np.logical_and(day, month)

        print(f"{datetime.now()}: Processing chunk {i+1} of {len(time_out)}")

        chunk = lai[ind, :, :]  # time, y, x
        chunk[chunk.mask] = np.NaN
        lai_mean = np.nanmean(chunk, axis=0)  # mean for that day over all the years

        # scale and fill
        lai_int = (lai_mean * multiplier).astype(np.uint16)
        lai_int[np.isnan(lai_mean)] = FILL
        lai_var[i, :, :] = lai_int[:]

    out.close()
    print(f"Generated netCDF file '{outfile}'")
    return

