#!/usr/bin/env python3
# File Name: cack.py
# Description: CACK Kernel preparation
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 05 Dez 2023 17:06:29
# Last Modified: 06 Jan 2024 13:38:42

# stdlib imports
from datetime import datetime

# dependencies
import numpy as np
from netCDF4 import Dataset, num2date
from osgeo import gdal, gdal_array
from scipy.interpolate import RegularGridInterpolator, griddata

# local imports
from prep.lc import raster_metadata
from settings import CACK, LC, FuSE, get_mask

FILL = np.iinfo(np.uint16).max


def _toWGS84(lat: np.ndarray, lon: np.ndarray, data: np.ndarray) -> tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    'Reprojects' data to WGS84.
    """
    lat_flat = lat.flatten()
    lon_flat = lon.flatten()
    data_flat = data.flatten()

    y, x = lat.shape

    # regular grid for WGS84
    wgs84_lon, wgs84_lat = np.meshgrid(
        np.linspace(lon_flat.min(), lon_flat.max(), x),
        np.linspace(lat_flat.min(), lat_flat.max(), y)
    )

    interpolated = griddata(
        (lat_flat, lon_flat), data_flat,
        (wgs84_lat, wgs84_lon),
        method='linear'
    )
    interpolated = interpolated.reshape(wgs84_lat.shape)
    return interpolated, wgs84_lat, wgs84_lon


def _crop(lat, lon, ref_lat, ref_lon, data) -> np.ndarray:
    """
    Crops data.
    """
    minlon = np.min(ref_lon)
    maxlon = np.max(ref_lon)
    minlat = np.min(ref_lat)
    maxlat = np.max(ref_lat)
    mask = (lon >= minlon) & (lon <= maxlon) & (lat >= minlat) & (lat <= maxlat)
    data[~mask] = np.NaN

    non_nan_indices = np.where(~np.isnan(data))
    min_row, max_row = np.min(non_nan_indices[0]), np.max(non_nan_indices[0])
    min_col, max_col = np.min(non_nan_indices[1]), np.max(non_nan_indices[1])
    cropped = data[min_row:max_row + 1, min_col:max_col + 1]
    return cropped


def _interpolate(matrix: np.ndarray, new_shape: tuple[int, int]) -> np.ndarray:
    """
    Interpolates matrix to a new shape linearly.
    """
    rows, cols = np.indices(matrix.shape)
    interpolator = RegularGridInterpolator(
        (np.arange(matrix.shape[0]), np.arange(matrix.shape[1])),
        matrix, method='linear', bounds_error=False, fill_value=np.nan
    )

    new_rows = np.linspace(0, matrix.shape[0] - 1, new_shape[0])
    new_cols = np.linspace(0, matrix.shape[1] - 1, new_shape[1])
    new_rows_mesh, new_cols_mesh = np.meshgrid(new_rows, new_cols, indexing='ij')
    return interpolator((new_rows_mesh, new_cols_mesh))


def prepare_cack() -> None:
    """
    Prepares the CACK Kernel (Bright and O'Halloran 2019)
    - Calculates mean CACK (for all months of CACK CM)
    - 'Zooms in' to the study area and crops a slightly bigger rectangle.
    - Resizes the raster to the MODIS resolution
    - Crops it to the real study area
    """
    cack = CACK()
    outfile = cack.get_outpath()
    ds = cack.get_dataset()
    cack_info = cack.get_info()

    lc_dataset = LC().get_dataset()
    lat = lc_dataset.variables["lat"][:]
    lon = lc_dataset.variables["lon"][:]

    metadata = raster_metadata()
    wkt = metadata["srs"].ExportToWkt()

    # metadata
    pixel_size = 1.0
    maxlat = 90
    minlon = 0  # starts in the middle of the wgs map (0° is on the far left)
    #               ulx   xres     xskew  uly  yskew   yres
    transform = [minlon, pixel_size, 0, maxlat, 0, -pixel_size]

    out = Dataset(outfile, "w", format="NETCDF4")
    out.Conventions = "CF-1.6"
    out.title = "CACK CM Kernel Austria"
    out.project = "CACK"
    out.institution = "EDI Data Portal"
    out.references = "https://portal.edirepository.org/nis/mapbrowse?packageid=edi.396.1"
    out.creation_date = str(datetime.now())
    out.comment = "CACK Climatological Mean Kernel 500m Austria, @Joseph Kiem"

    # create limited dimensions
    out.createDimension("month", size=12)
    out.createDimension("lat", size=len(lat))
    out.createDimension("lon", size=len(lon))

    crs = out.createVariable(
        "crs", "u2",
        dimensions=(),
        fill_value=None,
        zlib=False
    )
    crs.spatial_ref = wkt
    # -------------------------------------------------- #
    # uint16 month
    month_var  = out.createVariable(
        "month", "u2",
        dimensions=("month"),
        fill_value=None,
        zlib=False
    )
    month_var.set_auto_maskandscale(False)
    month_var.long_name = "month"
    month_var.units = "month"
    month_var.axis = "T"
    # -------------------------------------------------- #
    lat_var  = out.createVariable(
        "lat", "f8",
        dimensions=("lat"),
        fill_value=None,
        zlib=False
    )
    lat_var.set_auto_maskandscale(False)
    lat_var.long_name = "latitude"
    lat_var.units = "degrees_north"
    lat_var.axis = "Y"
    # -------------------------------------------------- #
    lon_var  = out.createVariable(
        "lon", "f8",
        dimensions=("lon"),
        fill_value=None,
        zlib=False
    )
    lon_var.set_auto_maskandscale(False)
    lon_var.long_name = "longitude"
    lon_var.units = "degrees_east"
    lon_var.axis = "X"
    # -------------------------------------------------- #
    cack_var = out.createVariable(
        "cack", "f8",
        dimensions=("month", "lat", "lon"),
        fill_value=None,
        zlib=False,
        shuffle=False,
    )
    cack_var.set_auto_maskandscale(False)
    cack_var.long_name = "Mean CACK Kernel"
    cack_var.units = cack_info["variables"]["CACK CM"]["Units"]
    cack_var.grid_mapping = "crs"

    lat_var[:] = lat
    lon_var[:] = lon

    for m in range(1, 13):
        # create mean cack
        var = ds["CACK CM"][m - 1, :, :]
        var = var.transpose([1, 0])  # y, x

        gdal_ds = gdal_array.OpenArray(var)
        gdal_ds.SetSpatialRef(metadata["srs"])
        gdal_ds.SetProjection(metadata["proj"])
        gdal_ds.SetGeoTransform(transform)

        vrt_cropped = gdal.Translate(
            "",
            gdal_ds,
            format="VRT",                    # virtual format
            projWin=metadata["box_big"],     # 'zoom in'
        )

        vrt_resized = gdal.Warp(
            "",
            vrt_cropped,
            outputType=gdal.GDT_Float32,     # same as input data
            format="VRT",                    # virtual format
            xRes=metadata["xres"],           # pixel sizes
            yRes=metadata["yres"],
            outputBounds=metadata["box"],    # crop to bounding box from MCD12Q1
            resampleAlg=gdal.GRA_Bilinear,   # upsample to 500m
        )

        cack_final = vrt_resized.ReadAsArray()

        month_var[m - 1] = m
        cack_var[m - 1, :, :] = cack_final

    out.close()
    print(f"Generated netCDF file '{outfile}'")
    return


def fuse_cack(fuse: FuSE) -> None:
    """
    Calculates kernel parameters for FuSE (GCA/RCA) dependent CACK.

    - calculates delta for timerange 1 / timerange 2
    - adds delta to 'current' CACK in order to create future CACK for a given GCA/RCA
    - results in monthly mean CACK (climatological CACK) for each year (2020-2100)

    Input files provided by Dr. Albin Hammerle.
    The files include the calculated 'k' (following Bright & O'Halloran 2019) from eurocordex data (changes in k for different scenarios).
    If not available, the CACK CM (above) could be used.
    """
    cack_ori = CACK()
    cack_ds = Dataset(cack_ori.get_outpath(), "r")
    cack_info = cack_ori.get_info()

    cack = CACK(file=fuse.get_filename() + ".nc")
    outfile = cack.get_outpath()
    ds = cack.get_dataset()

    mask = get_mask()

    lc_dataset = LC().get_dataset()
    ref_lat = lc_dataset.variables["lat"][:]
    ref_lon = lc_dataset.variables["lon"][:]

    lat = ds["lat"][:]
    lon = ds["lon"][:]

    time = ds["time"]
    time_units = "days since 1999-12-31 00:00:00"
    dtime = num2date(time[:], time_units).data

    y_res, x_res = mask.shape

    # first time range (14 years, 12 months,)
    months = list(range(1, 13))
    values = np.full((14, 12, y_res, x_res), np.nan)
    for i, y in enumerate(range(2006, 2020)):
        for m in months:
            t = [x.year == y and x.month == m for x in dtime]
            var = ds["k"][t, :, :][0, :, :]
            wgs84, wgs84_lat, wgs84_lon = _toWGS84(lat, lon, var)
            cropped = _crop(wgs84_lat, wgs84_lon, ref_lat, ref_lon, wgs84)
            interpolated = _interpolate(cropped, mask.shape)  # type: ignore
            interpolated[~mask] = np.NaN
            values[i, m - 1, :, :] = interpolated

    # monthly means
    monthly = np.nanmean(values, axis=0)
    values = None  # type: ignore

    # write to netCDF
    years = list(range(2020, 2101))
    metadata = raster_metadata()
    wkt = metadata["srs"].ExportToWkt()
    multiplier = 100

    out = Dataset(outfile, "w", format="NETCDF4")
    out.Conventions = "CF-1.6"
    out.title = "CACK CM Kernel Austria"
    out.project = "CACK"
    out.institution = "EDI Data Portal"
    out.references = "https://portal.edirepository.org/nis/mapbrowse?packageid=edi.396.1"
    out.creation_date = str(datetime.now())
    out.comment = "CACK Climatological Mean Kernel 500m Austria, @Joseph Kiem"

    # create limited dimensions
    out.createDimension("year", size=len(years))
    out.createDimension("month", size=12)
    out.createDimension("lat", size=y_res)
    out.createDimension("lon", size=x_res)

    crs = out.createVariable(
        "crs", "u2",
        dimensions=(),
        fill_value=None,
        zlib=False
    )
    crs.spatial_ref = wkt
    # -------------------------------------------------- #
    # uint16 year
    year_var = out.createVariable(
        "year", "u2",
        dimensions=("year"),
        fill_value=None,
        zlib=False
    )
    year_var.set_auto_maskandscale(False)
    year_var.long_name = "year"
    year_var.units = "year"
    year_var.axis = "T"
    # uint16 month
    month_var = out.createVariable(
        "month", "u2",
        dimensions=("month"),
        fill_value=None,
        zlib=False
    )
    month_var.set_auto_maskandscale(False)
    month_var.long_name = "month"
    month_var.units = "month"
    month_var.axis = "M"
    # -------------------------------------------------- #
    lat_var = out.createVariable(
        "lat", "f8",
        dimensions=("lat"),
        fill_value=None,
        zlib=False
    )
    lat_var.set_auto_maskandscale(False)
    lat_var.long_name = "latitude"
    lat_var.units = "degrees_north"
    lat_var.axis = "Y"
    # -------------------------------------------------- #
    lon_var = out.createVariable(
        "lon", "u2",
        dimensions=("lon"),
        fill_value=None,
        zlib=False
    )
    lon_var.set_auto_maskandscale(False)
    lon_var.long_name = "longitude"
    lon_var.units = "degrees_east"
    lon_var.axis = "X"
    # -------------------------------------------------- #
    cack_var = out.createVariable(
        "cack", "f8",
        dimensions=("year", "month", "lat", "lon"),
        fill_value=FILL,
        zlib=True,
        complevel=4,
        shuffle=False,
    )
    cack_var.set_auto_maskandscale(False)
    cack_var.long_name = "CACK Kernel"
    cack_var.units = cack_info["variables"]["CACK CM"]["Units"]
    cack_var.grid_mapping = "crs"
    cack_var.scale_factor = 1 / multiplier

    lat_var[:] = ref_lat
    lon_var[:] = ref_lon
    month_var[:] = months
    year_var[:] = years

    # second time range
    for i, y in enumerate(years):
        print(f"Year {y}")
        for m in months:
            t = [x.year == y and x.month == m for x in dtime]
            if np.any(t):
                var = ds["k"][t, :, :][0, :, :]
                wgs84, wgs84_lat, wgs84_lon = _toWGS84(lat, lon, var)
                cropped = _crop(wgs84_lat, wgs84_lon, ref_lat, ref_lon, wgs84)
                interpolated = _interpolate(cropped, mask.shape)  # type: ignore
                interpolated[~mask] = np.NaN

                # delta
                diff = monthly[m - 1, :, :] - interpolated

                # read cack and add
                c = cack_ds["cack"][m - 1, :, :]
                v = c + diff
                nans = np.isnan(v)
                v[nans] = FILL
                v[~nans] *= multiplier
                cack_var[i, m - 1, :, :] = v.astype(np.uint16)

    out.close()
    print(f"Generated netCDF file '{outfile}'")
    return
