#!/usr/bin/env python3
# File Name: albedo.py
# Description: Albedo calculation
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 09 Jan 2023 10:03:50
# Last Modified: 20 Sep 2023 21:27:25

# stdlib imports
import os
from datetime import datetime, timedelta
from math import ceil

# dependencies
import numpy as np
from netCDF4 import Dataset, num2date
from rustlib import alb as ralb

# local imports
import settings
from settings import DIRS
from util import daily, date2doy, date2lst, read_at_time

# fill value
FILL = np.iinfo(np.uint16).max


def read_mcd(time: list[datetime]) -> tuple[np.ndarray, np.ndarray]:
    """
    Reads MCD data from 2 files.
    This assumes that there are 2 BDRF files.
    The first that ends at 31.12.2010 and the second begins at 01.01.2011.
    Will set fillvalues to NaN / 'FILL'.
    """
    # find dates corresponding to files
    greater = np.array([True if t > datetime(2010, 12, 31) else False for t in time])
    first = np.array(time)[~greater]
    second = np.array(time)[greater]

    # read vars and shapes
    ds = settings.BDRF(0).get_dataset()
    time_var = ds["time"]
    cftime = num2date(time_var[:], time_var.units, calendar=time_var.calendar).data
    dtime = np.array(list(map(lambda x: datetime.fromisoformat(x.isoformat()), cftime)))
    mcd_var = ds["BRDF_Albedo_Parameters_shortwave"]
    qc_var = ds["BRDF_Albedo_Band_Mandatory_Quality_shortwave"]

    # prealloc
    mcd = np.full(
        (len(time), mcd_var.shape[1], mcd_var.shape[2], mcd_var.shape[3]),
        np.NaN,
        dtype=np.float64
    )
    qc = np.full(
        (len(time), mcd_var.shape[1], mcd_var.shape[2]),
        FILL,
        dtype=np.uint16
    )

    # number of already written days
    written = 0

    # read from first file
    if len(first) > 0:
        time_start = np.argmax(dtime >= first[0])
        time_end = np.argmax(dtime >= first[-1]) + 1
        idx = range(time_start, time_end)
        # set written
        written = len(list(idx))
        # read and fill
        mcd_temp = mcd_var[idx, :, :, :]
        mcd_temp[mcd_temp.mask] = np.NaN
        qc_temp = qc_var[idx, :, :]
        qc_temp[qc_temp.mask] = FILL
        # assign
        mcd[0:written, :, :, :] = mcd_temp
        qc[0:written, :, :] = qc_temp

    # read from second file
    if len(second) > 0:
        ds = settings.BDRF(1).get_dataset()
        time_var = ds["time"]
        cftime = num2date(time_var[:], time_var.units, calendar=time_var.calendar).data
        dtime = np.array(list(map(lambda x: datetime.fromisoformat(x.isoformat()), cftime)))
        mcd_var = ds["BRDF_Albedo_Parameters_shortwave"]
        qc_var = ds["BRDF_Albedo_Band_Mandatory_Quality_shortwave"]
        # time
        time_start = np.argmax(dtime >= second[0])
        time_end = np.argmax(dtime >= second[-1]) + 1
        idx = range(time_start, time_end)
        idx_len = len(list(idx))
        # read and fill
        mcd_temp = mcd_var[idx, :, :, :]
        mcd_temp[mcd_temp.mask] = np.NaN
        qc_temp = qc_var[idx, :, :]
        qc_temp[qc_temp.mask] = FILL
        # assign
        mcd[written:written + idx_len, :, :, :] = mcd_temp
        qc[written:written + idx_len, :, :] = qc_temp
    return mcd, qc


def _coords() -> tuple[np.ndarray, np.ndarray, dict]:
    """
    Reads coordinates and info from MCD12Q1.
    """
    file = settings.LC().get_outpath()
    ds = Dataset(file, "r")
    lat = ds["lat"][:]
    lon = ds["lon"][:]

    info = settings.LC().get_info()
    return lon, lat, info


def calculate_albedo(n: int = 7) -> None:
    """
    Calculates albedo from 2002 to 2019 using MCD43A1 and BESS data.
    'n' sets the number of days it will include in one iteration.
    Converted from MATLAB code provided by Dr. Albin Hammerle.

    Writes the albedo data to netCDF4.
    QC:     0 = good (full)
            1 = processed
    """
    # time
    start = datetime(2002, 1, 1, 0, 0, 0)
    complete_range = daily(start, datetime(2019, 12, 31))
    time_len = len(complete_range)

    # bess data
    bess_dir = os.path.join(DIRS["output_dir"], DIRS["bess"])

    # lat and lon meshgrid
    lon, lat, info = _coords()
    x, y = np.meshgrid(lon, lat)
    x = x[:, :, np.newaxis]
    y = y[:, :, np.newaxis]

    # create CF-1.6 conform time units
    date_string = start.strftime("%Y-%m-%d %H:%M:%S")
    time_units = f"days since {date_string}"
    time_calendar = "julian"  # not sure whether gregorian / julian matters for daily resolution..

    # set data for netcdf
    outfile = os.path.join(DIRS["output_dir"], DIRS["albedo"])
    multiplier = 100

    ## EDIT
    # ds = Dataset(outfile, "r+", clobber=True, format="NETCDF4")

    # create the netCDF file
    out = Dataset(outfile, "w", format="NETCDF4")
    out.Conventions = "CF-1.6"
    out.title = "Albedo Austria"
    out.creation_date = str(datetime.now())
    out.comment = "Calculated albedo values Austria, @Joseph Kiem"
    out.createDimension("time", size=time_len)
    out.createDimension("lat", size=len(lat))
    out.createDimension("lon", size=len(lon))
    # variables----------------------------------------- #
    crs = out.createVariable(
        "crs", "u2",
        dimensions=(),
        fill_value=None,
        zlib=False
    )
    crs.spatial_ref = info["variables"]["crs"]["spatial_ref"]
    # -------------------------------------------------- #
    time_var  = out.createVariable(
        "time", "u2",
        dimensions=("time"),
        fill_value=None,
        zlib=False
    )
    time_var.set_auto_maskandscale(False)
    time_var.long_name = "time"
    time_var.units = time_units
    time_var.calendar = time_calendar
    time_var.axis = "T"
    # -------------------------------------------------- #
    lat_var  = out.createVariable(
        "lat", "f8",
        dimensions=("lat"),
        fill_value=None,
        zlib=False
    )
    lat_var.set_auto_maskandscale(False)
    lat_var.long_name = "latitude"
    lat_var.units = "degrees_north"
    lat_var.axis = "Y"
    # -------------------------------------------------- #
    lon_var  = out.createVariable(
        "lon", "f8",
        dimensions=("lon"),
        fill_value=None,
        zlib=False
    )
    lon_var.set_auto_maskandscale(False)
    lon_var.long_name = "longitude"
    lon_var.units = "degrees_east"
    lon_var.axis = "X"
    # -------------------------------------------------- #
    alb_var = out.createVariable(
        "bw", "u2",
        dimensions=("time", "lat", "lon"),
        fill_value=FILL,
        zlib=True,
        complevel=4,
        shuffle=True,
        chunksizes=(1, len(lat), len(lon)),
    )
    alb_var.set_auto_maskandscale(False)
    alb_var.long_name = "mean daily albedo"
    alb_var.units = "unitless"
    alb_var.grid_mapping = "crs"
    alb_var.scale_factor = 1 / multiplier
    # -------------------------------------------------- #
    qc_var = out.createVariable(
        "qc", "u2",
        dimensions=("time", "lat", "lon"),
        fill_value=FILL,
        zlib=True,
        complevel=4,
        shuffle=True,
        chunksizes=(1, len(lat), len(lon)),
    )
    qc_var.set_auto_maskandscale(False)
    qc_var.long_name = "quality control flag; MCD43A1"
    qc_var.units = "unitless"
    qc_var.grid_mapping = "crs"

    lat_var[:] = lat
    lon_var[:] = lon
    time_var[:] = list(range(0, time_len))

    # loop over time range with interval
    steps = ceil(len(complete_range) / n)
    nwritten = 0
    time_range = None

    for i, t in enumerate(range(0, len(complete_range), n)):
        print(f"{datetime.now()}: Albedo {i+1} / {steps}")
        try:
            time_range = complete_range[t:t + n]
        except:
            time_range = complete_range[t:]

        # create hourly range from start to end of time_range (offset by 30 minutes)
        hourly = [
            start + timedelta(minutes=30) + timedelta(hours=x)
            for x in range(0, (time_range[-1] + timedelta(days=1) - time_range[0]).days * 24)
        ]

        # create doy and lst as decimals
        doy = np.array(list(map(lambda x: date2doy(x), hourly)))
        doy = doy.reshape([1, 1, len(doy)])
        lst = np.array(list(map(lambda x: date2lst(x), hourly)))
        lst = lst.reshape([1, 1, len(lst)])

        # calculate radiation and solar zenith angle
        rs, sza = ralb.extraterrestrial_radiation(doy, lst, y, x)

        # read bess and bring to shape (y, x, t)
        bess = read_at_time(bess_dir, "Rg", time_range).transpose((1, 2, 0))
        shape = bess.shape

        # correction factor
        rs_cf = ralb.correction_factor(bess, rs)
        bess, rs = np.ndarray([]), np.ndarray([])  # clear

        # partition diffuse fraction of radiation
        fdif = ralb.partition_diffuse(sza, rs_cf)

        # read mcd; remove axis; bring to shape (y, x, t) and repeat it hourly.
        gs, qc = read_mcd(time_range)
        g0 = gs[:, :, :, 0].transpose((1, 2, 0))
        g1 = gs[:, :, :, 1].transpose((1, 2, 0))
        g2 = gs[:, :, :, 2].transpose((1, 2, 0))
        gs = np.array([])  # clear

        # calculate albedo
        alb = ralb.albedo(sza, fdif, rs_cf, g0, g1, g2)
        g0, g1, g2 = np.array([]), np.array([]), np.array([])

        # daily mean albedo
        # creates warning for first iteration
        # can be ignored (I think), since means are calculated anyway
        alb_dm = np.full(shape, np.NaN)
        mean_range = int(len(hourly) / 24)
        for a in range(0, mean_range):
            alb_dm[:, :, a] = np.nanmean(alb[:, :, a * 24:a * 24 + 24], axis=2)

        alb_dm = alb_dm.transpose((2, 0, 1))  # t, y, x

        # set fill and scale
        nans = np.isnan(alb_dm)
        alb_dm[nans] = FILL
        alb_dm[~nans] *= multiplier

        # write
        alb_var[nwritten:nwritten + len(time_range), :, :] = alb_dm.astype(np.uint16)
        qc_var[nwritten:nwritten + len(time_range), :, :] = qc.astype(np.uint16)
        nwritten = nwritten + len(time_range)

    out.close()
    print(f"Generated netCDF file '{outfile}'")
    return

