#!/usr/bin/env python3
# File Name: dem.py
# Description: DEM data preparation
# Docs: https://gdal.org/api/python/osgeo.gdal.html#osgeo.gdal.DEMProcessing
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: Thu 14 Jul 2022 09:54:35 AM CEST
# Last Modified: 24 Feb 2024 12:47:07

# stdlib imports
from datetime import datetime
from os.path import join
from tempfile import gettempdir

# dependencies
from netCDF4 import Dataset
from osgeo import gdal, gdal_array

# local imports
import settings
from prep.lc import raster_metadata


def prepare_dem() -> None:
    """
    Downsamples the DEM data to 500m resolution (same as other MODIS data).
    Calculates aspect and slope from the DEM.
    NOTE: the original (fuse study area extent) downsampled DEM was
          slightly offset on the coordinate grid.
    NOTE: this uses a slightly bigger DEM dataset (+1 decimal degree in every direction)
          extracted from AppEEARS which then gets cropped
    """
    print("Processing...")
    dem = settings.DEM()
    dem_dataset = dem.get_dataset()
    dem_path = dem.get_path()
    dem_info = dem.get_info()
    outfile = dem.get_outpath()
    fill = dem_info["variables"]["ASTER_GDEM_DEM"]["_FillValue"]

    metadata = raster_metadata()
    dem_gdal = gdal.Open(f"NETCDF:{dem_path}:ASTER_GDEM_DEM", gdal.GA_ReadOnly)
    transform = dem_gdal.GetGeoTransform()

    # read array as GDAL dataset and set metadata
    dem_data = dem_dataset.variables["ASTER_GDEM_DEM"][0, :, :]
    ds = gdal_array.OpenArray(dem_data)
    ds.SetSpatialRef(metadata["srs"])
    ds.SetProjection(metadata["proj"])
    ds.SetGeoTransform(transform)

    # downsample dem to target resolution and crop to MODIS data
    vrt = gdal.Warp(
        "",
        ds,
        outputType=gdal.GDT_Int16,       # same as input data
        format="VRT",                    # virtual format
        xRes=metadata["xres"],           # pixel sizes
        yRes=metadata["yres"],
        outputBounds=metadata["box"],    # crop to bounding box from MCD12Q1
        srcNodata=fill,                  # nodata / fillvalues
        dstNodata=fill,                  # nodata / fillvalues
        resampleAlg=gdal.GRA_Average,
    )

    dem_final = vrt.ReadAsArray()

    # calculate aspect and slope (write tempfiles as VRT is not supported)
    # use GDAL instead of richdem. It wouldn't compile for python3.11
    aspect_ds = gdal.DEMProcessing(
        join(gettempdir(), "aspect.tif"),
        vrt,
        processing="aspect",
        format="Gtiff",
        alg="Horn",
        trigonometric=False,
        zeroForFlat=True,  # allow flat surfaces to have an aspect of 0
        computeEdges=True,
    )
    slope_ds = gdal.DEMProcessing(
        join(gettempdir(), "slope.tif"),
        vrt,
        processing="slope",
        format="Gtiff",
        alg="Horn",
        scale=111120,  # you can use scale=111120 if the vertical units are meters
        computeEdges=True,
    )
    aspect = aspect_ds.ReadAsArray()
    slope = slope_ds.ReadAsArray()

    lat_len = len(metadata["lat"])
    lon_len = len(metadata["lon"])

    # create the netCDF file
    out = Dataset(outfile, "w", format="NETCDF4")
    out.Conventions = dem_info["attributes"]["Conventions"]
    out.title = dem_info["attributes"]["title"]
    out.institution = dem_info["attributes"]["institution"]
    out.source = dem_info["attributes"]["institution"]
    out.creation_date = str(datetime.now())
    out.comment = "ASTER_GDEM_DEM for Austria downsampled to 500m, @Joseph Kiem"

    # create limited dimensions
    out.createDimension("time", size=1)
    out.createDimension("lat", size=lat_len)
    out.createDimension("lon", size=lon_len)

    # variables----------------------------------------- #
    crs = out.createVariable(
        "crs", "u2",
        dimensions=(),
        fill_value=None,
        zlib=False
    )
    crs.spatial_ref = dem_info["variables"]["crs"]["spatial_ref"]
    # -------------------------------------------------- #
    # uint16 time
    time_var  = out.createVariable(
        "time", "u2",
        dimensions=("time"),
        fill_value=None,
        zlib=False
    )
    time_var.set_auto_maskandscale(False)
    time_var.long_name = "time"
    time_var.units = dem_info["variables"]["time"]["units"]
    time_var.calendar = dem_info["variables"]["time"]["calendar"]
    time_var.axis = "T"
    # -------------------------------------------------- #
    lat_var  = out.createVariable(
        "lat", "f8",
        dimensions=("lat"),
        fill_value=None,
        zlib=False
    )
    lat_var.set_auto_maskandscale(False)
    lat_var.long_name = "latitude"
    lat_var.units = "degrees_north"
    lat_var.axis = "Y"
    # -------------------------------------------------- #
    lon_var  = out.createVariable(
        "lon", "f8",
        dimensions=("lon"),
        fill_value=None,
        zlib=False
    )
    lon_var.set_auto_maskandscale(False)
    lon_var.long_name = "longitude"
    lon_var.units = "degrees_east"
    lon_var.axis = "X"
    # -------------------------------------------------- #
    dem_var = out.createVariable(
        "DEM", "i2",
        dimensions=("time", "lat", "lon"),
        fill_value=fill,
        zlib=True,
        complevel=4,
        shuffle=True,
        chunksizes=(1, lat_len, lon_len),
    )
    dem_var.set_auto_maskandscale(False)
    dem_var.long_name = dem_info["variables"]["ASTER_GDEM_DEM"]["long_name"]
    dem_var.units = dem_info["variables"]["ASTER_GDEM_DEM"]["units"]
    dem_var.grid_mapping = "crs"
    # -------------------------------------------------- #
    slope_var = out.createVariable(
        "slope", "f8",
        dimensions=("time", "lat", "lon"),
        fill_value=fill,
        zlib=True,
        complevel=4,
        shuffle=True,
        chunksizes=(1, lat_len, lon_len),
    )
    slope_var.set_auto_maskandscale(False)
    slope_var.long_name = "Slope"
    slope_var.units = "degrees"
    slope_var.grid_mapping = "crs"
    # -------------------------------------------------- #
    aspect_var = out.createVariable(
        "aspect", "f8",
        dimensions=("time", "lat", "lon"),
        fill_value=fill,
        zlib=True,
        complevel=4,
        shuffle=True,
        chunksizes=(1, lat_len, lon_len),
    )
    aspect_var.set_auto_maskandscale(False)
    aspect_var.long_name = "Aspect"
    aspect_var.units = "degrees"
    aspect_var.grid_mapping = "crs"
    # -------------------------------------------------- #
    # write data
    lat_var[:] = metadata["lat"]
    lon_var[:] = metadata["lon"]
    time_var[:] = dem_dataset.variables["time"][:]
    dem_var[:] = dem_final
    slope_var[:] = slope
    aspect_var[:] = aspect

    out.close()
    print(f"Generated netCDF file '{outfile}'")
    return
