#!/usr/bin/env python3
# File Name: lc.py
# Description: Landcover data preparation
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 01 Jan 2023 14:20:14
# Last Modified: 07 Dez 2023 17:10:03

# stdlib import
from datetime import date, datetime

# dependencies
import numpy as np
from cftime import to_tuple
from netCDF4 import Dataset, num2date
from osgeo import gdal

# local imports
import settings
from util import mode


def raster_metadata() -> dict:
    """
    Reads the pixel size and lat lon coordinates from the MCD12Q1 file.
    These are needed to crop and resize other data.
    NOTE: MCD12Q1 is needed for this to work.
    """
    # read lat lon from MCD12Q1
    lc_file = settings.LC().get_path()
    lc_dataset = settings.LC().get_dataset()
    lc_lat = lc_dataset.variables["lat"][:]
    lc_lon = lc_dataset.variables["lon"][:]

    # read pixel size and bounding box from MCD12Q1
    # NOTE: BESS data does not have srs or proj set, so taking it from MCD12Q1 (WGS84, etc.)
    lc = gdal.Open(f"NETCDF:{lc_file}:LC_Type1", gdal.GA_ReadOnly)
    srs = lc.GetSpatialRef()
    proj = lc.GetProjection()
    lc_minlon, lc_xres, _, lc_maxlat, _, lc_yres = lc.GetGeoTransform()
    lc_maxlon = lc_minlon + (lc.RasterXSize * lc_xres)
    lc_minlat = lc_maxlat + (lc.RasterYSize * lc_yres)

    bounding_box_big = [lc_minlon - 1, lc_maxlat + 1, lc_maxlon + 1, lc_minlat - 1]
    # NOTE: gdal.Warp() -outputBounds is organized differently
    bounding_box = [lc_minlon, lc_minlat, lc_maxlon, lc_maxlat]

    metadata = {
        "srs": srs,
        "proj": proj,
        "xres": lc_xres,
        "yres": lc_yres,
        "lat": lc_lat,
        "lon": lc_lon,
        "box_big": bounding_box_big,
        "box": bounding_box,
    }

    return metadata


def lc_mode(threshold: float = 80.0, years: list[int] = list(range(2002, 2020))) -> None:
    """
    Calculates the mode landcover and filters each pixel over the time dimension
    using the frequency and some threshold.
    The filtered pixels are set 'Unclassified'.
    NOTE: Values outside of Austria are filtered via the FuSE mask (therefore the FuSE mask is needed).
    """
    lc = settings.LC()
    info = lc.get_info()
    lc_dataset = lc.get_dataset()
    outfile = lc.get_outpath()

    # get land cover names and values
    lc_type_var = lc_dataset.variables["LC_Type1"]
    attr_names: list[str] = [x for x in lc_type_var.ncattrs() if str(x)[0].isupper()]
    attrs = {}
    for name in attr_names:
        attrs[name] = lc_type_var.getncattr(name)

    # get time and indices
    lc_time_var = lc_dataset.variables["time"]
    dtime = num2date(lc_time_var[:], lc_time_var.units, calendar=lc_time_var.calendar).data
    time_idx = [to_tuple(t)[0] in years for t in dtime]

    lc_type = lc_type_var[time_idx, :, :]

    # calculate mode and freq
    # filter out pixels that do not have the same land cover for at least some percentage of the time (threshold)
    print("Calculating mode..")
    m, f = np.apply_along_axis(lambda x: mode(x), 0, lc_type)
    n = len(years) / 100 * threshold
    m[f < n] = attrs["Unclassified"]

    # mask values outside of Austria
    mask = settings.get_mask()
    m[~mask] = attrs["Unclassified"]

    # prepare other variables
    date_time = date(2019, 1, 1)
    date_string = date_time.strftime("%Y-%m-%d %H:%M:%S")
    lat = lc_dataset.variables["lat"][:]
    lon = lc_dataset.variables["lon"][:]

    # create dataset
    out = Dataset(outfile, "w", format="NETCDF4")
    out.Conventions = info["attributes"]["Conventions"]
    out.title = info["attributes"]["title"]
    out.institution = info["attributes"]["institution"]
    out.source = info["attributes"]["source"]
    out.creation_date = str(datetime.now())
    out.comment = "Landcover mode, @Joseph Kiem"

    out.createDimension("time", size=1)
    out.createDimension("lat", size=len(lat))
    out.createDimension("lon", size=len(lon))

    # variables----------------------------------------- #
    crs = out.createVariable(
        "crs", "u2",
        dimensions=(),
        fill_value=None,
        zlib=False
    )
    crs.spatial_ref = info["variables"]["crs"]["spatial_ref"]
    # -------------------------------------------------- #
    time_var  = out.createVariable(
        "time", "u2",
        dimensions=("time"),
        fill_value=None,
        zlib=False
    )
    time_var.set_auto_maskandscale(False)
    time_var.long_name = "time"
    time_var.units = f"days since {date_string}"
    time_var.calendar = info["variables"]["time"]["calendar"]
    time_var.axis = info["variables"]["time"]["axis"]
    # -------------------------------------------------- #
    lat_var  = out.createVariable(
        "lat", "f8",
        dimensions=("lat"),
        fill_value=None,
        zlib=False
    )
    lat_var.set_auto_maskandscale(False)
    lat_var.standard_name = info["variables"]["lat"]["standard_name"]
    lat_var.units = info["variables"]["lat"]["units"]
    lat_var.axis = info["variables"]["lat"]["axis"]
    # -------------------------------------------------- #
    lon_var  = out.createVariable(
        "lon", "f8",
        dimensions=("lon"),
        fill_value=None,
        zlib=False
    )
    lon_var.set_auto_maskandscale(False)
    lon_var.standard_name = info["variables"]["lon"]["standard_name"]
    lon_var.units = info["variables"]["lon"]["units"]
    lon_var.axis = info["variables"]["lon"]["axis"]
    # -------------------------------------------------- #
    type_var  = out.createVariable(
        "type", "u8",
        dimensions=("time", "lat", "lon"),
        fill_value=None,
        zlib=False
    )
    type_var.set_auto_maskandscale(False)
    type_var.coordinates = info["variables"]["LC_Type1"]["coordinates"]
    type_var.grid_mapping = info["variables"]["LC_Type1"]["grid_mapping"]
    for attr, val in attrs.items():
        type_var.setncattr(attr, val)

    lat_var[:] = lat
    lon_var[:] = lon
    time_var[:] = 0
    type_var[:] = m

    out.close()
    print(f"Generated netCDF file '{outfile}'")
    return

