#!/usr/bin/env python3
# File Name: fuse.py
# Description: FuSE-AT data preparation functions and study area shapefile generation.
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: Thu 14 Jul 2022 09:54:35 AM CEST
# Last Modified: 12 Feb 2024 16:22:05

# stdlib imports
import os
from datetime import datetime
from pathlib import Path
from zipfile import ZipFile

# dependencies
import cftime
import numpy as np
from netCDF4 import Dataset, num2date
from osgeo import gdal, gdal_array, ogr, osr

# local imports
import settings
from prep.lc import raster_metadata


def write_study_area(file: str, shp: settings.StudyArea | settings.BiggerStudyArea) -> None:
    """
    Fetches the bounds of the FuSE data study area and writes them to a Shapefile.

    WHY?:
        The FuSE data is already cropped to Austria. In order to have the same spatial extent
        for all the data, this is the easiest way to bring them to a common coordinate grid.
        The previous attempt was to use a Shapefile for Austria and crop the other dataset with that,
        but then the FuSE data is not on the same coordinate grid.
        Furthermore the FuSE data has smaller bounds than the AUT_border_WGS84 shapefile..
    NOTE: only needed once
    NOTE: the 'writing to file' part is not very elegant, but works for now.
    """
    # read reference metadata
    metadata = raster_metadata()
    offset = shp.get_offset()

    # create coords (offset in decimal degrees)
    ring = ogr.Geometry(ogr.wkbLinearRing)
    ring.AddPoint(metadata["minlon"] - offset, metadata["maxlat"] + offset)
    ring.AddPoint(metadata["maxlon"] + offset, metadata["maxlat"] + offset)
    ring.AddPoint(metadata["maxlon"] + offset, metadata["minlat"] - offset)
    ring.AddPoint(metadata["minlon"] - offset, metadata["minlat"] - offset)
    ring.AddPoint(metadata["minlon"] - offset, metadata["maxlat"] + offset)

    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(ring)

    # set shapefile parameters
    driver = ogr.GetDriverByName("Esri Shapefile")
    ds = driver.CreateDataSource(shp.get_path())
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)

    # create layer
    layer = ds.CreateLayer("area", srs, ogr.wkbPolygon)
    layer.CreateField(ogr.FieldDefn("id", ogr.OFTInteger))
    defn = layer.GetLayerDefn()

    # create feature
    feat = ogr.Feature(defn)
    feat.SetField("id", 0)

    # create geometry and add feature
    geom = ogr.CreateGeometryFromWkb(poly.ExportToWkb())
    feat.SetGeometry(geom)
    layer.CreateFeature(feat)

    # flush it
    ds = layer = feat = geom = None

    # zip the files for AppEEARS
    name = shp.get_filename()
    directory = shp.get_directory()
    zip_path = os.path.join(directory, f"{name}.zip")

    filelist = os.listdir(directory)

    # remove old shapefile zip
    if f"{name}.zip" in filelist:
        os.remove(zip_path)
        filelist = os.listdir(directory)

    # filter only the wanted files
    filelist = [f for f in filelist if Path(f).stem == name]

    zip = ZipFile(zip_path, "w")
    os.chdir(directory)

    for f in filelist:
        zip.write(f)
        os.remove(f)
    zip.close()

    print(f"Generated '{zip_path}'")
    return


def _get_mask(file: str) -> np.ndarray:
    """
    Warps and resizes the FuSE mask using GDAL.
    NOTE: only needed once
    """
    # read reference metadata
    metadata = raster_metadata()

    # read the mask
    ds = gdal.Open(f"NETCDF:{file}:mask", gdal.GA_ReadOnly)

    # warp and resize the mask
    vrt = gdal.Warp(
        "",
        ds,
        dstSRS="EPSG:4326",
        outputType=gdal.GDT_UInt16,
        format="VRT",
        xRes=metadata["xres"],
        yRes=metadata["yres"],
        resampleAlg=gdal.GRA_NearestNeighbour,  # for binary data
        targetAlignedPixels=True                # aligns the coordinate grid to the MODIS data
    )

    mask = vrt.ReadAsArray()
    return mask


def write_mask(fuse: settings.FuSE) -> None:
    """
    Writes mask to file.
    """
    ds = Dataset(fuse.get_outpath(), "r")
    mask = ds["mask"][:]
    lat_len, lon_len = mask.shape

    outfile = os.path.join(settings.DIRS["output_dir"], "mask.nc")
    out = Dataset(outfile, "w", format="NETCDF4")
    out.createDimension("lat", size=lat_len)
    out.createDimension("lon", size=lon_len)
    mask_var = out.createVariable(
        "mask", "u2",
        dimensions=("lat", "lon"),
        fill_value=None,
        zlib=False
    )
    mask_var.set_auto_maskandscale(False)
    mask_var[:] = mask
    out.close()


def prepare_fuse(fuse: settings.FuSE) -> None:
    """
    - Takes a subset of the variables (based on time).
    - Warps the FuSE data to the WGS84 datum and resizes it to 500m resolution
      (same as the MODIS data) using GDAL.
    - Writes the data to a new netCDF file.
    NOTE: specify time range with 'time_start'
          ('time_end' was removed, but could be added again easily)
    NOTE: if compression takes too long, files can be compressed later, e.g:
          http://climate-cms.wikis.unsw.edu.au/NetCDF_Compression_Tools
    NOTE: reads crs metadata from MCD12Q1, so that one is needed.
    NOTE: GDAL warping and resizing is very fast;
          the writing / compression takes a while.
    NOTE: this is pretty much what AppEEARS does:
          https://appeears.earthdatacloud.nasa.gov/help?section=area%2Freprojection
    """
    fuse_dataset = fuse.get_dataset()
    file = fuse.get_path()
    fuse_info = fuse.get_info()
    fill = fuse_info["variables"]["HS"]["_FillValue"]
    outfile = fuse.get_outpath()

    # time start (including)
    # the different FuSE dataset's time dimension have different length (difference of 1)
    # guess either until '30-Dec-2100' or '31-Dec-2100'
    time_start = cftime.DatetimeProlepticGregorian(2000, 1, 1)

    # read time
    fuse_time_var = fuse_dataset.variables["time"]
    dtime = num2date(fuse_time_var[:], fuse_time_var.units, calendar=fuse_time_var.calendar).data

    # find the index for the specified starting time in the array and the length
    # then calculate the end
    time_index_start = np.argmax(dtime >= time_start)
    time_len = len(dtime[time_index_start:])
    time_index_end = time_index_start + time_len

    # read reference metadata
    metadata = raster_metadata()
    gdal_f = gdal.Open(f"NETCDF:{file}:mask", gdal.GA_ReadOnly)
    srs = gdal_f.GetSpatialRef()
    proj = gdal_f.GetProjection()
    transform = gdal_f.GetGeoTransform()

    # spatial metadata
    lat_len = len(metadata["lat"])
    lon_len = len(metadata["lon"])
    srs2 = osr.SpatialReference()
    srs2.ImportFromEPSG(4326)
    wkt = srs2.ExportToWkt()

    # other
    multiplier = 100
    uint16_max = np.iinfo(np.uint16).max

    # create the netCDF file
    out = Dataset(outfile, "w", format="NETCDF4")
    out.Conventions = fuse_info["attributes"]["Conventions"]
    out.title = fuse_info["attributes"]["title"]
    out.institution = fuse_info["attributes"]["institution"]
    out.project = fuse_info["attributes"]["project"]
    out.creation_date = str(datetime.now())
    out.original_file = fuse.get_filename()
    out.comment = "Warped and resized FuSE-AT snowgrid data, @Joseph Kiem"

    # create limited dimensions
    out.createDimension("time", size=time_len)
    out.createDimension("lat", size=lat_len)
    out.createDimension("lon", size=lon_len)

    # variables----------------------------------------- #
    crs = out.createVariable(
        "crs", "u2",
        dimensions=(),
        fill_value=None,
        zlib=False
    )
    crs.spatial_ref = wkt
    # -------------------------------------------------- #
    mask_var = out.createVariable(
        "mask", "u1",
        dimensions=("lat", "lon"),
        fill_value=None,
        zlib=False
    )
    mask_var.set_auto_maskandscale(False)
    mask_var.long_name = fuse_info["variables"]["mask"]["long_name"]
    mask_var.units = fuse_info["variables"]["mask"]["units"]
    mask_var.grid_mapping = "crs"
    # -------------------------------------------------- #
    # coverting to uint16 should be okay here (decimal not needed)
    time_var  = out.createVariable(
        "time", "u2",
        dimensions=("time"),
        fill_value=None,
        zlib=False
    )
    time_var.set_auto_maskandscale(False)
    time_var.long_name = "time"
    time_var.units = fuse_info["variables"]["time"]["units"]
    time_var.calendar = fuse_info["variables"]["time"]["calendar"]
    time_var.axis = "T"
    # -------------------------------------------------- #
    lat_var  = out.createVariable(
        "lat", "f8",
        dimensions=("lat"),
        fill_value=None,
        zlib=False
    )
    lat_var.set_auto_maskandscale(False)
    lat_var.long_name = "latitude"
    lat_var.units = "degrees_north"
    lat_var.axis = "Y"
    # -------------------------------------------------- #
    lon_var  = out.createVariable(
        "lon", "f8",
        dimensions=("lon"),
        fill_value=None,
        zlib=False
    )
    lon_var.set_auto_maskandscale(False)
    lon_var.long_name = "longitude"
    lon_var.units = "degrees_east"
    lon_var.axis = "X"
    # -------------------------------------------------- #
    hs_var = out.createVariable(
        "HS", "u2",
        dimensions=("time", "lat", "lon"),
        fill_value=uint16_max,
        zlib=True,
        complevel=4,
        shuffle=True,
        chunksizes=(1, lat_len, lon_len),
    )
    hs_var.set_auto_maskandscale(False)
    hs_var.long_name = fuse_info["variables"]["HS"]["long_name"]
    hs_var.units = fuse_info["variables"]["HS"]["units"]
    hs_var.grid_mapping = "crs"
    hs_var.scale_factor = 1 / multiplier
    # -------------------------------------------------- #
    # write static data
    lat_var[:] = metadata["lat"]
    lon_var[:] = metadata["lon"]
    mask_var[:] = _get_mask(file)

    # range excludes last entry
    for i, ti in enumerate(range(time_index_start, time_index_end, 1)):
        print(f"{datetime.now()}: FuSE {i + 1}/{time_len}")
        # GDAL does not allow slicing (or atleast I couldn't figure it out)..
        #   - the only way is to convert the snowheight array slice to a GDAL dataset
        #   - should not alter the values, since it is still an array, but in GDAL
        # NOTE: the netCDF4 module does scale, addoffset and fill automatically
        # NOTE: data = t, y, x
        hs = fuse_dataset.variables["HS"][ti, :, :]
        # bring the data to the right orientation (data is flipped upside down)
        # NOTE: endresult is still y, x
        hs = hs.transpose([1, 0])  # x, y
        hs = np.rot90(hs, k=1)     # rotate to the right orientation -> y, x

        # read array as GDAL dataset and set metadata
        ds = gdal_array.OpenArray(hs)
        ds.SetSpatialRef(srs)
        ds.SetProjection(proj)
        ds.SetGeoTransform(transform)

        # warp and resize snow height
        # - bilinear does yield good results if used in gdal.Warp ()
        # - still preserves nodata values
        # - maximum values are a little bit lower, which is expected
        vrt = gdal.Warp(
            "",
            ds,
            dstSRS="EPSG:4326",                # WGS84
            outputType=gdal.GDT_Float64,
            format="VRT",                      # virtual format
            srcNodata=fill,                    # nodata / fillvalues
            dstNodata=fill,
            xRes=metadata["xres"],             # pixel sizes
            yRes=metadata["yres"],
            resampleAlg=gdal.GRA_Bilinear,
            targetAlignedPixels=True           # aligns the coordinate grid to the MODIS data
        )

        # read dataset back as array
        # the new array should have the same shape (y, x) and the same resolution as the MODIS data
        hs_final = vrt.ReadAsArray()
        ds = vrt = None

        multiply_index = hs_final != fill
        fill_index = hs_final == fill
        # fillvalues, scale, conversion
        hs_final[fill_index] = uint16_max
        hs_final[multiply_index] *= multiplier
        hs_ready = hs_final.astype(np.uint16)

        # write data
        time_var[i] = fuse_time_var[ti].astype(np.uint16)
        hs_var[i, :, :] = hs_ready

    out.close()
    print(f"Generated netCDF file '{outfile}'")
    return

