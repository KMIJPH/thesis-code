#!/usr/bin/env python3
# File Name: co2.py
# Description: TDEE / CO2 plotting
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 14 Dez 2023 22:52:24
# Last Modified: 25 Jan 2024 23:39:13


# stlib imports
import glob
import os
from typing import Any

# dependencies
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots

# local imports
from plot.radiative_forcing import gca_rca, rcp
from settings import DIRS, FigureSettings

fs = FigureSettings()

RCPS = fs.rcps
X = list(range(2020, 2101))


def _plot_panel(fig: go.Figure, rcp: str, data: dict[str, Any], scale: tuple[float, float], col: int, legend: bool) -> tuple[float, float]:
    """
    Plots one panel of the TDEE plot.
    """
    color = fs.rcp_colors[rcp]

    tdee = data["tdee"]["combined"]
    cum_tdee = np.cumsum(tdee)

    if np.nanmin(cum_tdee) < scale[0]:
        scale = (np.nanmin(cum_tdee), scale[1])
    if np.nanmax(cum_tdee) > scale[1]:
        scale = (scale[0], np.nanmax(cum_tdee))

    fig.add_trace(
        go.Scatter(
            x=X,
            y=cum_tdee,
            showlegend=True if legend else False,
            name=rcp,
            line=dict(color=color, width=3, dash="dot"),
            mode="lines",
        ),
        col=col, row=1,
    )

    nans = np.isnan(cum_tdee)
    s = np.full(cum_tdee.shape, np.NaN)
    last_false = np.where(nans == False)[0][-1]
    s[last_false] = np.nansum(tdee)

    fig.add_trace(
        go.Scatter(
            x=X,
            y=s,
            showlegend=False,
            name=rcp,
            marker=dict(color=color, size=10, symbol="square"),
            mode="markers",
        ),
        col=col, row=1,
    )
    return scale


def co2():
    """
    Plots TDEE.
    """
    print("Plotting tdee.")
    files = glob.glob(os.path.join(DIRS["results"], "rf_*.csv"))
    files = list(filter(lambda x: "_yearly" in x, files))

    data = {}

    for file in files:
        r = rcp(file)
        model = gca_rca(file)
        # rf = pd.read_csv(file, index_col=0)
        d = pd.read_csv(file.replace("rf", "tdee").replace("_yearly", ""), index_col=0)

        if model not in data:
            data[model] = [{"rcp": r, "tdee": d}]
        else:
            data[model] = data[model] + [{"rcp": r, "tdee": d}]

    models = list(data.keys())
    models.sort()
    print(models)

    fig = make_subplots(
        rows=1,
        cols=2,
        x_title="Time",
        y_title="Cumulative TDEE (Mt eCO<sub>2</sub>)",
        subplot_titles=["CLMcom", "HadGEM"],
    )

    scale = (np.inf, -np.inf)

    for i, r in enumerate(RCPS):
        f1 = list(filter(lambda x: x["rcp"] == r, data[models[0]]))[0]
        scale = _plot_panel(fig, r, f1, scale, 1, True)

        f2 = list(filter(lambda x: x["rcp"] == r, data[models[1]]))[0]
        scale = _plot_panel(fig, r, f2, scale, 2, False)

    scale = (scale[0] - (scale[0] / 100), scale[1] + (scale[1] / 100))

    fs.margin.update(l=150, t=75, b=120)
    fig.update_layout(
        plot_bgcolor="#ffffff",
        height=900,
        xaxis=dict(
            tickfont=fs.tick_font,
            anchor="y",
            range=[X[0], X[-1] + 1],
        ),
        xaxis2=dict(
            tickfont=fs.tick_font,
            anchor="y",
            range=[X[0], X[-1] + 1],
        ),
        yaxis=dict(
            tickfont=fs.tick_font,
            anchor="x",
            showticklabels=True,
            range=scale,
        ),
        yaxis2=dict(
            tickfont=fs.tick_font,
            anchor="x2",
            showticklabels=True,
            range=scale,
        ),
        legend=dict(
            font=fs.tick_font,
            yanchor="top",
            y=0.99,
            xanchor="left",
            x=0.01,
            traceorder="reversed",
            bordercolor="black",
            borderwidth=2,
            bgcolor="white",
        ),
        showlegend=True,
        barmode="overlay",
        margin=fs.margin,
    )

    ann = len(fig.layout.annotations)
    for i in range(0, ann):
        if fig.layout.annotations[i]["text"].startswith("Time"):
            fig.layout.annotations[i]["font"] = fs.axes_font
            fig.layout.annotations[i]["y"] = -0.02
        elif fig.layout.annotations[i]["text"].startswith("Cumulative TDEE"):
            fig.layout.annotations[i]["font"] = fs.axes_font
            fig.layout.annotations[i]["x"] = -0.02
        elif fig.layout.annotations[i]["text"] in ["CLMcom", "HadGEM"]:
            fig.layout.annotations[i]["font"] = fs.title_font
        else:
            fig.layout.annotations[i]["font"] = fs.tick_font

    props = fs.axes_props.copy()
    props["zeroline"] = True
    props["zerolinewidth"] = 1
    props["zerolinecolor"] = "#000000"

    fig.update_xaxes(fs.axes_props)
    fig.update_yaxes(props)
    fs.write(
        fig,
        os.path.join(
            DIRS["figures"],
            "tdee"
        )
    )


def co2_table() -> None:
    """
    Writes TDEE sums as a latex table.
    """
    files = glob.glob(os.path.join(DIRS["results"], "rf_*.csv"))
    files = list(filter(lambda x: "_yearly" in x, files))

    data = {}

    for file in files:
        r = rcp(file)
        model = gca_rca(file)
        d = pd.read_csv(file.replace("rf", "tdee").replace("_yearly", ""), index_col=0)
        d2 = d.sum()

        if model not in data:
            data[model] = [{"rcp": r, "tdee": d2}]
        else:
            data[model] = data[model] + [{"rcp": r, "tdee": d2}]

    models = list(data.keys())
    models.sort()

    for model in models:
        rcp_values = [entry["rcp"] for entry in data[model]]
        tdee_values = [entry["tdee"] for entry in data[model]]
        df = pd.DataFrame(tdee_values, index=rcp_values)
        df_sorted = df[df.mean().sort_values(ascending=False).index]
        df_sorted = df_sorted.style.format(precision=2)  # type: ignore

        with open(os.path.join(DIRS["results"], f"tdee_{model}.tex"), "w") as tf:
            tf.write(df_sorted.to_latex())
