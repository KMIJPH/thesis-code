#!/usr/bin/env python3
# File Name: fuse.py
# Description: FuSE data comparison
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 27 Okt 2023 11:08:58
# Last Modified: 14 Feb 2024 22:35:39


# stdlib imports
import glob
import os
import re
from datetime import datetime
from typing import Any

# dependencies
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots

# local imports
from calc.stats import stats
from settings import DIRS, Data, FigureSettings, FuSE
from util import daily, is_outlier, read_at_time

fs = FigureSettings()

RCPS = fs.rcps


def gca_rca(file: str) -> str:
    r = re.search(
        "sd_yearly_2000-2100_HS_SNOWGRID-CL_SDM_(.+)_rcp[0-9]+_(.+).csv",
        os.path.basename(file)
    )
    gca = r.group(1)  # type: ignore
    rca = r.group(2)  # type: ignore
    return gca + "_" + rca


def name(file: str) -> str:
    return re.search(
        "sd_yearly_HS_SNOWGRID-CL_SDM_(.+).csv",
        os.path.basename(file)
    ).group(1)  # type: ignore


def rcp(file: str) -> str:
    return re.search(
        ".+_(rcp[0-9]+)_.+.csv",
        os.path.basename(file)
    ).group(1)  # type: ignore


def fuse_comparison() -> None:
    """
    Plots fuse data for different GCA / RCA / ensembles.
    """
    print("Plotting fuse comparison.")
    files = glob.glob(os.path.join(DIRS["results"], "sd_yearly_2000-2100*"))

    # create data dictionary
    data: dict[str, Any] = {}

    for file in files:
        r = rcp(file)
        model = gca_rca(file)

        sd = pd.read_csv(file, index_col=0)
        if model not in data:
            data[model] = {r: {"time": pd.DatetimeIndex(sd["time"]), "mean": sd["mean"].values}}
        else:
            data[model][r] = {"time": pd.DatetimeIndex(sd["time"]), "mean": sd["mean"].values}

    models = list(data.keys())
    titles = [chr(ord("a") + i) for i in range(len(models))]

    fig = make_subplots(
        rows=3,
        cols=len(models),
        x_title="Time",
        y_title=fs.var_names["sd"],
        column_titles=titles,
        row_titles=RCPS,
    )

    print(models)

    scale = dict(x=(np.inf, -np.inf), y=(np.inf, -np.inf))

    for row, r in enumerate(RCPS):
        for col, model in enumerate(models):
            x = [x.year for x in data[model][r]["time"]]
            y = data[model][r]["mean"]

            if np.nanmin(x) < scale["x"][0]:
                scale["x"] = (np.nanmin(x), scale["x"][1])
            if np.nanmax(x) > scale["x"][1]:
                scale["x"] = (scale["x"][0], np.nanmax(x))

            if np.nanmin(y) < scale["y"][0]:
                scale["y"] = (np.nanmin(y), scale["y"][1])
            if np.nanmax(y) > scale["y"][1]:
                scale["y"] = (scale["y"][0], np.nanmax(y))

            fig.add_trace(
                go.Scatter(
                    x=x, y=y, name=model,
                    showlegend=False, line=dict(color=fs.rcp_colors[r], width=1)
                ),
                row=row + 1, col=col + 1,
            )

    fs.margin.update(l=150, t=100)
    fig.update_layout(
        plot_bgcolor="#ffffff",
        showlegend=False,
        margin=fs.margin,
    )

    # set same axis scaling for each row/column
    n_axes = 3 * len(models)
    for axis in range(1, n_axes + 1):
        xname = "xaxis" if axis == 1 else f"xaxis{axis}"
        yname = "yaxis" if axis == 1 else f"yaxis{axis}"
        fig.update_layout(
            {
                xname: {
                    "range": [scale["x"][0], scale["x"][1]],
                    "tickfont": fs.annotation_font,
                    "showticklabels": True  # if axis in [1, 8, 15] else False

                },
                yname: {
                    "range": [0, scale["y"][1] + (scale["y"][1] / 50)],
                    "tickfont": fs.annotation_font,
                    "tickformat": ".1f",
                    "showticklabels": True  # if axis in [1, 8, 15] else False
                }
            }
        )

    fig.update_xaxes(fs.axes_props)
    fig.update_yaxes(fs.axes_props)

    ann = len(fig.layout.annotations)
    for i in range(0, ann):
        if fig.layout.annotations[i]["text"].startswith("rcp"):
            fig.layout.annotations[i]["font"] = fs.tick_font
            fig.layout.annotations[i]["font"]["color"] = fs.rcp_colors[fig.layout.annotations[i]["text"]]
        if fig.layout.annotations[i]["text"] == "Time":
            fig.layout.annotations[i]["font"] = fs.axes_font
        if fig.layout.annotations[i]["text"] == fs.var_names["sd"]:
            fig.layout.annotations[i]["x"] = -0.02
            fig.layout.annotations[i]["font"] = fs.axes_font
        if fig.layout.annotations[i]["text"] in titles:
            fig.layout.annotations[i]["font"] = fs.title_font

    fs.write(
        fig,
        os.path.join(
            DIRS["figures"],
            "fuse_comparison"
        )
    )


def fuse_histo(fuse: FuSE, only_snow: bool = False, only_winter: bool = False) -> None:
    ds, _ = Data.SD.ds(fuse=fuse)
    year = 2002
    time_range = daily(datetime(year, 1, 1), datetime(year, 12, 31))

    if only_winter:
        time_range = (daily(datetime(year - 1, 11, 1), datetime(year, 3, 1)))

    hs = read_at_time(ds, "HS", time_range)
    hs[hs.mask] = np.NaN

    if only_snow:
        hs[hs < 1] = np.NaN

    # outliers = is_outlier(hs.data, method="custom")
    # hs[outliers] = np.NaN
    # np.nanmax(hs)

    s = stats(hs, d=2002)
    df = pd.DataFrame([s])

    with open(os.path.join(DIRS["results"], f"stats_{fuse.get_filename()}.tex"), "w") as tf:
        tf.write(df.to_latex())

    hist, edges = np.histogram(hs, bins=20, range=(np.nanmin(hs), np.nanmax(hs)))
    df = pd.DataFrame({'bin_edges': edges[:-1], 'frequency': hist})

    with open(os.path.join(DIRS["results"], f"histo_{fuse.get_filename()}.tex"), "w") as tf:
        tf.write(df.to_latex())
