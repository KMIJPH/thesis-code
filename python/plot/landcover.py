#!/usr/bin/env python3
# File Name: landcover.py
# Description: Landcover related plots
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 12 Jan 2023 14:18:03
# Last Modified: 01 Feb 2024 23:08:04

# stdlib imports
import os
from tempfile import mkdtemp
from zipfile import ZipFile

# dependencies
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from netCDF4 import Dataset
from osgeo import gdal, ogr
from PIL import Image, ImageOps
from plotly.subplots import make_subplots

# local imports
import settings
from plot.percentages import percentages
from settings import DIRS, LC_FILTER, LC_NAMES, Data, FigureSettings

fs = FigureSettings()


def _borders(lat: np.ndarray, lon: np.ndarray) -> str:
    """
    Clips the eurostat shapefile to the study area extent.
    Coverts the clipped shapefile to a raster which can then be used in the landcover plot.
    Shapefile from: https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units
    """
    driverName = "ESRI Shapefile"
    driver = ogr.GetDriverByName(driverName)

    # original
    path = os.path.join(DIRS["shapefile_dir"], "CNTR_BN_01M_2020_4326.shp.zip")
    shp = driver.Open(path, 0)
    layer = shp.GetLayer()

    # osgeo wouldn't let me open the shapefile from the zip
    zip = ZipFile(os.path.join(DIRS["shapefile_dir"], "FuSE_study_area.zip"))
    temp = mkdtemp()
    zip.extractall(temp)

    clip_path = os.path.join(temp, "FuSE_study_area.shp")
    clip = driver.Open(clip_path, 0)
    clip_layer = clip.GetLayer()

    # clip
    srs = ogr.osr.SpatialReference()
    srs.ImportFromEPSG(4326)

    temp2 = mkdtemp()
    clipped_path = os.path.join(temp2, "output.shp")
    out = driver.CreateDataSource(clipped_path)
    out_layer = out.CreateLayer("out", srs)
    ogr.Layer.Clip(layer, clip_layer, out_layer)

    shp.Destroy()
    clip.Destroy()
    out.Destroy()

    # to raster
    temp_file = os.path.join(temp2, "output.tif")
    options = gdal.RasterizeOptions(
        outputType=gdal.GDT_Float32,
        format="GTiff",
        height=len(lat),
        width=len(lon),
        noData=0.0,
        outputBounds=[np.min(lon), np.min(lat), np.max(lon), np.max(lat)],
        outputSRS=srs,
        allTouched=True,
    )
    t = gdal.Rasterize(temp_file, clipped_path, options=options)
    t = None  # save the tif
    return temp_file


def _percentages(lc: np.ndarray) -> pd.Series:
    """
    Returns percentages (area) of landcover types.
    """
    not_null = len(lc[lc != 0])
    perc = {k: len(lc[lc == v]) / not_null * 100 for k, v in Data.LC.lc_types().items()}
    return pd.Series(perc.values(), index=perc.keys())  # type: ignore


def lc_stats() -> go.Figure:
    lc_path = settings.LC().get_outpath()
    lc_ds = Dataset(lc_path, "r")
    lc = lc_ds["type"][0, :, :].data
    lat = lc_ds["lat"][:].data
    lon = lc_ds["lon"][:].data

    # set unclassified to 0
    lc[lc == 255] = 0

    # colorbar properties (tick labels and colors)
    # skipping unclassified in labels
    names = [x for x in lc_ds["type"].ncattrs() if str(x)[0].isupper() and str(x) != "Unclassified"]
    abbr_names = [LC_NAMES[x] for x in names]

    fig = make_subplots(
        rows=2,
        cols=2,
        column_widths=[0.8, 0.3],
        row_heights=[0.6, 0.4],
        subplot_titles=["Mode Land Cover", "Elevation (m)", "Area (%)"],
        horizontal_spacing=0.025,
        vertical_spacing=0.125,
        specs=[[{"rowspan": 2}, {}], [None, {"type": "bar"}]]
    )

    # map
    fig.add_trace(
        go.Heatmap(
            z=lc,
            x=lon,
            y=lat,
            showscale=False,
            showlegend=False,
            colorscale=fs.create_colorscale(list(fs.lc_colors.values())),
            hovertemplate="<br>".join([
                "Longitude: %{x}",
                "Latitude: %{y}",
                "Land cover: %{z}<extra></extra>",
            ]),
            colorbar=dict(
                title={
                    "text": "Land cover",
                    "font": fs.axes_font,
                },
                titleside="top",
                tickmode="array",
                tickvals=np.linspace(1.5, len(abbr_names) - 0.5, len(abbr_names)),
                ticktext=abbr_names,
                tickfont=fs.tick_font,
                ticks="outside",
            ),
        ),
        row=1, col=1
    )

    # add background
    path = _borders(lat, lon)
    im = Image.open(path)
    im = im.convert("RGB")
    im = ImageOps.invert(im)
    fig.add_layout_image(
        dict(
            source=im,
            xref="x",
            yref="y",
            x=np.min(lon),
            y=np.max(lat),
            sizex=np.max(lon) - np.min(lon),
            sizey=np.max(lat) - np.min(lat),
            sizing="stretch",
            opacity=1.0,
            layer="below"
        )
    )

    # boxplots
    dem_path = settings.DEM().get_outpath()
    dem_ds = Dataset(dem_path, "r")
    elev = dem_ds["DEM"][0, :, :].data

    values = dict(filter(lambda k: k[0] not in (LC_FILTER + ["DNF"]), Data.LC.lc_types().items()))
    colors = [fs.lc_colors[k] for k, _ in values.items()]

    grouped = {}
    for name, value in values.items():
        temp = elev[lc == value]
        grouped[name] = temp

    # create dataframe from grouped variables (setting non matching lengths to NaN)
    df = pd.DataFrame(dict([(k, pd.Series(v)) for k, v in grouped.items()]))

    for i, (name, data) in enumerate(df.items()):  # type: ignore
        fig.add_trace(
            go.Box(
                x=data,
                name=name,
                fillcolor=colors[i],
                showlegend=False,
                whiskerwidth=1,
                quartilemethod="exclusive",
                boxpoints=False,
                line={
                    "color": "#000000",
                    "width": 1,
                },
            ),
            row=1, col=2,
        )

    fig.update_layout(boxgap=0.1, boxgroupgap=0.1)

    # add pie chart
    perc = _percentages(lc)
    fig.add_trace(percentages(perc, col="lc"), row=2, col=2)

    temp = fs.annotation_font.copy()
    temp["size"] = 10

    # labels
    fs.margin.update(b=25, t=25)
    fig.update_layout(
        plot_bgcolor="#ffffff",
        xaxis=dict(
            showticklabels=True,
            tickfont=fs.tick_font,
            anchor="y",
            ticks="outside",
            tickformat=".1f",
        ),
        yaxis=dict(
            showticklabels=True,
            tickfont=fs.tick_font,
            anchor="x",
            ticks="outside",
            tickformat=".1f",
        ),
        xaxis2=dict(
            range=[0, 3800],
            showticklabels=True,
            tickfont=fs.tick_font,
            ticks="outside",
            anchor="y2",
        ),
        yaxis2=dict(
            showticklabels=False,
            tickfont=temp,
            ticks=None,
            anchor="x2",
        ),
        xaxis3=dict(
            showticklabels=False,
            tickfont=fs.tick_font,
            anchor="y3",
            ticks="outside",
            tickmode="array",
            tickvals=[int(10000 * x) for x in np.linspace(0.0, 1.0, 5)],
            ticktext=[str(x * 25) for x in range(0, 5)],
        ),
        yaxis3=dict(
            showticklabels=False,
            tickfont=fs.tick_font,
            anchor="x3",
        ),
        margin=fs.margin,
    )
    fig.add_annotation(
        dict(
            text=np.round(perc["MXF"], 1),
            showarrow=False,
            x=0.91, y=0.45,
            font=fs.tick_font,
            xref="x3 domain", yref="y3 domain",
        )
    )
    fig.add_annotation(
        dict(
            text=np.round(perc["CPL"], 1),
            showarrow=False,
            x=0.62, y=0.45,
            font=fs.tick_font,
            xref="x3 domain", yref="y3 domain",
        )
    )
    fig.add_annotation(
        dict(
            text=np.round(perc["GRA"], 1),
            showarrow=False,
            x=0.45, y=0.45,
            font=fs.tick_font,
            xref="x3 domain", yref="y3 domain",
        )
    )
    fig.add_annotation(
        dict(
            text=np.round(perc["WSA"], 1),
            showarrow=False,
            x=0.23, y=0.45,
            font=fs.tick_font,
            xref="x3 domain", yref="y3 domain",
        )
    )

    for i, d in enumerate(fig.data):
        if type(d) == go.Heatmap:
            d["colorbar"].update(orientation="h", x=0.5, y=1.1, title=None, ypad=5)

    ann = len(fig.layout.annotations)
    for i in range(0, ann):
        if fig.layout.annotations[i]["text"] in ["Elevation (m)", "Area (%)", "Mode Land Cover"]:
            fig.layout.annotations[i]["font"] = fs.axes_font

    temp2 = fs.axes_props.copy()
    temp2.pop("ticks")

    fig.update_xaxes(temp2)
    fig.update_yaxes(temp2)
    fs.write(
        fig,
        os.path.join(
            DIRS["figures"],
            "lc_stats"
        )
    )
