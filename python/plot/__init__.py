#!/usr/bin/env python3
# File Name: __init__.py
# Description: Plot module
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 13 Jan 2023 11:15:47
# Last Modified: 11 Feb 2024 16:56:23


from .co2 import co2
from .fuse import fuse_comparison
from .landcover import lc_stats
from .model import feature_importance, model_stats
from .partition import gra_mxf, mean_partition, mean_partition_by
from .radiative_forcing import rad_forc
from .solar import solar
from .stats import seasonal, yearly, yearly_lc
from .taylor import TaylorDiag, taylor


def to_eps(path: str):
    """
    Converts pdf figures to eps.
    """
    import glob
    import os
    import subprocess

    files = glob.glob(os.path.join(path, "*.pdf"))
    for file in files:
        subprocess.run(["pdftops", "-eps", file])

