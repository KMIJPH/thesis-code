#!/usr/bin/env python3
# File Name: radiative_forcing.py
# Description: Radiative forcing plots
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 06 Dez 2023 13:01:23
# Last Modified: 02 Jan 2024 16:45:10

# stlib imports
import glob
import os
import re

# dependencies
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots

# local imports
from settings import DIRS, FigureSettings

fs = FigureSettings()

RCPS = fs.rcps


def gca_rca(file: str) -> str:
    r = re.search(
        "rf_HS_SNOWGRID-CL_SDM_(.+)_rcp[0-9]+_(.+).csv",
        os.path.basename(file)
    )
    gca = r.group(1)  # type: ignore
    rca = r.group(2)  # type: ignore
    return gca + "_" + rca


def rcp(file: str) -> str:
    return re.search(
        ".+_(rcp[0-9]+)_.+.csv",
        os.path.basename(file)
    ).group(1)  # type: ignore


def rad_forc() -> None:
    """
    Plots a bar plot comparing radiative forcing for different models (and rcps).
    """
    print("Plotting radiative forcing.")
    files = glob.glob(os.path.join(DIRS["results"], "rf_*.csv"))
    files = list(filter(lambda x: "_yearly" not in x, files))

    bars: dict[str, list] = {}

    for file in files:
        r = rcp(file)
        model = gca_rca(file)
        df = pd.read_csv(file, index_col=0)

        if model not in bars:
            bars[model] = [{"rcp": r, "keys": df.columns.values, "values": df.values[0]}]
        else:
            bars[model] = bars[model] + [{"rcp": r, "keys": df.columns.values, "values": df.values[0]}]

    models = list(bars.keys())
    models.sort()

    fig = make_subplots(
        rows=1,
        cols=len(models),
        x_title=fs.var_names["rf"],
        subplot_titles=models
    )

    for col, m in enumerate(models):
        for i, r in enumerate(RCPS):
            color = fs.rcp_colors[r]
            rgb = fs.hex2rgb(color)
            rgba = f"rgba({rgb[0]},{rgb[1]},{rgb[2]},0.5)"
            filt = list(filter(lambda x: x["rcp"] == r, bars[m]))[0]

            lcs = filt["keys"][1:]
            ys = filt["values"][1:]

            sort_index = np.argsort(ys)
            ys = ys[sort_index]
            lcs = lcs[sort_index]

            xs = np.array(list(range(0, len(lcs)))) + (14 * (i + 1))
            middle = ((len(xs) - 1) / 2) + (14 * (i + 1))

            fig.add_trace(
                go.Bar(
                    x=[filt["values"][0]],
                    y=[middle],
                    orientation="h",
                    marker_color=rgba,
                    showlegend=True if col == 0 else False,
                    name=r,
                    marker_line_color="black",
                    width=len(xs),
                ),
                col=col + 1, row=1,
            )

            fig.add_trace(
                go.Bar(
                    x=ys,
                    y=xs,
                    orientation="h",
                    marker_color=color,
                    marker_line_color="black",
                    text=lcs,
                    width=1,
                    showlegend=False,
                    textposition="outside",
                    outsidetextfont=fs.tick_font,
                ),
                col=col + 1, row=1,
            )

    fig.update_layout(
        plot_bgcolor="#ffffff",
        xaxis=dict(
            tickfont=fs.tick_font,
            anchor="y"
        ),
        xaxis2=dict(
            tickfont=fs.tick_font,
            anchor="y"
        ),
        yaxis=dict(
            anchor="x",
            showticklabels=False,
        ),
        yaxis2=dict(
            anchor="x",
            showticklabels=False,
        ),
        legend=dict(
            font=fs.tick_font,
            yanchor="top",
            y=0.2,
            xanchor="left",
            x=0.9,
            traceorder="reversed",
            bordercolor="black",
            borderwidth=2,
            bgcolor="white",
        ),
        showlegend=True,
        barmode="overlay",
    )

    ann = len(fig.layout.annotations)
    for i in range(0, ann):
        if fig.layout.annotations[i]["text"].startswith("Radiative"):
            fig.layout.annotations[i]["font"] = fs.axes_font
        else:
            fig.layout.annotations[i]["font"] = fs.tick_font

    fig.update_xaxes(fs.axes_props)
    fs.write(
        fig,
        os.path.join(
            DIRS["figures"],
            "rf"
        )
    )


def rf_table() -> None:
    """
    Writes RF as a latex table.
    """
    files = glob.glob(os.path.join(DIRS["results"], "rf_*.csv"))
    files = list(filter(lambda x: "_yearly" not in x, files))

    data = {}

    for file in files:
        r = rcp(file)
        model = gca_rca(file)
        d = pd.read_csv(file, index_col=0)

        if model not in data:
            data[model] = [{"rcp": r, "rf": pd.Series(d.squeeze())}]
        else:
            data[model] = data[model] + [{"rcp": r, "rf": pd.Series(d.squeeze())}]

    models = list(data.keys())
    models.sort()

    for model in models:
        rcp_values = [entry["rcp"] for entry in data[model]]
        rf_values = [entry["rf"] for entry in data[model]]
        df = pd.DataFrame(rf_values, index=rcp_values)
        df_sorted = df[df.mean().sort_values(ascending=False).index]
        df_sorted = df_sorted.style.format(precision=2)  # type: ignore

        # the separate lc values don't equal the combined value since for the combined rf we used the area of austria,
        # while ignoring that not all pixels are classified (land cover).
        # the separate lc values use the area of pixels (same is true for TDEE)
        # row_sums = df.drop("combined", axis=1).sum(axis=1)

        with open(os.path.join(DIRS["results"], f"rf_{model}.tex"), "w") as tf:
            tf.write(df_sorted.to_latex())
