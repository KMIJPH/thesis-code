#!/usr/bin/env python3
# File Name: model.py
# Description: Model plots
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 22 Jul 2023 14:19:35
# Last Modified: 24 Feb 2024 15:49:03


# stdlib imports
import glob
import os
import re
from math import ceil

# dependencies
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots

# local imports
from calc import MultiModel
from plot.percentages import percentages
from plot.taylor import taylor
from settings import DIRS, FigureSettings, FuSE

fs = FigureSettings()


def model_stats_table(fuse: FuSE) -> None:
    files = glob.glob(os.path.join(DIRS["train"], "*lgbm_*_skill.json"))
    files = list(filter(lambda x: fuse.get_filename() in x, files))

    data = {}

    for file in files:
        lc_match = re.search('^([A-Z]+)[_]*', os.path.basename(file))
        if lc_match:
            lc = lc_match.group(1)  # type: ignore
            d = pd.read_json(file, typ="series")
            data[lc] = d
        else:
            all = pd.read_json(file)
            data["ALL"] = all["combined"]

    df = pd.DataFrame(data)
    df = df.drop(["pearsonr", "std_ref", "adj_r2"])
    df = df.sort_index(axis=1)
    df = df.style.format(precision=5)  # type: ignore

    with open(os.path.join(DIRS["results"], f"model_stats_{fuse.get_filename()}.tex"), "w") as tf:
        tf.write(df.to_latex())


def feature_importance(fuse: FuSE, only_snow: bool) -> None:
    """
    Plots feature importance
    """
    print("Plotting feature importance.")
    model = MultiModel(fuse, only_snow=only_snow)
    n = len(model.models)
    rows = int(n / 3)
    cols = ceil(n / rows)

    titles = list(model.models.keys())
    titles.insert(12, "")

    fig = make_subplots(
        rows=rows,
        cols=cols,
        x_title="Features", y_title="Importance (%)",
        subplot_titles=titles
    )

    row = 0
    col = 0
    for i, d in enumerate(model.models.items()):
        if row == 3:
            if col == 0:
                col = 1

        # name = d[0]
        m = d[1]
        gain = m.feature_importance("gain")
        split = m.feature_importance("split")
        feature_names = m.feature_name()

        df = {
            "lc": feature_names,
            "gain": [x / sum(gain) * 100 for x in gain],
            "split": [x / sum(split) * 100 for x in split]
        }

        fig.add_trace(
            go.Bar(
                x=df["lc"],
                y=df["gain"],
                marker=dict(color=fs.cs[0], line=dict(color="#000000")),
                name="gain",
                showlegend=True if i == 0 else False
            ),
            row=row + 1, col=col + 1
        )

        fig.add_trace(
            go.Bar(
                x=df["lc"],
                y=df["split"],
                marker=dict(color=fs.cs[10], line=dict(color="#000000")),
                name="split",
                showlegend=True if i == 0 else False
            ),
            row=row + 1, col=col + 1
        )

        col = col + 1

        if col >= cols:
            row = row + 1
            col = 0

    fig.update_layout(
        plot_bgcolor="#ffffff",
        title=dict(
            text=fs.subtitle(fuse.get_filename()).replace("_rcp26", ""),
            x=0.5,
            y=0.95,
            font=fs.title_font,
            xanchor="center",
            yanchor="top",
        ),
        showlegend=True,
        legend=dict(
            font=fs.tick_font,
            yanchor="top",
            y=0.15,
            xanchor="left",
            x=0.85,
        ),
    )

    ann = len(fig.layout.annotations)
    for i in range(0, ann - 2):
        fig.layout.annotations[i]["font"] = fs.tick_font

    for i in range(ann - 2, ann):
        fig.layout.annotations[i]["font"] = fs.axes_font

    # fig.update_xaxes(fs.axes_props)
    fig.update_yaxes(fs.axes_props)

    fs.write(
        fig,
        os.path.join(
            DIRS["figures"],
            f"feature_importance_{fs.clean_fuse(fuse.get_filename())}{'_snow' if only_snow else ''}"
        )
    )


def model_stats(fuse: FuSE):
    """
    Combines taylors to subplots.
    """
    print("Plotting model stats.")
    ts = []
    t = taylor(fuse=fuse)
    t.update()
    ts.append(t)

    fig = make_subplots(
        rows=2, cols=2,
        specs=[[{"type": "polar"}, {}], [{}, {}]],
        vertical_spacing=0.3,
        row_heights=[0.8, 0.2],
    )

    # add traces
    layouts = []
    for i, t in enumerate(ts):
        if i != len(ts) - 1:
            ts[i].fig.update_layout(showlegend=False)

        layouts.append(ts[i].fig.layout)

        for trace in ts[i].fig.data:
            trace["showlegend"] = False if i != len(ts) - 1 or trace["name"] == "" else True
            fig.add_trace(trace, row=1, col=i + 1)

    # use the original polar settings
    polars = {}
    for i, layout in enumerate(layouts):
        name = "polar" if i == 0 else f"polar{i+1}"
        orig = fig.layout[name]
        new = layout["polar"]
        orig.update(dict(domain=dict(x=[0.05, 0.5])))
        new["angularaxis"].update(dict(gridcolor="#828282", linecolor="#000000", linewidth=2)),
        new["radialaxis"].update(dict(gridcolor="#828282", linecolor="#000000", linewidth=2)),
        d = {
            "angularaxis": new["angularaxis"],
            "radialaxis": new["radialaxis"],
            "sector": new["sector"],
            "domain": orig["domain"],
            "bgcolor": "#ffffff",
        }
        polars[name] = d

    fig.update_layout(polars)

    # real vs pred
    la = os.path.join(DIRS["results"], "alb_yearly_2002-2019.csv")
    modis_la = os.path.join(DIRS["results"], f"alb_pred_modis_yearly_2002-2019_{fuse.get_filename()}.csv")

    real = pd.read_csv(la, index_col=0)
    modis = pd.read_csv(modis_la, index_col=0)

    x = pd.DatetimeIndex(modis["time"])
    common = [v in x for v in pd.DatetimeIndex(real["time"])]
    x_num = [xx.year for xx in x]
    real = real[common]

    fig.add_trace(
        go.Scatter(
            x=x_num, y=modis["mean"], name="predicted", legend="legend2",
            line=dict(color=fs.var_colors["alb"], dash="dot")
        ),
        row=1, col=2
    )
    fig.add_trace(
        go.Scatter(
            x=x_num, y=real["mean"], name="real", legend="legend2",
            line=dict(color=fs.var_colors["alb"])
        ),
        row=1, col=2
    )

    # feature importance
    def importance(model: MultiModel) -> pd.DataFrame:
        gain_importance: dict[str, int] = {}
        split_importance: dict[str, int] = {}

        for i, d in enumerate(model.models.items()):
            m = d[1]
            gain = m.feature_importance("gain")
            split = m.feature_importance("split")
            feature_names = m.feature_name()

            for feature, gain_value, split_value in zip(feature_names, gain, split):
                gain_importance[feature] = gain_importance.get(feature, 0) + gain_value
                split_importance[feature] = split_importance.get(feature, 0) + split_value

        total_gain = sum(gain_importance.values())
        total_split = sum(split_importance.values())
        gain_percentages = {feature: (gain_value / total_gain) * 100 for feature, gain_value in gain_importance.items()}
        split_percentages = {feature: (split_value / total_split) * 100 for feature, split_value in split_importance.items()}
        gain_df = pd.DataFrame(list(gain_percentages.items()), columns=["feature", "gain"])
        split_df = pd.DataFrame(list(split_percentages.items()), columns=["feature", "split"])
        return pd.merge(gain_df, split_df, on="feature")

    size = 10000
    model_all = MultiModel(fuse, only_snow=False)
    imp_all = importance(model_all)
    imp_all.set_index("feature", inplace=True)
    imp_all = imp_all.sort_index(ascending=True)

    for col, x in enumerate(["gain", "split"]):  # type: ignore
        fig.add_trace(
            percentages(imp_all[x], size), row=2, col=col + 1
        )

    for i, d in enumerate(fig.data):
        if type(d) == go.Heatmap:
            d["colorbar"].update(orientation="h", x=0.5, y=-0.25, title=None)
            d["showscale"] = True if d.xaxis == "x2" else False

    fig.update_xaxes(fs.axes_props)
    fig.update_yaxes(fs.axes_props)
    fs.margin.update(l=25, t=50, b=150, r=100)
    temp = fs.tick_font.copy()
    temp["size"] = 20
    fig.update_layout(
        width=fs.width,
        height=fs.height,
        plot_bgcolor="#ffffff",
        margin=fs.margin,
        showlegend=True,
        xaxis=dict(
            range=[2003, 2019],
            title={
                "text": "Time",
                "font": fs.tick_font,
            },
            tickfont=fs.tick_font,
            anchor="y"
        ),
        yaxis=dict(
            side="right",
            tickfont=fs.tick_font,
            anchor="x"
        ),
        yaxis2=dict(
            showticklabels=False,
            anchor="x2",
            ticks=None,
        ),
        yaxis3=dict(
            showticklabels=False,
            anchor="x3",
            ticks=None,
        ),
        xaxis2=dict(
            showticklabels=False,
            tickfont=fs.tick_font,
            anchor="y2",
            tickmode="array",
            ticks="outside",
            tickvals=[int(size * x) for x in np.linspace(0.0, 1.0, 11)],
            ticktext=[str(x * 10) for x in range(0, 11)],
        ),
        xaxis3=dict(
            showticklabels=False,
            tickfont=fs.tick_font,
            anchor="y3",
            tickmode="array",
            ticks="outside",
            tickvals=[int(size * x) for x in np.linspace(0.0, 1.0, 11)],
            ticktext=[str(x * 10) for x in range(0, 11)],
        )
    )
    fig.update_layout(
        legend=dict(
            font=temp,
            yanchor="top",
            y=0.91,
            xanchor="left",
            bordercolor="black",
            borderwidth=2,
            x=0,
        ),
        legend2=dict(
            font=fs.tick_font,
            yanchor="top",
            y=0.99,
            xanchor="left",
            x=0.88,
            bordercolor="black",
            borderwidth=2,
            bgcolor="white",
        )
    )
    temp = fs.tick_font.copy()
    temp["color"] = "#ffffff"
    clmcom = True if "CLMcom" in fuse.get_filename() else False
    fig.update_layout(
        annotations=[
            dict(
                text=np.round(imp_all["gain"]["lai"], 1),
                showarrow=False,
                x=0.6, y=0.45,
                font=fs.tick_font,
                xref="x2 domain", yref="y2 domain",
            ),
            dict(
                text=np.round(imp_all["gain"]["dss"], 1),
                showarrow=False,
                x=0.23, y=0.45,
                font=temp,
                xref="x2 domain", yref="y2 domain",
            ),
            dict(
                text=np.round(imp_all["gain"]["sd"], 1),
                showarrow=False,
                x=0.17, y=0.45,
                font=fs.tick_font,
                xref="x2 domain", yref="y2 domain",
            ),
            dict(
                text=np.round(imp_all["split"]["sd"], 1) if clmcom else np.round(imp_all["split"]["dss"], 1),
                showarrow=False,
                x=0.95, y=0.45,
                font=fs.tick_font if clmcom else temp,
                xref="x3 domain", yref="y3 domain",
            ),
            dict(
                text=np.round(imp_all["split"]["dss"], 1) if clmcom else np.round(imp_all["split"]["sd"], 1),
                showarrow=False,
                x=0.80, y=0.45,
                font=temp if clmcom else fs.tick_font,
                xref="x3 domain", yref="y3 domain",
            ),
            dict(
                text=np.round(imp_all["split"]["lai"], 1),
                showarrow=False,
                x=0.62, y=0.45,
                font=fs.tick_font,
                xref="x3 domain", yref="y3 domain",
            ),
            dict(
                text="a",
                showarrow=False,
                x=0.03, y=1,
                font=fs.title_font,
                xref="paper", yref="paper",
            ),
            dict(
                text="b",
                showarrow=False,
                x=0.52, y=1,
                font=fs.title_font,
                xref="paper", yref="paper",
            ),
            dict(
                text="c",
                showarrow=False,
                x=0.03, y=0.2,
                font=fs.title_font,
                xref="paper", yref="paper",
            ),
            dict(
                text="d",
                showarrow=False,
                x=0.52, y=0.2,
                font=fs.title_font,
                xref="paper", yref="paper",
            ),
            dict(
                text=t.x_label,
                showarrow=False,
                x=0.15, y=0.3,
                textangle=0,
                font=fs.tick_font,
                xref="paper", yref="paper",
            ),
            dict(
                text=t.y_label,
                showarrow=False,
                x=0.37, y=0.93,
                textangle=47,
                font=fs.tick_font,
                xref="paper", yref="paper",
            ),
            dict(
                text=fs.var_names["alb"],
                showarrow=False,
                x=1.06, y=0.75,
                textangle=90,
                font=fs.tick_font,
                xref="paper", yref="paper",
            ),
            dict(
                text="Feature Importance (%)",
                showarrow=False,
                x=0.5, y=-0.11,
                font=fs.tick_font,
                xref="paper", yref="paper",
            ),
            dict(
                text="Gain",
                showarrow=False,
                x=0.5, y=-0.4,
                font=fs.tick_font,
                xref="x2 domain", yref="y2 domain",
            ),
            dict(
                text="Split",
                showarrow=False,
                x=0.5, y=-0.4,
                font=fs.tick_font,
                xref="x3 domain", yref="y3 domain",
            ),
        ]
    )

    fs.write(
        fig,
        os.path.join(
            DIRS["figures"],
            f"model_stats_{fuse.get_filename()}"
        )
    )
