#!/usr/bin/env python3
# File Name: solar.py
# Description:
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 07 Sep 2023 12:55:52
# Last Modified: 02 Jan 2024 16:43:04

# stdlib imports
import json
import os

# dependencies
import numpy as np
import pandas as pd
import plotly.graph_objects as go

# local imports
from settings import DIRS, FigureSettings, FuSE
from util import metrics

fs = FigureSettings()


def sde_star(std: float, std_ref: float, r: float) -> float:
    """
    Calculates standardized SDE according to Wadoux (2021) - eq. 9
    """
    sigma_star = std / std_ref
    return np.sqrt(1 + sigma_star**2 - (2 * sigma_star * r))


def me_star(me: float, std_ref: float) -> float:
    """
    Calculates standardized ME according to Wadoux (2021) - below eq. 11
    """
    return me / std_ref


def arc(center_x: float, center_y: float, radius: float) -> tuple[np.ndarray, np.ndarray]:
    """
    Returns the coordinates for an arc.
    """
    theta = np.linspace(0, np.pi, 100)
    x = center_x + radius * np.cos(theta)
    y = center_y + radius * np.sin(theta)
    return x, y


class SolarDiag():
    """
    Solar diagram according to Wadoux et al. (2021).

    kwargs:
        title: title to set
        y_max: maximum value for y
        y_step: tick step for y
        x_step: tick step for x
        autoscale: autoscale y axis
        scale: scales x axis
        width: plot width
        height: plot height
        shifts: offset for the text labels (xy)
    """

    def __init__(self, **kwargs):
        self.fig = go.Figure()
        self.title = kwargs["title"] if "title" in kwargs else "Solar Diagram"
        self.y_max = kwargs["y_max"] if "y_max" in kwargs else 1.1
        self.x_step = kwargs["x_step"] if "x_step" in kwargs else 0.1
        self.y_step = kwargs["y_step"] if "y_step" in kwargs else 0.1
        self.autoscale = kwargs["autoscale"] if "autoscale" in kwargs else False
        self.width = kwargs["width"] if "width" in kwargs else 1000
        self.height = kwargs["height"] if "height" in kwargs else 900
        self.shifts = kwargs["shifts"] if "shifts" in kwargs else None

        self.x_range = 1.1

        # the cuts are determined based on the correlation, see Jollief et al., (2009), Eq. 14
        self.sun_cuts = [1.0, 0.71, 0.44, 0.31]
        self.sun_rs = [0, 0.7, 0.9, 0.95]
        self.sun_colors = ["#fdeba3", "#fdf4b5", "#fffbd6", "#ffffe4"]

        self.shapes = []

        # draw solar arcs
        for i, r in enumerate(self.sun_cuts):
            x, y = arc(0, 0, r)
            # legend
            self.shapes.append(
                dict(
                    x0=0, y0=0, x1=0, y1=0,
                    type="rect", layer="below", showlegend=True, name=f"r > {self.sun_rs[i]}",
                    fillcolor=self.sun_colors[i], line=dict(color="#000000", width=0.5)
                )
            )
            # arc
            self.shapes.append(
                dict(
                    path=f"M {x[0]}, {y[0]} L {x[1]}, {y[1]} {' '.join([f'L {xi}, {yi}' for xi, yi in zip(x, y)][2:])} Z",
                    type="path", layer="below",
                    fillcolor=self.sun_colors[i], line=dict(color="rgba(0, 0, 0, 0)")
                )
            )

        # add RMSE* = 1 line
        arx, ary = arc(0, 0, 1)
        for i, (x, y) in enumerate(zip(arx[:-1], ary[:-1])):
            self.shapes.append(
                dict(
                    x0=x, y0=y, x1=arx[i + 1], y1=ary[i + 1], layer="below",
                    type="line", line=dict(color="#000000", width=3)
                )
            )

    def add(self, data: dict[str, float], **kwargs):
        """
        Adds a single point (model) to the plot.

        kwargs:
            size: marker size
            name: marker name
        """
        size = kwargs["size"] if "size" in kwargs else 30
        name = kwargs["name"] if "name" in kwargs else ""

        sde = sde_star(data["std"], data["std_ref"], data["pearsonr"])
        me = me_star(data["me"], data["std_ref"])

        if self.autoscale:
            if sde > self.y_max:
                self.y_max = np.round(sde + (sde / 10), 1)

            if abs(me) > self.x_range:
                self.x_range = np.round(abs(me) + (abs(sde / 10)), 1)

        self.fig.add_trace(
            go.Scatter(
                x=[me], y=[sde],
                mode="markers",
                name=name,
                showlegend=False,
                opacity=1,
                marker=dict(
                    size=size,
                    cmin=0,
                    cmid=0.5,
                    cmax=1,
                    color=[data["nse"]],
                    coloraxis="coloraxis",
                    line=dict(width=1, color="#000000")
                )
            )
        )

    def update(self):
        # manually add annotations
        annotations = []
        for trace in self.fig.data:
            name = trace["name"]
            if self.shifts and name not in self.shifts:
                print(f"WARNING: {name} not in shifts!")
                continue
            annotations.append(
                dict(
                    x=trace["x"][0],
                    y=trace["y"][0],
                    text=name,
                    showarrow=True,
                    font=fs.tick_font,
                    arrowwidth=2.5,
                    ay=1 if not self.shifts else self.shifts[trace["name"]][1],
                    ax=1 if not self.shifts else self.shifts[trace["name"]][0],
                )
            )

        self.fig.update_layout(
            plot_bgcolor="#ffffff",
            margin=dict(
                l=50, r=0, t=200, b=0
            ),
            title=dict(
                text=self.title,
                x=0.5,
                font=fs.title_font,
                xanchor="center",
                yanchor="top",
            ),
            xaxis=dict(
                tickfont=fs.tick_font,
                anchor="y",
                showgrid=False,
                tickwidth=2,
                ticks="outside",
                tickvals=np.arange(-self.x_range, self.x_range + self.x_step, self.x_step),
                ticktext=[str(-self.x_range)] + [""] * (len(np.arange(-self.x_range,
                                                                      self.x_range + self.x_step, self.x_step)) - 2) + [str(self.x_range)],
                range=[-self.x_range, self.x_range],
            ),
            yaxis=dict(
                tickfont=fs.tick_font,
                anchor="free",
                position=0.5,
                showgrid=False,
                ticks="outside",
                layer="below traces",
                tickwidth=2,
                tickvals=np.arange(self.y_step, self.y_max + self.y_step, self.y_step),
                ticktext=[""] * len(np.arange(self.y_step, self.y_max, 0.1)) + [str(self.y_max)],
                range=[0, self.y_max],
            ),
            legend=dict(
                font=fs.tick_font,
                itemsizing="trace",
                yanchor="top", xanchor="left",
                y=1.1, x=0.99
            ),
            coloraxis=dict(
                colorscale="Inferno",
                cmin=0,
                cmax=1,
            ),
            coloraxis_colorbar=dict(
                y=0.4,
                len=0.8,
                tickvals=np.linspace(0, 1, 11),
                outlinecolor="#000000",
                outlinewidth=1,
                ticks="inside",
                tickmode="array",
                ticktext=["≤0"] + [str(x / 10) for x in range(1, 11)],
                tickfont=dict(size=15, family=fs.font),
                tickcolor="#ffffff",
                tickwidth=2,
                title=dict(text="MEC", font=fs.tick_font),
            ),
            showlegend=True,
            annotations=[
                dict(
                    text="ME*",
                    showarrow=False,
                    x=-0.04, y=-0.03,
                    textangle=270,
                    font=fs.tick_font,
                    xref="paper", yref="paper",
                ),
                dict(
                    text="SDE*",
                    showarrow=False,
                    x=0.5, y=1.07,
                    font=fs.tick_font,
                    xref="paper", yref="paper",
                )
            ] + annotations,
            shapes=self.shapes,
        )

        self.fig.update_xaxes(fs.axes_props)
        self.fig.update_yaxes(fs.axes_props)

    def save(self, name: str):
        self.update()
        fs.write(
            self.fig,
            os.path.join(
                DIRS["figures"],
                name
            )
        )

    def show(self):
        self.update()
        self.fig.show()


def solar(fuse: FuSE) -> None:
    """
    Plots a solar diagram using the skill metrics obtain by training the separate models.

    kwargs:
        lc_filter: list of landcover types to ignore
    """
    filename = os.path.join(DIRS["train"], f"lgbm_{fuse.get_filename()}")

    with open(f"{filename}_skill.json", "r") as file:
        combined = json.load(file)

    shifts = dict(
        combined=(-100, 0),
        ENF=(100, 0),
        DNF=(100, 0),
        DBF=(-100, 0),
        CVM=(100, -5),
        MXF=(-100, -30),
        OSH=(100, 0),
        WSA=(-100, 15),
        SAV=(100, -30),
        GRA=(-100, 0),
        PWL=(-100, 0),
        CPL=(-100, -15),
        URB=(100, -15),
        ICE=(100, 15),
        BAR=(100, 30),
        WBD=(100, 0),
    )
    s = SolarDiag(
        title="Solar Diagram",
        width=1400,
        shifts=shifts,
    )

    s.add(combined["combined"], name="combined")

    for k, v in combined.items():
        if k == "combined":
            continue
        filename = os.path.join(DIRS["train"], f"{k}_lgbm_{fuse.get_filename()}")
        with open(f"{filename}_skill.json", "r") as file:
            skill = json.load(file)
            s.add(skill, name=k)

    s.save(f"solar_{fuse.get_filename()}")


def test_solar():
    """
    Testing solar diagram using the provided example at:
    https://github.com/AlexandreWadoux/MapQualityEvaluation

    NOTE: skipping label positioning
    """

    s = SolarDiag(
        title="Solar Diagram",
        y_max=1.9,
        width=1000,
        height=900,
    )

    models = pd.read_csv("../R/models.csv", index_col=0)
    obser = pd.read_csv("../R/obser.csv", index_col=0)

    for c in models.columns:
        data = metrics(obser["x"], models[c], 10)

        # nanvalues to 0
        for k, v in data.items():
            if np.isnan(v):
                data[k] = 0
        s.add(data, name=c)

    s.show()

