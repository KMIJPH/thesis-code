#!/usr/bin/env python3
# File Name: taylor.py
# Description: Taylor diagram
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 30 Aug 2023 16:17:06
# Last Modified: 24 Jan 2024 17:20:49

# stdlib imports
import json
import os
from math import ceil

# dependencies
import numpy as np
import plotly.graph_objects as go

# local imports
from settings import DIRS, FigureSettings, FuSE

fs = FigureSettings()


def angle(v: float) -> float:
    """
    Tangent function to calculate the degree angle at which to plot for a given decimal value.
    """
    return np.degrees(np.arctan(v / np.sqrt(1.0 - v**2)))


def arc(r0, theta0, rho) -> tuple[np.ndarray, np.ndarray]:
    """
    Creates circle coordinates.
    """
    theta0 = theta0 * np.pi / 180
    phi = np.linspace(0, 2 * np.pi, 100)
    x = rho * np.cos(phi) + r0 * np.cos(theta0)
    y = rho * np.sin(phi) + r0 * np.sin(theta0)
    r = np.sqrt(x**2 + y**2)

    j = np.where(y < 0)
    theta  = np.arctan2(y, x)
    theta[j] = theta[j] + 2 * np.pi
    return (r, np.degrees(theta))


def normalize_std(reference_std: float, std: float) -> float:
    """
    Normalizes standard deviations.
    """
    normalized_std = std / reference_std
    return normalized_std


class TaylorDiag():
    """
    Taylor diagram according to Taylor (2001).

    kwargs:
        title: title
        normalize: whether to normalize standard deviations
        min: x min
        max: x max
        radius_max: radius max
        radius_step: radius step
        extend: 180 or 90 degrees (default False=90)
        arcs: whether to draw CRMSD arcs or not
        arc_labelm: arc label rotation multiplier
        x_label: x label
        y_label: y label
        add_ref: whether to add reference or not
        ref_line: whether to draw reference line
    """

    def __init__(self, ref: float, **kwargs):
        self.fig = go.Figure(data=go.Scatterpolar())
        self.ref = ref

        self.min = kwargs["min"] if "min" in kwargs else 0
        self.max = kwargs["max"] if "max" in kwargs else 0
        self.title = kwargs["title"] if "title" in kwargs else None
        self.radius_step = kwargs["radius_step"] if "radius_step" in kwargs else 2
        self.radius_max = kwargs["radius_max"] if "radius_max" in kwargs else self.max
        self.arcs = kwargs["arcs"] if "arcs" in kwargs else True
        self.x_label = kwargs["x_label"] if "x_label" in kwargs else "Standard Deviation"
        self.y_label = kwargs["y_label"] if "y_label" in kwargs else "Correlation"
        self.add_ref = kwargs["add_ref"] if "add_ref" in kwargs else True
        self.ref_line = kwargs["ref_line"] if "ref_line" in kwargs else True
        self.arc_labelm = kwargs["arc_labelm"] if "arc_labelm" in kwargs else 1
        self.normalize = kwargs["normalize"] if "normalize" in kwargs else False

        self.rng = 0
        self.deg_min = 0
        self.deg_max = 90
        self.ticks = np.array([0, 0.2, 0.4, 0.6, 0.8, 0.9, 0.95, 0.99, 1])

        self.width = fs.width
        self.height = fs.height

        if "extend" in kwargs:
            if kwargs["extend"] is True:
                m = -self.ticks
                self.ticks = np.concatenate((np.flip(m[1:]), self.ticks))
                self.deg_max = 180
        else:
            self.width = fs.width * 0.6  # type: ignore

        self.tick_deg = [angle(x) for x in self.ticks]
        self.tick_str = [str(np.round(x, 2)) for x in self.ticks]

    def _add_arc(self, radius: float):
        """
        Adds a single CRMSD circle.
        """
        if self.normalize:
            # only scale the radius, not the CRMSD value (label)
            r, theta = arc(1.0, 90, radius / self.ref)
        else:
            r, theta = arc(self.ref, 90, radius)
        text = [""] * len(r)
        text[len(r) - ceil(radius * self.arc_labelm)] = str(np.round(radius, 2))

        self.fig.add_trace(
            go.Scatterpolar(
                r=r, theta=theta,
                mode="lines+text",
                line=dict(color="black", width=1, dash="dash"),
                text=text,
                showlegend=False,
                textfont=fs.dim(fs.tick_font),
                textposition="middle center",
                hoverinfo="none",
                name=""
            )
        )

    def add(self, std: float, corr: float, text: str, **kwargs):
        """
        Adds a single point (model) to the plot.

        kwargs:
            size: symbol size
            width: line width
            marker: marker type
            color: color
        """
        if self.normalize and text != "Reference":
            rel = normalize_std(self.ref, std)
        else:
            rel = std

        if rel > self.max:
            self.max = rel + (rel / 10)
            if self.normalize:
                self.radius_max = kwargs["radius_max"] if "radius_max" in kwargs else std + (std / 10)
            else:
                self.radius_max = kwargs["radius_max"] if "radius_max" in kwargs else self.max

        color = kwargs["color"] if "color" in kwargs else None
        size = kwargs["size"] if "size" in kwargs else 30
        width = kwargs["width"] if "width" in kwargs else size / 3
        symbol = kwargs["marker"] if "marker" in kwargs else np.random.default_rng(self.rng).choice(
            ['circle-open', 'square-open', 'diamond-open', 'cross-open', 'x-open', 'triangle-up-open', 'asterisk-open']
        )
        self.rng += 1

        self.fig.add_trace(
            go.Scatterpolar(
                r=[rel],
                theta=[angle(corr)],
                mode="markers",
                name=text,
                customdata=[corr],
                hovertemplate="<br>".join([
                    "<b>std</b>: %{r}",
                    "<b>corr</b>: %{customdata}"
                ]),
                marker=dict(
                    size=size,
                    symbol=symbol,
                    line=dict(width=width),
                    color=color,
                )
            )
        )

    def update(self):
        if self.title:
            self.fig.update_layout(
                dict(
                    title=dict(
                        text=self.title,
                        x=0.5,
                        font=fs.title_font,
                        xanchor="center",
                        yanchor="top",
                    ),
                )
            )
        self.fig.update_layout(
            dict(
                template=None,
                width=self.width,
                height=self.height,
                showlegend=True,
                legend=dict(
                    font=fs.tick_font,
                ),
                polar=dict(
                    domain=dict(
                        x=[0, 1],
                        y=[0, 1]
                    ),
                    sector=[self.deg_min, self.deg_max],
                    angularaxis=dict(
                        thetaunit="degrees",
                        direction="clockwise",
                        showticksuffix="none",
                        griddash="dot",
                        tickmode="array",
                        tickvals=self.tick_deg,
                        ticktext=self.tick_str,
                        tickfont=fs.tick_font,
                    ),
                    radialaxis=dict(
                        griddash="dot",
                        range=[self.min, self.max],
                        angle=0,
                        tickfont=fs.tick_font,
                        tickangle=-90,
                        tickformat=".1f",
                    )
                ),
            )
        )

        if self.arcs:
            for x in np.arange(self.radius_step, self.radius_max + self.radius_step, self.radius_step):
                self._add_arc(x)

        if self.ref_line:
            self.fig.add_trace(
                go.Scatterpolar(
                    r=np.repeat(1.0 if self.normalize else self.ref, 100),
                    theta=np.linspace(0, 90, 100) if self.deg_max == 90 else np.linspace(-90, 90, 100),
                    mode="lines",
                    line=dict(color="black", width=1, dash="dot"),
                    textfont=fs.tick_font,
                    showlegend=False,
                    hoverinfo="none",
                    name=""
                )
            )

        if self.add_ref:
            if self.normalize:
                self.add(1, 1, "Reference", marker="circle", width=1, color="#000000")
            else:
                self.add(self.ref, 1, "Reference", marker="circle", width=1, color="#000000")

    def save(self, name: str):
        self.update()
        fs.write(
            self.fig,
            os.path.join(
                DIRS["figures"],
                name
            )
        )

    def show(self):
        self.update()
        self.fig.show()


def taylor(fuse: FuSE) -> TaylorDiag:
    """
    Returns a taylor diagram.
    """
    filename = os.path.join(DIRS["train"], f"lgbm_{fuse.get_filename()}")

    with open(f"{filename}_skill.json", "r") as file:
        combined = json.load(file)

    # normal taylor
    t = TaylorDiag(
        combined["combined"]["std_ref"],
        radius_step=0.05,
        arc_labelm=80,
        normalize=True,
        x_label="Standard deviations (normalised)"
    )

    symbols = dict(
        combined="square-open",
        ENF="triangle-up-open",
        DNF="triangle-up-open",
        DBF="triangle-up-open",
        MXF="triangle-up-open",
        WSA="asterisk-open",
        SAV="asterisk-open",
        CPL="x-open",
        CVM="x-open",
        OSH="circle-open",
        GRA="circle-open",
        PWL="diamond-open",
        URB="diamond-open",
        ICE="cross-open",
        BAR="cross-open",
    )

    for k, v in combined.items():
        symbol = symbols[k]

        if k == "combined":
            t.add(v["std"], v["corr"], "Combined", size=25, width=3, color="#000000", marker=symbol)
        else:
            filename = os.path.join(DIRS["train"], f"{k}_lgbm_{fuse.get_filename()}")

            # stats for a given model
            with open(f"{filename}_skill.json", "r") as file:
                skill = json.load(file)

            # std * normalization factor (ref_combined / ref)
            # normalize each models std to its own std_ref
            normalized = skill["std"] * (combined["combined"]["std_ref"] / skill["std_ref"])

            try:
                t.add(normalized, skill["corr"], k, size=25, width=3, color=fs.lc_colors[k], marker=symbol)
            except KeyError:
                t.add(normalized, skill["corr"], k, size=25, width=3, marker=symbol)

    t.fig.update_layout(
        margin=dict(
            l=0, r=0, t=100, b=150
        ),
        annotations=[
            dict(
                text=t.x_label,
                showarrow=False,
                x=0.5, y=-0.17,
                textangle=0,
                font=fs.tick_font,
                xref="paper", yref="paper",
            ),
            dict(
                text=t.y_label,
                showarrow=False,
                x=0.85, y=0.87,
                textangle=47,
                font=fs.tick_font,
                xref="paper", yref="paper",
            )
        ],
    )

    return t


def test_taylor() -> None:
    """
    Simple reference test.
    """
    # Taylor (2001) - fig 1
    t = TaylorDiag(5.5, max=9, extend=True, radius_max=14, arc_labelm=1.7)
    t.add(6.5, 0.7, "test")
    t.show()

    # https://en.wikipedia.org/wiki/Taylor_diagram - fig 1
    t = TaylorDiag(2.8, max=4, radius_step=1, arc_labelm=2)
    t.add(2.8, 0.78, "A")
    t.add(2.7, 0.62, "B")
    t.add(2.3, 0.76, "C")
    t.show()
