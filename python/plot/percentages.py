#!/usr/bin/env python3
# File Name: percentages.py
# Description: Percentages plot
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 24 Jan 2024 15:57:38
# Last Modified: 25 Jan 2024 15:20:03

# dependencies
import numpy as np
import pandas as pd
import plotly.graph_objects as go

from settings import FigureSettings

fs = FigureSettings()


def percentages(percentages: pd.DataFrame | pd.Series, size: int = 10000, col: str = "var") -> go.Heatmap:
    """
    Creates a single "bar plot" where percentages are displayed.
    """
    categories = np.array([x for x in range(1, len(percentages) + 1)])
    values = percentages.values
    sorting_index = np.argsort(percentages)  # type: ignore
    colours = [fs.var_colors[x] for x in percentages.index] if col == "var" else [fs.lc_colors[x] for x in percentages.index]
    num_occurrences_float = (values / 100 * size)  # type: ignore
    num_occurrences = np.round(num_occurrences_float).astype(int)
    diff = size - np.sum(num_occurrences)
    num_occurrences[-1] += diff
    matrix = np.concatenate(
        [np.full((10, num), cat) for cat, num in zip(categories[sorting_index], num_occurrences[sorting_index])], axis=1
    )

    return go.Heatmap(
        z=matrix,
        colorscale=fs.create_colorscale(colours),
        colorbar=dict(
            title=dict(text="Feature", font=fs.axes_font),
            titleside="top",
            tickmode="array",
            tickvals=np.linspace(1.5, len(categories) - 0.5, len(categories)),
            ticktext=percentages.index,
            tickfont=fs.tick_font,
            ticks="outside",
        ),
    )
