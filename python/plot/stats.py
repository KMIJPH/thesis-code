#!/usr/bin/env python3
# File Name: stats.py
# Description: Stats plots
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 08 Mar 2023 20:39:14
# Last Modified: 02 Apr 2024 10:12:15

# stdlib imports
import glob
import os
import re
from typing import Any

# dependencies
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from scipy.stats import linregress

# local imports
from settings import DIRS, FigureSettings, FuSE

fs = FigureSettings()

RCPS = fs.rcps
YEARS = fs.years


def _e(number: float, exponent: int) -> str:
    """
    Represents a float using an fixed exponent.
    """
    coefficient = number * 10 ** -exponent
    formatted_number = f"{coefficient:.2f}e{exponent:+03d}"
    return formatted_number


def _get_lc(fuse: FuSE, prefix: str) -> list[str]:
    """
    Returns the calculated mean albedo files for the separated landcovers.
    """
    files = glob.glob(os.path.join(DIRS["results"], f"{prefix}_*_{fuse.get_filename()}*.csv"))
    return files


def _yearly_data(fuse: FuSE, la: str, rcp: str, prefix: str, lc: str, scale: tuple[float, float]) -> tuple[np.ndarray, pd.Series, pd.Series, pd.Series, Any, Any, tuple[float, float]]:
    """
    Convenience function.
    """
    file = la.replace("rcp26", rcp)
    df = pd.read_csv(file, index_col=0)

    x = pd.DatetimeIndex(df["time"])
    x_num = np.array([xx.year for xx in x])
    y = df["mean"]
    q25 = df["q0.25"]
    q75 = df["q0.75"]
    res = linregress(x_num, y)
    line = x_num  * res.slope + res.intercept

    scale = (min(scale[0], np.min(y)), max(scale[1], np.max(y)))

    if prefix == "alb_pred":
        # predicted modis era
        modis_pred_file = os.path.join(
            DIRS["results"],
            f"alb_pred_modis_lai_mean_yearly_{lc}2002-2019_{fuse.get_filename()}.csv"
        )
        modis_pred = pd.read_csv(modis_pred_file, index_col=0)

        # 2002 already skipped
        x_modis_pred = pd.DatetimeIndex(pd.to_datetime(modis_pred["time"]))
        y_modis_pred = modis_pred["mean"]
        q25_modis_pred = modis_pred["q0.25"]
        q75_modis_pred = modis_pred["q0.75"]

        x = np.concatenate((x_modis_pred, x))  # type: ignore
        x_num = np.concatenate(([xx.year for xx in x_modis_pred], x_num))  # type: ignore
        y = np.concatenate((y_modis_pred, y))
        q25 = np.concatenate((q25_modis_pred, q25))
        q75 = np.concatenate((q75_modis_pred, q75))
        res = linregress(x_num, y)
        line = x_num  * res.slope + res.intercept

        scale = (min(scale[0], np.nanmin(y)), max(scale[1], np.nanmax(y)))

    return x_num, y, q25, q75, line, res, scale


def yearly(prefix: str, exponent: int, filt: str = "2020-2100") -> None:
    """
    Plots yearly (mean) data.

    prefix: file prefix
    filt: filename filter (include)
    """
    print("Plotting yearly.")

    fuse = FuSE("HS_SNOWGRID-CL_SDM_ICHEC_EC-EARTH_r12i1p1_rcp26_CLMcom_CCLM4-8-17.nc")
    f = FuSE(fuse.fuse_26())
    landcovers = _get_lc(f, f"{prefix}_yearly")
    landcovers = list(filter(lambda x: filt in x, landcovers))
    landcovers.sort()

    for la in landcovers:
        line_shown = False
        y_title = "Albedo"
        if prefix == "sd":
            y_title = fs.var_names["sd"]
        if prefix == "dss":
            y_title = fs.var_names["dss"]

        fig = make_subplots(
            rows=3,
            cols=2,
            row_titles=RCPS,
            column_titles=["CLMcom", "HadGEM"],
            horizontal_spacing=0.05,
            vertical_spacing=0.05,
            x_title="Time", y_title=y_title,
        )

        lc_match = re.search(f'^{prefix}_yearly_([A-Z]+)_*', os.path.basename(la))
        if lc_match:
            lc = lc_match.group(1) + "_"  # type: ignore
        else:
            lc = ""

        scale = (np.inf, -np.inf)
        years = (3000, 0)

        for col in range(0, 2):
            if col == 1:
                la = la.replace("HS_SNOWGRID-CL_SDM_ICHEC_EC-EARTH_r12i1p1_rcp26_CLMcom_CCLM4-8-17",
                                "HS_SNOWGRID-CL_SDM_MOHC_HadGEM2-ES_r1i1p1_rcp26_SMHI_RCA4")

            for row, rcp in enumerate(RCPS):
                x, y, _, _, line, res, scale = _yearly_data(fuse, la, rcp, prefix, lc, scale)
                fig.add_trace(
                    go.Scatter(
                        x=x, y=y, name="mean", line=dict(color=fs.rcp_colors[rcp], width=5),
                        showlegend=True if row == 0 and col == 0 else False
                    ),
                    row=row + 1, col=col + 1
                )
                # y_upper = q75
                # y_lower = q25
                #
                # color = fs.rcp_colors[rcp].lstrip("#")
                # rgb = tuple(int(color[i:i + 2], 16) for i in (0, 2, 4))
                # rgba = f"rgba({rgb[0]},{rgb[1]},{rgb[2]},0.2)"
                #
                # fig.add_trace(
                #     go.Scatter(
                #         x=np.concatenate((x, x[::-1])),
                #         y=np.concatenate((y_upper, (y_lower)[::-1])),
                #         fill="toself",
                #         fillcolor=rgba,
                #         line=dict(color="rgba(255,0,0,0)"),
                #         name="IQR",
                #         showlegend=True if row == 0 and col == 0 else False
                #     ),
                #     row=row + 1, col=col + 1
                # )

                years = (min(years[0], np.nanmin(x)), max(years[1], np.nanmax(x)))
                scale = (min(scale[0], np.nanmin(y)), max(scale[1], np.nanmax(y)))
                # scale = (min(scale[0], np.nanmin(y_lower)), max(scale[1], np.nanmax(y_upper)))

                # if prefix == "alb_pred":
                #     # real modis data
                #     modis_file = re.sub(f"{prefix}_yearly([_A-Z]*)_[0-9]+.*\\.csv", r"alb_yearly\1_2002-2019.csv", la)
                #     modis = pd.read_csv(modis_file, index_col=0)
                #     y_modis = modis["mean"][1:]
                #     y_modis = np.concatenate((y_modis, np.full(len(y) - len(y_modis), np.NaN)))  # type: ignore
                #
                #     scale = (min(scale[0], np.nanmin(y_modis)), max(scale[1], np.nanmax(y_modis)))
                #
                #     fig.add_trace(
                #         go.Scatter(
                #             x=x, y=y_modis, name="MODIS", mode="lines",
                #             line=dict(color=fs.rcp_colors["MODIS"], dash="dot", width=3),
                #             showlegend=True if row == 0 and col == 0 else False
                #         ),
                #         row=row + 1, col=col + 1
                #     )

                if res.pvalue < 0.05:
                    fig.add_trace(
                        go.Scatter(
                            x=x, y=line, mode="lines",
                            line=dict(color="#000000", dash="dash", width=3), name="trend",
                            showlegend=True if not line_shown else False
                        ), row=row + 1, col=col + 1
                    )
                    line_shown = True
                    pval = "<0.001" if res.pvalue < 0.001 else f"={res.pvalue: .2}"
                    fig.add_annotation(
                        x=0.05, y=0.1, xref="x domain", yref="y domain",
                        text=f"slope={_e(res.slope, exponent)}<br>p{pval}", showarrow=False,
                        font=fs.dim(fs.tick_font),
                        row=row + 1, col=col + 1
                    )

        # set same axis scaling for each subpanel
        for axis in range(1, 7):
            yname = "yaxis" if axis == 1 else f"yaxis{axis}"
            xname = "xaxis" if axis == 1 else f"xaxis{axis}"
            fig.update_layout(
                {
                    yname: dict(
                        range=[scale[0] - (scale[0] / 100), scale[1] + (scale[1] / 100)],
                        tickfont=fs.tick_font,
                        tickformat=".3f" if prefix != "dss" else ",d",
                        showticklabels=True if axis in [1, 3, 5] else False,
                        anchor=xname.replace("axis", ""),
                    ),
                    xname: dict(
                        range=[years[0], years[1]],
                        tickfont=fs.tick_font,
                        tickangle=45,
                        showticklabels=True if axis in [5, 6] else False,
                        anchor=yname.replace("axis", ""),
                    )
                }
            )

        fs.margin.update(l=150, b=150)
        fig.update_layout(
            plot_bgcolor="#ffffff",
            showlegend=True,
            legend=dict(
                bgcolor="white",
                font=fs.annotation_font,
                yanchor="top",
                y=0.99,
                xanchor="right",
                x=0.46,
                bordercolor="black",
                borderwidth=2,
                valign="middle",
            ),
            margin=fs.margin,
        )

        ann = len(fig.layout.annotations)
        for i in range(0, ann):
            if fig.layout.annotations[i]["text"].startswith("rcp"):
                fig.layout.annotations[i]["font"] = fs.tick_font
                fig.layout.annotations[i]["font"]["color"] = fs.rcp_colors[fig.layout.annotations[i]["text"]]
            if fig.layout.annotations[i]["text"] == y_title:
                fig.layout.annotations[i]["font"] = fs.axes_font
                fig.layout.annotations[i]["x"] = -0.03
            if fig.layout.annotations[i]["text"] == "Time":
                fig.layout.annotations[i]["font"] = fs.axes_font
                fig.layout.annotations[i]["y"] = -0.06
            if fig.layout.annotations[i]["text"] in ["CLMcom", "HadGEM"]:
                fig.layout.annotations[i]["font"] = fs.title_font

        fig.update_xaxes(fs.axes_props)
        fig.update_yaxes(fs.axes_props)
        fs.write(fig, os.path.join(DIRS["figures"], f"{prefix}{'' if lc == '' else '_' + lc}"))


def yearly_lc(fuse: FuSE) -> None:
    landcovers = glob.glob(os.path.join(DIRS["results"], "alb_pred_yearly*.csv"))
    landcovers.sort()

    names = []
    for la in landcovers:
        lc_match = re.search(f'^alb_pred_yearly_([A-Z]+)_2020-2100_{fuse.get_filename()}.csv', os.path.basename(la))
        if lc_match:
            lc = lc_match.group(1)  # type: ignore
            names.append(lc)
    names.insert(12, "")
    names.append("")

    scale = (np.inf, -np.inf)
    fig = make_subplots(
        rows=4,
        cols=4,
        x_title="Time", y_title="Albedo",
        # horizontal_spacing=0.05,
        # vertical_spacing=0.05,
        subplot_titles=names,
    )

    row = 1
    col = 1

    for name in names:
        if name != "":
            la = os.path.join(DIRS["results"], f"alb_pred_yearly_{name}_2020-2100_{fuse.get_filename()}.csv")
            for rcp in RCPS:
                x, y, _, _, line, res, scale = _yearly_data(fuse, la, rcp, "pred", name + "_", scale)

                fig.add_trace(
                    go.Scatter(
                        x=x, y=y, name=rcp,
                        line=dict(color=fs.rcp_colors[rcp], width=2),
                        showlegend=True if row == 1 and col == 1 else False,
                        mode="lines",
                    ),
                    row=row, col=col
                )

        if col == 4:
            row += 1
            col = 1
        else:
            col += 1

    fs.margin.update(l=125)
    fig.update_layout(
        plot_bgcolor="#ffffff",
        margin=fs.margin,
        legend=dict(
            font=fs.tick_font,
            yanchor="top",
            y=0.15,
            xanchor="left",
            x=0.85,
            itemsizing="constant",
            itemwidth=50,
            bordercolor="black",
            borderwidth=2,
            bgcolor="white",
        ),
    )

    ann = len(fig.layout.annotations)
    for i in range(0, ann):
        if fig.layout.annotations[i]["text"] in names:
            fig.layout.annotations[i]["font"] = fs.tick_font
        if fig.layout.annotations[i]["text"] == fs.var_names["alb"]:
            fig.layout.annotations[i]["font"] = fs.axes_font
            fig.layout.annotations[i]["x"] = -0.02
        if fig.layout.annotations[i]["text"] == "Time":
            fig.layout.annotations[i]["font"] = fs.axes_font
            fig.layout.annotations[i]["y"] = -0.02

    for axis in range(1, 16):
        yname = "yaxis" if axis == 1 else f"yaxis{axis}"
        xname = "xaxis" if axis == 1 else f"xaxis{axis}"
        fig.update_layout(
            {
                yname: dict(
                    tickfont=fs.tick_font,
                    tickformat=".2f",
                    anchor=xname.replace("axis", ""),
                ),
                xname: dict(
                    tickfont=fs.tick_font,
                    anchor=yname.replace("axis", ""),
                    showticklabels=True if axis in [1, 5, 9, 14] else False
                )
            }
        )

    fig.update_xaxes(fs.axes_props)
    fig.update_yaxes(fs.axes_props)
    fs.write(fig, os.path.join(DIRS["figures"], f"alb_pred_yearly_{fuse.get_filename()}"))


def seasonal(fuse: FuSE) -> None:
    """
    Plots seasonal (monthly mean) predicted albedo data.
    """
    landcovers = glob.glob(os.path.join(DIRS["results"], "alb_seasonal*.csv"))
    landcovers.sort()

    names = []
    for la in landcovers:
        lc_match = re.search('^alb_seasonal_([A-Z]+)_2003-2019.csv', os.path.basename(la))
        if lc_match:
            lc = lc_match.group(1)  # type: ignore
        else:
            lc = "ALL"
        names.append(lc)
    names.append("")

    fig = make_subplots(
        rows=4,
        cols=4,
        x_title="Months", y_title="Albedo",
        subplot_titles=names,
        vertical_spacing=0.08,
        horizontal_spacing=0.04
    )

    row = 1
    col = 1

    for la in landcovers:
        lc_match = re.search('^alb_seasonal_([A-Z]+)_2003-2019.csv', os.path.basename(la))
        if lc_match:
            lc = lc_match.group(1)  # type: ignore
        else:
            lc = ""

        df_modis = pd.read_csv(la, index_col=0)
        df_lai = pd.read_csv(
            os.path.join(
                DIRS["results"],
                f"alb_pred_modis_seasonal{'_' + lc if lc != '' else ''}_2003-2019_{fuse.get_filename()}.csv"),
            index_col=0
        )
        df_lai_mean = pd.read_csv(
            os.path.join(
                DIRS["results"],
                f"alb_pred_modis_lai_mean_seasonal{'_' + lc if lc != '' else ''}_2003-2019_{fuse.get_filename()}.csv"),
            index_col=0
        )

        x = df_modis["time"]
        y_upper = df_modis["mean"] + df_modis["std"]
        y_lower = df_modis["mean"] - df_modis["std"]

        fig.add_trace(
            go.Scatter(
                x=x, y=df_modis["mean"], name="MODIS",
                line=dict(color=fs.rcp_colors["MODIS"], width=3),
                showlegend=True if row == 1 and col == 1 else False,
                mode="lines+markers",
            ),
            row=row, col=col
        )
        fig.add_trace(
            go.Scatter(
                x=x, y=df_lai["mean"], name="PRED_LAI",
                line=dict(color=fs.var_colors["dss"], dash="dot", width=3),
                showlegend=True if row == 1 and col == 1 else False,
                mode="lines+markers",
            ),
            row=row, col=col
        )
        fig.add_trace(
            go.Scatter(
                x=x, y=df_lai_mean["mean"], name="PRED_LAI_MEAN",
                line=dict(color=fs.var_colors["sd"], dash="dash", width=3),
                showlegend=True if row == 1 and col == 1 else False,
                mode="lines+markers",
            ),
            row=row, col=col
        )

        color = fs.rcp_colors["MODIS"].lstrip("#")
        rgb = tuple(int(color[i:i + 2], 16) for i in (0, 2, 4))
        rgba = f"rgba({rgb[0]},{rgb[1]},{rgb[2]},0.2)"
        fig.add_trace(
            go.Scatter(
                x=np.concatenate((x, x[::-1])),
                y=np.concatenate((y_upper, (y_lower)[::-1])),
                fill="toself",
                fillcolor=rgba,
                line=dict(color="rgba(255,0,0,0)"),
                name="std",
                showlegend=True if row == 0 and col == 0 else False
            ),
            row=row, col=col
        )

        if col == 4:
            row += 1
            col = 1
        else:
            col += 1

    for axis in range(1, 16):
        yname = "yaxis" if axis == 1 else f"yaxis{axis}"
        xname = "xaxis" if axis == 1 else f"xaxis{axis}"
        fig.update_layout(
            {
                yname: dict(
                    tickfont=fs.annotation_font,
                    tickformat=".2f",
                    anchor=xname.replace("axis", ""),
                ),
                xname: dict(
                    tickfont=fs.annotation_font,
                    tickformat=",d",
                    range=[1, 12],
                    tickvals=list(range(1, 13)),
                    anchor=yname.replace("axis", ""),
                )
            }
        )

    ann = len(fig.layout.annotations)
    for i in range(0, ann):
        if fig.layout.annotations[i]["text"] in names:
            fig.layout.annotations[i]["font"] = fs.tick_font
        if fig.layout.annotations[i]["text"] == "Months":
            fig.layout.annotations[i]["y"] = -0.01
            fig.layout.annotations[i]["font"] = fs.axes_font
        if fig.layout.annotations[i]["text"] == "Albedo":
            fig.layout.annotations[i]["x"] = -0.01
            fig.layout.annotations[i]["font"] = fs.axes_font

    margin = fs.margin
    fig.update_layout(
        plot_bgcolor="#ffffff",
        margin=margin,
        legend=dict(
            font=fs.tick_font,
            yanchor="top",
            y=0.15,
            xanchor="left",
            x=0.815,
            bordercolor="black",
            borderwidth=2,
            bgcolor="white",
        ),
    )
    fig.update_xaxes(fs.axes_props)
    fig.update_yaxes(fs.axes_props)
    fs.write(fig, os.path.join(DIRS["figures"], f"seasonal_{fuse.get_filename()}"))
