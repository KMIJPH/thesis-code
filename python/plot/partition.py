#!/usr/bin/env python3
# File Name: partition.py
# Description: Partition plots
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 17 Jan 2023 11:27:55
# Last Modified: 11 Feb 2024 18:31:40

# stdlib imports
import glob
import os
import re

# dependencies
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots

# local imports
from settings import DIRS, Data, FigureSettings, FuSE

fs = FigureSettings()


def _partition_scatter(x: list[float], ys: [np.ndarray], integer: bool = True) -> list[go.Scatter]:
    def split(strng, sep, pos):
        strng = strng.split(sep)
        return sep.join(strng[:pos]), sep.join(strng[pos:])

    data = []

    for i, c in enumerate(ys):
        if i > 11:  # skip last classes -> too cluttered
            continue
        try:
            lower = float(split(c.name, "-", 1)[0])  # type: ignore
            upper = float(split(c.name, "-", 1)[1])  # type: ignore
        except ValueError:
            lower = float(split(c.name, "-", 2)[0])  # type: ignore
            upper = float(split(c.name, "-", 2)[1])  # type: ignore

        if integer:
            lower = int(lower)
            upper = int(upper)

        name = lower if i == 0 else f"{lower} - {upper}"
        y = c.values
        col = fs.cs[i * 2 if i * 2 <= 24 else 24]

        # create text annotations
        nans = ~np.isnan(y)  # type: ignore
        tp = np.sum(nans) - (0 if i == 0 else i + 1)  # text position
        texts = [name if t == tp else "" for t in range(0, y.size)]  # type: ignore

        data.append(go.Scatter(
            x=x,
            y=y,
            name=name,
            mode="lines+text",
            text=texts,
            textposition="middle center",
            textfont=dict(color=col, size=fs.tick_font["size"]),
            line=dict(color=col, width=3)
        ))

    return data


def gra_mxf(fuse: FuSE) -> None:
    """
    Plots mean partition by data (GRA and MXF).
    """
    mean = Data.ALB
    by = Data.SD
    partition = Data.DSS
    print("Plotting mean partition.")
    outfile = f"partition_{partition}_*_{fuse.get_filename()}_mean_{mean}_by_{by}"

    files = glob.glob(
        os.path.join(
            DIRS["results"], f"{outfile}.csv"
        )
    )
    files = list(filter(lambda x: "GRA" in x or "MXF" in x, files))
    files.sort()
    titles = [
        re.search(f'^partition_dss_(.+)_{fuse.get_filename()}*', os.path.basename(file)).group(1)  # type: ignore
        for file in files
    ]

    fig = make_subplots(
        rows=2,
        cols=2,
        x_title=fs.var_names["sd"],
        y_title=fs.var_names["alb"],
        column_titles=titles,
    )

    for i, file in enumerate(files):
        df = pd.read_csv(file, index_col=0)
        x = df[str(by)].to_list()
        classes = df.columns[1:]

        ys: list[pd.Series] = [df[c] for c in classes]
        fig.add_traces(_partition_scatter(x, ys), cols=i + 1, rows=1)

    # LAI
    mean = Data.ALB
    by = Data.SD
    partition = Data.LAI
    outfile = f"partition_{partition}_*_{fuse.get_filename()}_mean_{mean}_by_{by}"
    files = glob.glob(
        os.path.join(
            DIRS["results"], f"{outfile}.csv"
        )
    )
    files = list(filter(lambda x: "GRA" in x or "MXF" in x, files))
    files.sort()
    titles = [re.search(f'^partition_lai_(.+)_{fuse.get_filename()}*',
                        os.path.basename(file)).group(1) for file in files]  # type: ignore

    for i, file in enumerate(files):
        df = pd.read_csv(file, index_col=0)
        x = df[str(by)].to_list()
        classes = df.columns[1:]

        ys = [df[c] for c in classes]
        fig.add_traces(_partition_scatter(x, ys, False), cols=i + 1, rows=2)

    for scat in fig.data:
        scat["mode"] = "lines"
        if scat["xaxis"] in ["x2", "x4"]:
            scat["showlegend"] = False
        if scat["xaxis"] == "x3":
            scat["legend"] = "legend2"

    fs.margin.update(l=150, t=75)
    fig.update_layout(
        plot_bgcolor="#ffffff",
        xaxis=dict(
            tickfont=fs.tick_font,
            range=[x[0], x[-1]],
            anchor="y"
        ),
        xaxis2=dict(
            tickfont=fs.tick_font,
            range=[x[0], x[-1]],
            anchor="y2"
        ),
        xaxis3=dict(
            tickfont=fs.tick_font,
            range=[x[0], x[-1]],
            anchor="y3"
        ),
        xaxis4=dict(
            tickfont=fs.tick_font,
            range=[x[0], x[-1]],
            anchor="y4"
        ),
        yaxis=dict(
            tickfont=fs.tick_font,
            anchor="x",
            tickformat=".1f",
            range=[0, 1],
        ),
        yaxis2=dict(
            tickfont=fs.tick_font,
            anchor="x2",
            tickformat=".1f",
            range=[0, 1],
        ),
        yaxis3=dict(
            tickfont=fs.tick_font,
            anchor="x3",
            tickformat=".1f",
            range=[0, 1],
        ),
        yaxis4=dict(
            tickfont=fs.tick_font,
            anchor="x4",
            tickformat=".1f",
            range=[0, 1],
        ),
        legend=dict(
            title=dict(
                text="DSS",
            ),
            bgcolor="white",
            font=fs.annotation_font,
            yanchor="top",
            y=1,
            xanchor="right",
            x=1.095,
            itemsizing="constant",
            itemwidth=30,
            valign="middle",
        ),
        legend2=dict(
            title=dict(
                text="LAI",
            ),
            bgcolor="white",
            font=fs.annotation_font,
            yanchor="top",
            y=0.42,
            xanchor="right",
            x=1.11,
            itemsizing="constant",
            itemwidth=30,
            valign="middle",
        ),
        showlegend=True,
        margin=fs.margin,
    )

    ann = len(fig.layout.annotations)
    for i in range(0, ann):
        if fig.layout.annotations[i]["text"] == fs.var_names["alb"]:
            fig.layout.annotations[i]["font"] = fs.axes_font
            fig.layout.annotations[i]["x"] = -0.02
        if fig.layout.annotations[i]["text"] == fs.var_names["sd"]:
            fig.layout.annotations[i]["font"] = fs.axes_font
            fig.layout.annotations[i]["y"] = -0.02
        if fig.layout.annotations[i]["text"] in ["MXF", "GRA"]:
            fig.layout.annotations[i]["font"] = fs.title_font

    fig.update_xaxes(fs.axes_props)
    fig.update_yaxes(fs.axes_props)
    fs.write(
        fig,
        os.path.join(
            DIRS["figures"],
            f"gra_mxf_{fuse.get_filename()}"
        )
    )


def mean_partition_by(fuse: FuSE, mean: Data, by: Data, partition: Data) -> None:
    """
    Plots mean partition by data.
    """
    print("Plotting mean partition.")
    outfile = f"partition_{partition}_*_{fuse.get_filename()}_mean_{mean}_by_{by}"

    files = glob.glob(
        os.path.join(
            DIRS["results"], f"{outfile}.csv"
        )
    )
    files.sort()

    names = []
    for la in files:
        lc_match = re.search(
            f'^partition_{partition}_([A-Z]+)_{fuse.get_filename()}_mean_{mean}_by_{by}.csv', os.path.basename(la))
        if lc_match:
            lc = lc_match.group(1)  # type: ignore
            names.append(lc)
    names.insert(12, "")
    names.append("")

    fig = make_subplots(
        rows=4,
        cols=4,
        subplot_titles=names,
        x_title=fs.var_names["sd"], y_title=fs.var_names["alb"],
    )

    row = 1
    col = 1

    for name in names:
        if name != "":
            file = os.path.join(
                DIRS["results"], f"partition_{partition}_{name}_{fuse.get_filename()}_mean_{mean}_by_{by}.csv")
            df = pd.read_csv(file, index_col=0)
            x = df[str(by)].to_list()
            classes = df.columns[1:]

            ys: list[pd.Series] = [df[c] for c in classes]

            data = _partition_scatter(x, ys, True if partition.dtype() == np.uint64 else False)
            for d in data:
                fig.add_trace(d, row=row, col=col)

        if col == 4:
            row += 1
            col = 1
        else:
            col += 1

    for scat in fig.data:
        scat["mode"] = "lines"
        if scat["xaxis"]  == "x":
            scat["showlegend"] = True
        else:
            scat["showlegend"] = False

    fig.add_annotation(
        x=0.1, y=0.05,
        xref="paper",
        yref="paper",
        text="DSS" if partition == Data.DSS else "LAI",
        font=fs.title_font
    )

    fs.margin.update(l=125)
    fig.update_layout(
        margin=fs.margin,
        plot_bgcolor="#ffffff",
        legend=dict(
            bgcolor="white",
            yanchor="top",
            y=0.225,
            xanchor="left",
            x=0.85,
            font=fs.annotation_font,
            itemsizing="constant",
            itemwidth=30,
            valign="middle",
        )
    )

    ann = len(fig.layout.annotations)
    for i in range(0, ann):
        if fig.layout.annotations[i]["text"] in names:
            fig.layout.annotations[i]["font"] = fs.tick_font
        if fig.layout.annotations[i]["text"] == fs.var_names["alb"]:
            fig.layout.annotations[i]["font"] = fs.axes_font
            fig.layout.annotations[i]["x"] = -0.02
        if fig.layout.annotations[i]["text"] == fs.var_names["sd"]:
            fig.layout.annotations[i]["font"] = fs.axes_font
            fig.layout.annotations[i]["y"] = -0.02

    for axis in range(1, 16):
        yname = "yaxis" if axis == 1 else f"yaxis{axis}"
        xname = "xaxis" if axis == 1 else f"xaxis{axis}"
        fig.update_layout(
            {
                yname: dict(
                    tickfont=fs.tick_font,
                    tickformat=".2f",
                    anchor=xname.replace("axis", ""),
                ),
                xname: dict(
                    tickfont=fs.tick_font,
                    anchor=yname.replace("axis", ""),
                )
            }
        )

    fig.update_xaxes(fs.axes_props)
    fig.update_yaxes(fs.axes_props)
    fs.write(
        fig,
        os.path.join(
            DIRS["figures"],
            f"partition_{partition}_{fuse.get_filename()}_mean_{mean}_by_{by}"
        )
    )


def mean_partition(fuse: FuSE, partition: Data) -> None:
    """
    Plots mean partition data.
    """
    print("Plotting mean partition.")
    files = glob.glob(
        os.path.join(
            DIRS["results"], f"partition_{partition}_*_{fuse.get_filename()}_means.csv"
        )
    )

    for file in files:
        title = re.search(f'^(.+)_{fuse.get_filename()}*', os.path.basename(file)).group(1)  # type: ignore

        df = pd.read_csv(file, index_col=0)
        x = [float(re.search(r"\((.+),.+\)", x).group(1)) for _, x in df["classes"].items()]  # type: ignore
        names = df.columns[1:]

        fig = go.Figure(
            data=[
                go.Scatter(
                    x=x, y=df[name], name=name, mode="lines", line=dict(color=fs.var_colors[name]),
                    yaxis="y" if yaxis == 0 else f"y{yaxis+1}"
                )
                for yaxis, name in enumerate(names)
            ]
        )

        af = fs.axes_font.copy()
        af["color"] = fs.var_colors[names[0]]
        tf = fs.tick_font.copy()
        tf["color"] = fs.var_colors[names[0]]

        fig.update_layout(
            plot_bgcolor="#ffffff",
            title=dict(
                text=fs.subtitle(fuse.get_filename()) if partition in [Data.SD, Data.DSS] else title,
                x=0.5,
                y=0.95,
                font=fs.title_font,
                xanchor="center",
                yanchor="top",
            ),
            xaxis=dict(
                title={
                    "text": fs.var_names[str(partition)],
                    "font": fs.axes_font,
                },
                tickfont=fs.tick_font,
                anchor="y"
            ),
            yaxis=dict(
                title={
                    "text": fs.var_names[str(names[0])],
                    "font": af,
                },
                tickfont=tf,
                tickformat=".1f",
                anchor="x",
            ),
            showlegend=False,
        )

        if len(names) > 1:
            for i, name in enumerate(names[1:]):
                af = fs.axes_font.copy()
                af["color"] = fs.var_colors[name]
                tf = fs.tick_font.copy()
                tf["color"] = fs.var_colors[name]

                fig.update_layout(
                    {
                        f"yaxis{i+2}": dict(
                            title={
                                "text": fs.var_names[str(name)],
                                "font": af,
                            },
                            tickfont=tf,
                            overlaying="y",
                            side="right",
                            anchor="free",
                            autoshift=True,
                            rangemode="tozero",
                        )
                    }
                )

        fig.update_xaxes(fs.axes_props)
        fig.update_yaxes(fs.axes_props)
        fs.write(
            fig,
            os.path.join(
                DIRS["figures"],
                f"{title}_{fuse.get_filename()}"
            )
        )

