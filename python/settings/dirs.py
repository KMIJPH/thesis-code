#!/usr/bin/env python3
# File Name: dirs.py
# Description: Dirs dictionary containing all the directories and paths.
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: Thu 14 Jul 2022 09:54:35 AM CEST
# Last Modified: 05 Dez 2023 17:06:18

# NOTE: Should be modified directly if directories / filenames changed.

# stdlib imports
import json
import os

JSON = "dirs.json"

"""
DATA DIRECTORIES
----------------
output_dir:    output directory for generated files
bdrf_dir:      directory containing the MCD43A1 files
bess_dir:      directory containing the new BESS files (google drive)
canopy_dir:    directory containing the global canopy height tif
dem_dir:       directory containing the ASTGTM file
fuse_dir:      directory containing the FuSE files
lai_dir:       directory containing the MCD15A3H file
lc_dir:        directory containing the MCD12Q1 file
cack_dir:      directory containing the CACK kernel file
figures:       output directory for figures, etc.
partition:     output directory for partitioned files.
result:        output directory for results.
train:         output directory for training data and models.
shapefile_dir: output directory for generated shapefiles

OUTPUT FILE NAMES (will be generated in the output directory)
-------------------------------------
albedo:        name of the calculated albedo values file
bess:          name of the manipulated BESS file for Austria (google drive)
canopy:        name of the manipulated canopy file for Austria
dem:           name of the downsampled DEM file
lc:            name of the calculated mode lc
cack:          name of the mean CACK kernel
lai:           name of the interpolated lai
study_area:    name of the study area shapefile

INPUT FILE NAMES
----------------
canopy_og:     name of the original canopy file
dem_og:        name of the originial DEM file
lai_og:        name of the lai file
lc_og:         name of the landcover file
cack_og:       name of the CACK file
"""

ROOT = "/mnt/Data/Ma"  # change this
OUTPUT = os.path.join(ROOT, "Output")

DIRS = {
    # data directories
    "root_dir": ROOT,
    "output_dir": OUTPUT,
    "bdrf_dir": os.path.join(ROOT, "BDRF"),
    "bess_dir": os.path.join(ROOT, "BESS"),
    "canopy_dir": os.path.join(ROOT, "CANOPY"),
    "dem_dir": os.path.join(ROOT, "DEM"),
    "fuse_dir": os.path.join(ROOT, "FuSE"),
    "lai_dir": os.path.join(ROOT, "LAI"),
    "lc_dir": os.path.join(ROOT, "LC"),
    "cack_dir": os.path.join(ROOT, "CACK"),
    "figures": os.path.join(OUTPUT, "figures"),
    "partition": os.path.join(OUTPUT, "partition"),
    "results": os.path.join(OUTPUT, "results"),
    "train": os.path.join(OUTPUT, "train"),
    "shapefile_dir": os.path.join(OUTPUT, "shp"),

    # output files (to be generated in output directory)
    "albedo": "Albedo_Austria_500m.nc",
    "bess": "BESS_500m.nc",
    "canopy": "Canopy_2019_500m.nc",
    "cack": "CACK_mean_500m.nc",
    "dem": "ASTGTM_003_500m.nc",
    "lc": "MCD12Q1_mode_500m.nc",
    "lai": "MCD15A3H_int.nc",
    "study_area": "FuSE_study_area.shp",

    # input files
    "canopy_og": "Forest_height_2019_NAFR.tif",
    "dem_og": "ASTGTM_NC.003_30m_aid0001.nc",
    "lai_og": "MCD15A3H.006_500m_aid0001.nc",
    "lc_og": "MCD12Q1.006_500m_aid0001.nc",
    "cack_og": "CACKv1.0.nc",
}

ALBEDO_OUTPUT    = os.path.join(DIRS["output_dir"], DIRS["albedo"])
BESS_OUTPUT      = os.path.join(DIRS["output_dir"], DIRS["bess"])
CANOPY_OUTPUT    = os.path.join(DIRS["output_dir"], DIRS["canopy"])
LC_OUTPUT        = os.path.join(DIRS["output_dir"], DIRS["lc"])
LAI_OUTPUT       = os.path.join(DIRS["output_dir"], DIRS["lai"])
DEM_OUTPUT       = os.path.join(DIRS["output_dir"], DIRS["dem"])
CACK_OUTPUT      = os.path.join(DIRS["output_dir"], DIRS["cack"])


def create_output_folders() -> None:
    """
    Creates output directory and subfolders if they don't exist.
    """
    if not os.path.exists(DIRS["output_dir"]):
        os.makedirs(DIRS["output_dir"])

    if not os.path.exists(DIRS["shapefile_dir"]):
        os.makedirs(DIRS["shapefile_dir"])

    if not os.path.exists(DIRS["figures"]):
        os.makedirs(DIRS["figures"])

    if not os.path.exists(DIRS["partition"]):
        os.makedirs(DIRS["partition"])

    if not os.path.exists(DIRS["results"]):
        os.makedirs(DIRS["results"])

    if not os.path.exists(DIRS["train"]):
        os.makedirs(DIRS["train"])

    return


def write_dirs():
    """
    Writes directories in json format to the output directory.
    """
    with open(os.path.join(OUTPUT, JSON), "w") as f:
        json.dump(DIRS, f, indent=2)

