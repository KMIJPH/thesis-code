#!/usr/bin/env python3
# File Name: input.py
# Description: Input datasets
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: Thu 14 Jul 2022 09:54:35 AM CEST
# Last Modified: 22 Dez 2023 12:24:11

# stdlib imports
import os
import re
from pathlib import Path

# dependencies
from netCDF4 import Dataset

# local imports
from settings.dirs import ALBEDO_OUTPUT, BESS_OUTPUT, CACK_OUTPUT, CANOPY_OUTPUT, DEM_OUTPUT, DIRS, LAI_OUTPUT, LC_OUTPUT
from util import get_info

__all__ = [
    "BDRF",
    "BESS",
    "CACK",
    "CANOPY",
    "FuSE",
    "LAI",
    "LC",
    "DEM",
    "DataInput",
]


class DataInput():
    def __init__(self, input: str, output: str):
        self.input = input
        self.output = output

    def get_dataset(self) -> Dataset:
        """
        Returns the netCDF4 Dataset.
        """
        data = Dataset(self.input, "r")
        return data

    def get_info(self) -> dict:
        """
        Returns the netCDF metadata of the dataset.
        """
        data = self.get_dataset()
        return get_info(data)

    def get_path(self) -> str:
        """
        Returns the path to the input file.
        """
        return self.input

    def get_filename(self) -> str:
        """
        Returns the file name of the input file.
        """
        return Path(self.input).stem

    def get_outpath(self) -> str:
        """
        Returns the output path for the class.
        """
        return self.output


class BESS(DataInput):
    """
    BESS class (list of files), (source: google drive).
    NOTE: need to pass index.
    """

    def __init__(self, i: int):
        bess_lst = os.listdir(DIRS["bess_dir"])
        bess_lst = [f for f in bess_lst if Path(f).suffix == ".nc"]
        bess_lst.sort()
        self.files = list(map(lambda x: os.path.join(DIRS["bess_dir"], x), bess_lst))
        super().__init__(self.files[i], BESS_OUTPUT)

    def get_files(self) -> list[str]:
        """
        Returns the list of BESS files.
        """
        return self.files


class CACK(DataInput):
    """
    CACK class.
    """

    def __init__(self, **kwargs):
        if "file" not in kwargs:
            input = os.path.join(DIRS["cack_dir"], DIRS["cack_og"])
            super().__init__(input, CACK_OUTPUT)
        else:
            file = kwargs["file"]
            input = os.path.join(DIRS["cack_dir"], "CACK_" + os.path.basename(file))
            outfile = os.path.join(DIRS["output_dir"], os.path.basename(input))
            super().__init__(input, outfile)


class CANOPY(DataInput):
    """
    Canopy class.
    NOTE: input is a .TIF file.
    """

    def __init__(self):
        input = os.path.join(DIRS["canopy_dir"], DIRS["canopy_og"])
        super().__init__(input, CANOPY_OUTPUT)


class FuSE(DataInput):
    """
    FuSE class.
    """

    def __init__(self, file: str):
        input = os.path.join(DIRS["fuse_dir"], file)
        outfile = os.path.join(DIRS["output_dir"], os.path.basename(input))
        super().__init__(input, outfile)

    def get_dss_path(self):
        """
        Returns corresponding DSS path for given FuSE file.
        """
        file = self.get_filename().replace("HS_SNOWGRID-CL_SDM", "DSS")
        return f"{os.path.join(DIRS['output_dir'], file)}.nc"

    def fuse_26(self) -> str:
        """
        Returns the rcp26 file name, no matter which FuSE file was passed.
        """
        return re.sub("^(.+)_rcp[0-9]+_(.*)$", '\\1_rcp26_\\2', self.get_filename())


class BDRF(DataInput):
    """
    BDRF class (list of files).
    NOTE: need to pass index.
    """

    def __init__(self, i: int):
        bdrf_list = os.listdir(DIRS["bdrf_dir"])
        bdrf_list = [f for f in bdrf_list if Path(f).suffix == ".nc"]
        bdrf_list.sort()
        self.files = list(map(lambda x: os.path.join(DIRS["bdrf_dir"], x), bdrf_list))
        super().__init__(self.files[i], ALBEDO_OUTPUT)

    def get_files(self) -> list[str]:
        """
        Returns the list of BDRF files.
        """
        return self.files


class LC(DataInput):
    """
    LC class.
    """

    def __init__(self):
        input = os.path.join(DIRS["lc_dir"], DIRS["lc_og"])
        super().__init__(input, LC_OUTPUT)


class DEM(DataInput):
    """
    DEM class.
    """

    def __init__(self):
        input = os.path.join(DIRS["dem_dir"], DIRS["dem_og"])
        super().__init__(input, DEM_OUTPUT)


class LAI(DataInput):
    """
    LAI class.
    """

    def __init__(self):
        input = os.path.join(DIRS["lai_dir"], DIRS["lai_og"])
        super().__init__(input, LAI_OUTPUT)

