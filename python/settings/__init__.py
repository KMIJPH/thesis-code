#!/usr/bin/env python3
# File Name: __init__.py
# Description: Settings module.
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: Thu 14 Jul 2022 09:54:35 AM CEST
# Last Modified: 30 Dez 2023 12:05:05

from os.path import join

import numpy as np
from netCDF4 import Dataset

from .common import CORES, LC_FILTER, LC_NAMES, RAND, Data
from .dirs import DIRS, create_output_folders, write_dirs
from .figures import FigureSettings
from .input import *
from .shapefile import BiggerStudyArea, StudyArea


def get_mask() -> np.ndarray:
    """
    Returns the Austria mask.
    """
    ds = Dataset(join(DIRS["output_dir"], "mask.nc"), "r")
    return ds["mask"][:].astype(bool)
