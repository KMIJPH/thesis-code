#!/usr/bin/env python3
# File Name: shapefile.py
# Description: Study area shapefile class.
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: Thu 14 Jul 2022 09:54:35 AM CEST
# Last Modified: 20 Sep 2023 15:00:05

# stdlib imports
import os
from pathlib import Path

# local imports
from settings.dirs import DIRS


class _Shapefile():
    """
    Unnecessary shapefile class, but too lazy to rewrite now..
    """

    def __init__(self, output: str, offset: int):
        self.output = output
        self.offset = offset

    def get_offset(self) -> int:
        """
        Returns the offset (in decimal degrees)
        that should be added to the bounding box coordinates.
        """
        return self.offset

    def get_path(self) -> str:
        """
        Returns the full path to the output file.
        """
        return self.output

    def get_filename(self) -> str:
        """
        Returns the output file name.
        """
        return Path(self.output).stem

    def get_directory(self) -> str:
        """
        Returns the directory containing the output file.
        """
        return str(Path(self.output).parent)


class StudyArea(_Shapefile):
    """
    The study area shapefile used to extract MODIS data from AppEEARS.
    """

    def __init__(self):
        output = os.path.join(DIRS["shapefile_dir"], DIRS["study_area"])
        super().__init__(output, 0)


class BiggerStudyArea(_Shapefile):
    """
    The bigger study area shapefile used to extract DEM data from AppEEARS.
    """

    def __init__(self):
        file = Path(DIRS["study_area"])
        name = file.stem
        extension = file.suffix
        outname = f"{name}_big{extension}"
        output = os.path.join(DIRS["shapefile_dir"], outname)
        super().__init__(output, 1)

