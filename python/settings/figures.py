#!/usr/bin/env python3
# File Name: figures.py
# Description: Plotly figure settings
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 13 Jan 2023 11:15:28
# Last Modified: 25 Jan 2024 13:08:49

# stdlib imports
from enum import Enum
from typing import Any

# dependencies
import numpy as np
import plotly.graph_objects as go
import plotly.io as pio


class OutputType(Enum):
    HTML = 1
    PNG = 2
    EPS = 3
    PDF = 4


class Colourscheme():
    def __init__(self, colours: list[str]):
        self.colours = colours

    def __getitem__(self, idx: int) -> str:
        return self.colours[idx]

    def __len__(self) -> int:
        return len(self.colours)


class Discrete(Colourscheme):
    """
    Scientific colour maps 8.0 (lipari)
    https://zenodo.org/records/8409685
    """

    def __init__(self):
        super().__init__(
            [
                "#031326",
                "#07213A",
                "#0C2E4D",
                "#183E61",
                "#2B4C71",
                "#3F5679",
                "#525B7A",
                "#5E5D79",
                "#6B5F76",
                "#785F72",
                "#85606F",
                "#94616B",
                "#A56267",
                "#B56363",
                "#C8675F",
                "#D86E5E",
                "#E57B62",
                "#EA8B6A",
                "#E99973",
                "#E6A77D",
                "#E5B58A",
                "#E7C398",
                "#EDD3AD",
                "#F4E3C2",
                "#FDF5DA",
            ]

        )


class Categorical(Colourscheme):
    """
    Scientific colour maps 8.0 (lipari)
    https://zenodo.org/records/8409685
    """

    def __init__(self):
        super().__init__(
            [
                "#031326",
                "#FDF5DA",
                "#A56267",
                "#525B7A",
                "#E99B74",
                "#785F72",
                "#DA6F5E",
                "#183E61",
                "#E7C398",
                "#8D616D",
                "#365176",
                "#092844",
                "#655E77",
                "#E98466",
                "#BF6561",
                "#F0DBB7",
                "#E5AD82",
                "#B26364",
                "#EBCFA7",
                "#5C5D79",
                "#061D35",
                "#25486D",
                "#0E3353",
                "#EA906D",
                "#98616A",
            ]

        )


class FigureSettings():
    def __init__(self):
        self.rcps = ["rcp26", "rcp45", "rcp85"]
        self.years = ["2020-2050", "2050-2070", "2070-2101"]
        self.cs = Discrete()
        self.cat = Categorical()

        self.renderer = "pdf"
        self.output = OutputType.PDF
        self.font = "Serif"
        self.title_font = {"size": 45, "family": self.font, "color": "black"}
        self.axes_font = {"size": 38, "family": self.font, "color": "black"}
        self.tick_font = {"size": 26, "family": self.font, "color": "black"}
        self.annotation_font = {"size": 16, "family": self.font, "color": "black"}
        self.margin = dict(l=100, r=50, t=50, b=100)

        self.axes_props = dict(
            showline=True,
            mirror=True,
            ticks="outside",
            showgrid=True,
            griddash="dot",
            linewidth=2,
            gridwidth=0.5,
            linecolor="#000000",
            gridcolor="#949494",
        )

        self.width = 1920
        self.height = 1080

        # lc colours defined in the MCD readme
        self.lc_colors = dict(
            UNC="rgba(0,0,0,0)",
            ENF="#004a1e",
            EBF="#ff8800",  # changed color
            DNF="#4a002e",  # changed color
            DBF="#03befc",  # changed color
            MXF="#338245",
            CSH="#696b00",
            OSH="#b2b500",
            WSA="#704900",
            SAV="#c78100",
            GRA="#7dff4a",
            PWL="#5100c9",
            CPL="#f5e400",  # changed color
            URB="#ff4d4d",
            CVM="#ff87e5",
            ICE="#c9c9c9",
            BAR="#5a5a5a",
            WBD="#323cff",
        )
        # landcover colors
        # variable names lookup table
        self.var_names = dict(
            alb="Albedo",
            alb_pred="Albedo",
            alb_pred_modis="Albedo",
            dss="Days since snowfall",
            sd="Snow Depth (m)",
            elev="Elevation (m)",
            lc="Land cover",
            lai="LAI (m<sup>2</sup> m<sup>-2</sup>)",
            canopy="Canopy height (m)",
            rf="Radiative forcing (µW m<sup>-2</sup>)",
        )

        # variable colors
        self.var_colors = dict(
            alb=self.cat[11],
            alb_pred=self.cat[11],
            alb_pred_modis=self.cat[10],
            aspect=self.cat[5],
            canopy=self.cat[0],
            dss=self.cat[7],
            elev=self.cat[2],
            lai=self.cat[8],
            lat=self.cat[3],
            lc=self.cat[9],
            lon=self.cat[4],
            sd=self.cat[6],
            slope=self.cat[1],
        )

        # rcp colors
        self.rcp_colors = dict(
            rcp26=self.cat[0],
            rcp45=self.cat[2],
            rcp85=self.cat[4],
            MODIS=self.cat[5],
        )

        pio.templates["my_theme"] = go.layout.Template(
            layout=dict(
                font=dict(
                    color="black",
                    family=self.font,
                ),
                margin=self.margin,
                autosize=False,
                width=self.width,
                height=self.height,
            ),
        )

        pio.templates.default = "plotly+my_theme"

    def create_colorscale(self, colors: list[str]) -> list[list]:
        """
        Creates a discrete Plotly colorscale for a list of color strings.
        A colorscale sets the color boundaries for each fraction of data (upper and lower boundary).
        """
        num_classes = len(colors)
        r = np.linspace(0, num_classes + 1, num_classes + 1) / (num_classes + 1)
        colorscale = []
        for i in range(0, (num_classes)):
            if i == 0:
                colorscale.append([0.0, colors[0]])
            else:
                colorscale.append([r[i], colors[i - 1]])
                colorscale.append([r[i], colors[i]])
        colorscale.append([1.0, colors[-1]])
        return colorscale

    def dim(self, font: dict[str, Any]) -> dict[str, Any]:
        """
        Dim font.
        """
        new = font.copy()
        new["color"] = "#828282"
        return new

    def hex2rgb(self, hex: str) -> tuple[int, ...]:
        """
        Converts a hex color string to a rgb tuple.
        """
        color = hex.lstrip("#")
        return tuple(int(color[i:i + 2], 16) for i in (0, 2, 4))

    def clean_fuse(self, fuse_file: str) -> str:
        """
        Cleans fuse file name.
        """
        return fuse_file.replace('HS_SNOWGRID-CL_SDM_', '')

    def subtitle(self, fuse_file: str) -> str:
        """
        Returns subtitle.
        """
        return self.clean_fuse(fuse_file)

    def write(self, fig: go.Figure, path: str) -> None:
        """
        Saves the figure to the disk.
        """
        match self.output:
            case OutputType.HTML:
                fig.write_html(f"{path}.html")
            case OutputType.PNG:
                fig.write_image(f"{path}.png")
            case OutputType.EPS:
                fig.write_image(f"{path}.eps")
            case OutputType.PDF:
                pio.renderers.default = self.renderer
                pio.kaleido.scope.mathjax = None
                fig.write_image(f"{path}.pdf")

