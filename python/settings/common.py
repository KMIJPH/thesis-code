#!/usr/bin/env python3
# File Name: common.py
# Description: Common settings
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 19 Jul 2023 16:21:17
# Last Modified: 08 Dez 2023 10:09:53

# stdlib imports
import os
from enum import Enum
from typing import Any

# dependencies
import numpy as np
from netCDF4 import Dataset

# local imports
from settings.dirs import ALBEDO_OUTPUT, DIRS
from settings.input import CANOPY, DEM, LAI, LC, FuSE

RAND = 28071995  # random seed
CORES = 12  # cores used for models
LC_FILTER = ["EBF", "CSH", "WBD"]  # these landcovers should be skipped (not present in data, except for WBD)

LC_NAMES = {
    "Unclassified": "UNC",
    "Evergreen_Needleleaf_Forests": "ENF",
    "Evergreen_Broadleaf_Forests": "EBF",
    "Deciduous_Needleleaf_Forests": "DNF",
    "Deciduous_Broadleaf_Forests": "DBF",
    "Mixed_Forests": "MXF",
    "Closed_Shrublands": "CSH",
    "Open_Shrublands": "OSH",
    "Woody_Savannas": "WSA",
    "Savannas": "SAV",
    "Grasslands": "GRA",
    "Permanent_Wetlands": "PWL",
    "Croplands": "CPL",
    "Urban_and_Built_up_Lands": "URB",
    "Cropland_Natural_Vegetation_Mosaics": "CVM",
    "Permanent_Snow_and_Ice": "ICE",
    "Barren": "BAR",
    "Water_Bodies": "WBD",
}


class Data(Enum):
    """
    Available data for training / prediction.
    Enums seemed like the easiest way to generalise prediction / trainings / stat calculation, etc..
    """

    ALB = 1
    ALB_PRED = 2
    ALB_PRED_MODIS_ERA = 3
    ASPECT = 4
    CANOPY = 5
    DOY = 6
    DSS = 7
    ELEV = 8
    LAI = 9
    LAI_MEAN = 10
    LAT = 11
    LC = 12
    LON = 13
    SD = 14
    SLOPE = 15
    ALB_PRED_MODIS_ERA_LAI_MEAN = 16

    def __str__(self) -> str:
        match self:
            case Data.ALB:
                return "alb"
            case Data.ALB_PRED:
                return "alb_pred"
            case Data.ALB_PRED_MODIS_ERA:
                return "alb_pred_modis"
            case Data.ALB_PRED_MODIS_ERA_LAI_MEAN:
                return "alb_pred_modis_lai_mean"
            case Data.ASPECT:
                return "aspect"
            case Data.CANOPY:
                return "canopy"
            case Data.DOY:
                return "doy"
            case Data.DSS:
                return "dss"
            case Data.ELEV:
                return "elev"
            case Data.LAI:
                return "lai"
            case Data.LAI_MEAN:
                return "lai_mean"
            case Data.LAT:
                return "lat"
            case Data.LC:
                return "lc"
            case Data.LON:
                return "lon"
            case Data.SD:
                return "sd"
            case Data.SLOPE:
                return "slope"

    def varn(self) -> str:
        """
        Returns variable names to read.
        """
        match self:
            case Data.ALB | Data.ALB_PRED | Data.ALB_PRED_MODIS_ERA | Data.ALB_PRED_MODIS_ERA_LAI_MEAN:
                return "bw"
            case Data.ASPECT:
                return "aspect"
            case Data.CANOPY:
                return "canopy"
            case Data.DOY:
                return "doy"
            case Data.DSS:
                return "dss"
            case Data.ELEV:
                return "DEM"
            case Data.LAI | Data.LAI_MEAN:
                return "lai"
            case Data.LAT:
                return "lat"
            case Data.LC:
                return "type"
            case Data.LON:
                return "lon"
            case Data.SD:
                return "HS"
            case Data.SLOPE:
                return "slope"

    def dtype(self) -> Any:
        """
        Returns dtype.
        """
        match self:
            case Data.ALB | Data.ALB_PRED | Data.ALB_PRED_MODIS_ERA | Data.ALB_PRED_MODIS_ERA_LAI_MEAN | Data.ASPECT  | Data.LAI:
                return np.float64
            case Data.LAI_MEAN | Data.LAT | Data.LON | Data.SD | Data.SLOPE | Data.DOY:
                return np.float64
            case Data.DSS | Data.ELEV | Data.LC | Data.CANOPY:
                return np.uint64

    def ds(self, **kwargs) -> tuple[str, bool]:
        """
        Returns the path to the dataset and whether it is static or not.

        kwargs:
            fuse: fuse file
        """
        fuse = kwargs["fuse"] if "fuse" in kwargs else None

        match self:
            case Data.ALB:
                return ALBEDO_OUTPUT, False
            case Data.ALB_PRED:
                if isinstance(fuse, FuSE):
                    path = os.path.join(DIRS["output_dir"], f"Albedo_{fuse.get_filename()}_pred.nc")
                    return path, False
                else:
                    raise Exception("Pass FuSE dataset.")
            case Data.ALB_PRED_MODIS_ERA:
                if isinstance(fuse, FuSE):
                    path = os.path.join(DIRS["output_dir"], f"Albedo_{fuse.get_filename()}_pred_modis.nc")
                    return path, False
                else:
                    raise Exception("Pass FuSE dataset.")
            case Data.ALB_PRED_MODIS_ERA_LAI_MEAN:
                if isinstance(fuse, FuSE):
                    path = os.path.join(DIRS["output_dir"], f"Albedo_{fuse.get_filename()}_pred_modis_lai_mean.nc")
                    return path, False
                else:
                    raise Exception("Pass FuSE dataset.")
            case Data.ASPECT:
                return DEM().get_outpath(), True
            case Data.CANOPY:
                return CANOPY().get_outpath(), True
            case Data.DOY:
                return "", False
            case Data.DSS:
                if isinstance(fuse, FuSE):
                    return fuse.get_dss_path(), False
                else:
                    raise Exception("Pass FuSE dataset.")
            case Data.ELEV:
                return DEM().get_outpath(), True
            case Data.LAI:
                return LAI().get_outpath(), False
            case Data.LAI_MEAN:
                return LAI().get_outpath().replace(".nc", "_mean.nc"), False
            case Data.LAT:
                return DEM().get_outpath(), True
            case Data.LC:
                return LC().get_outpath(), True
            case Data.LON:
                return DEM().get_outpath(), True
            case Data.SD:
                if isinstance(fuse, FuSE):
                    return fuse.get_outpath(), False
                else:
                    raise Exception("Pass FuSE dataset.")
            case Data.SLOPE:
                return DEM().get_outpath(), True

    def lc_types(self) -> dict[str, int]:
        """
        Returns lc types dictionary.
        """
        lc_ds, _ = Data.LC.ds()
        lc = Dataset(lc_ds, "r")[f"{Data.LC.varn()}"]
        attr_names: list[str] = [
            x for x in lc.ncattrs() if str(x)[0].isupper() and str(x) != "Unclassified"
        ]
        return {LC_NAMES[x]: lc.getncattr(x) for x in attr_names}

    def lc_value(self, s: str) -> int:
        """
        Returns lc value for its name.
        """
        lc_types = self.lc_types()
        return lc_types[s]

    def lc_name(self, i: int) -> str:
        """
        Returns lc name for a given number.
        """
        lc_types = self.lc_types()
        return (list(lc_types.keys())[list(lc_types.values()).index(i)])

