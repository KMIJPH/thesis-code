# Master thesis source code

## Download

``` bash
git clone https://codeberg.org/KMIJPH/thesis-code
```

## Data

- [Albedo Model Parameters - MCD43A1 v006](https://lpdaac.usgs.gov/products/mcd43a1v006/)
- [Land Cover Type - MCD12Q1 v006](https://lpdaac.usgs.gov/products/mcd12q1v006/)
- [Digital Elevation Model - ASTGTM v003](https://lpdaac.usgs.gov/products/astgtmv003/)
- [LAI/FPAR - MCD15A3H v006](https://lpdaac.usgs.gov/products/mcd15a3hv006/)
- [Forest canopy height - GEDI 2019 (NAFR)](https://glad.umd.edu/dataset/gedi)
- [BESS shortwave radiation](https://www.environment.snu.ac.kr/bess-rad)
- [HS-Snowgrid from FuSE-AT](https://fuse-at.ccca.ac.at/)
- [CACK shortwave radiative forcing kerne](https://portal.edirepository.org/nis/mapbrowse?packageid=edi.396.1)
- [Countries 2020 Shapefile](https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units/countries#countries20)

## Dependencies

- [Python and development headers (python-dev)](https://www.python.org/)
- [Cython](https://cython.org/)
- [Virtualenv](https://virtualenv.pypa.io/en/latest/) (or something similar)
- [Rust & Cargo](https://www.rust-lang.org/)
- [GDAL](https://gdal.org/)
- [NetCDF4](https://www.unidata.ucar.edu/software/netcdf/)

### Depending on OS

- [PostgreSQL](https://www.postgresql.org/)
- [MariaDB](https://mariadb.org/)
- [Podofo](https://github.com/podofo/podofo)
- [CFITSIO](https://heasarc.gsfc.nasa.gov/fitsio/)
- [Arrow](https://arrow.apache.org/)

## Setup

``` bash
# I recommend to create a virtual environment
ENV_PATH="python/.venv" # or any other path
virtualenv "$ENV_PATH" --python=python3.11
source "$ENV_PATH/bin/activate"

# install maturin
pip install maturin
# gdal needs to match local version
GDAL=$(gdal-config --version | awk -F'[.]' '{print $1"."$2"."$3}')
pip install gdal==$GDAL
# build the project and its dependencies
maturin develop --release

# if netCDF4 complains
pip install numpy --upgrade
# if gdal/osgeo complains
pip install --no-cache-dir --force-reinstall "GDAL[numpy]==$GDAL"
```

## Usage

See
```bash
python python/main.py --help
```
for a list of commands.

## Tests

``` bash
source "$ENV_PATH/bin/activate"
python -m unittest discover ./python # python
cargo test # rust
```

## Directory layout

Directories are set [in a Python source file](./python/settings/dirs.py) and
expected to follow a specific layout:

`bdrf_dir` should contain 2 files:
- MCD43A1.006_500m_aid0001.nc (start-2011)
- MCD43A1.006_500m_aid0001_2.nc (2011-end)

`bess_dir` should contain one file for each day:
- BESS_RSDN_Daily.A2000061.nc
- BESS_RSDN_Daily.A2000062.nc
- ...

For the other needed directories refer to the [source file](./python/settings/dirs.py).

Other folders will be created upon calling the script (`output_dir`, etc..).
